//
//  ViewController.h
//  efoodguru
//
//  Created by Ranjithkumar on 05/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"


@interface EFGViewController : UIViewController

@property (nonatomic,strong) MMDrawerController * drawerController;

@end

