//
//  EFGWaterVC.h
//  efoodguru
//
//  Created by Ranjithkumar on 04/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFGNavigationView.h"

@interface EFGWaterVC : UIViewController<EFGNavigationProtocol, UITextFieldDelegate>

@end
