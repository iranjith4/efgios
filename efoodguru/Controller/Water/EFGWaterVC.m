//
//  EFGWaterVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 04/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGWaterVC.h"
#import "EFGUtilities.h"
#import "EFGTitleLabel.h"
#import "EFGAPIConnector.h"


@implementation EFGWaterVC{
    float xPos;
    float yPos;
    EFGNavigationView *navigationView;
    EFGTitleLabel *title;
    UIImageView *waterImageView;
    UITextField *waterCups;
    UIButton *setWater;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self initUI];
}

- (void) initUI{
    self.view.backgroundColor = [UIColor whiteColor];
    xPos = 0;
    yPos = STATUSBAR_HEIGHT;
    
    navigationView = [[EFGNavigationView alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 60)];
    [navigationView initViewsWithColor:[EFGUtilities themeColor] title:@"Add Weight" andBackText:@"Cancel"];
    navigationView.delegate = self;
    [self.view addSubview:navigationView];
    yPos += navigationView.frame.size.height + 20;
    
    title = [[EFGTitleLabel alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 40) andTitle:@"Water Consumption"];
    [self.view addSubview:title];
    yPos += title.frame.size.height + 5;
    
    waterImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, self.view.frame.size.height * 0.35)];
    waterImageView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:waterImageView];
    
    yPos += waterImageView.frame.size.height + 15;
    
    waterCups = [[UITextField alloc] initWithFrame:CGRectMake(15, yPos, self.view.frame.size.width - 30, 40)];
    waterCups.placeholder = @"Water Cups";
    waterCups.delegate = self;
    [self.view addSubview:waterCups];
    yPos += waterCups.frame.size.height;
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(waterCups.frame.origin.x, yPos, waterCups.frame.size.width, 1)];
    line.backgroundColor = [UIColor grayColor];
    [self.view addSubview:line];
    
    yPos += line.frame.size.height + 15;
    xPos = 15;
    
    setWater = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 40)];
    [setWater setTitle:@"ADD WATER" forState:UIControlStateNormal];
    [setWater setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    setWater.titleLabel.font = [UIFont systemFontOfSize:16];
    setWater.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    setWater.layer.cornerRadius = 6.0;
    [setWater addTarget:self action:@selector(setWatertapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:setWater];
    yPos += setWater.frame.size.height + 15;
}

- (void)setWatertapped{
    if (waterCups.text != nil && ![waterCups.text  isEqual: @""]) {
        NSDictionary *params = @{
                                 @"username" : [EFGUtilities getUserUsername],
                                 @"password" : [EFGUtilities getUserPassword],
                                 @"date_time" : [EFGUtilities getCurrentFormattedDate],
                                 @"quantity" : waterCups.text
                                 };
        [EFGUtilities showProgress];
        [[EFGAPIConnector sharedManager] saveWaterIntake:params success:^(NSDictionary *dict, NSURLResponse *resp) {
            NSLog(@"%@",dict);
            [EFGUtilities hideProgress];
            [self backNavigationTapped];
            [EFGUtilities showAlertWithTitle:@"Water!" message:@"Water data is added Successfully." actionTitle:@"Ok" inViewController:self type:1];
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [EFGUtilities hideProgress];
            [EFGUtilities serverError];
        }];
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)backNavigationTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
