//
//  EFGFoodVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 03/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGFoodVC.h"
#import "EFGUtilities.h"
#import "EFGFoodQuickAddVC.h"
#import "EFGAPIConnector.h"
#import "EFGCreateFoodOthers.h"



@implementation EFGFoodVC{
    float xPos;
    float yPos;
    EFGNavigationView *navView;
    UISearchBar *searchBar;
    UILabel *recentFoodLabel;
    UILabel *emptyLabel;
    UITableView *emptyFoodTable;
    UIButton *createFood;
    UIButton *quickAddCalories;
    MealType mealType;
    
    
    //Data
    NSArray *recentFoodArray;
}



- (void)viewDidLoad{
    [super viewDidLoad];
}

- (void)initWithFoodType:(MealType)type{
    mealType = type;
    NSString *typeString = [self getStringForType:type];
    NSDictionary *params = @{
                           @"username" : [EFGUtilities getUserUsername],
                           @"password" : [EFGUtilities getUserPassword],
                           @"meal_time" : typeString,
                           @"date_time" : [EFGUtilities getCurrentFormattedDate]
                           };
    [EFGUtilities showProgress];
    [[EFGAPIConnector sharedManager] getRecentFood:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        [EFGUtilities hideProgress];
        [self makeRecentFoodArrayWithDict:dict];
        [self initUI];
    } failure:^(NSHTTPURLResponse *resp, NSError *error) {
        [EFGUtilities hideProgress];
        [self initUI];
    }];
}

- (void)makeRecentFoodArrayWithDict:(NSDictionary *)dict{
    NSArray *apiFoods = dict[@"data"][@"api_foods"];
    NSArray *userFoods = dict[@"data"][@"custom_foods"];
    NSMutableArray *a = [[NSMutableArray alloc] initWithArray:apiFoods];
    for (NSDictionary *d in userFoods) {
        [a addObject:d];
    }
    recentFoodArray = [a mutableCopy];
}

- (void)initUI{
    
    //////////////////////////////////////////////////////////////////////////////////////////
    //Temp Init Methods
    
    
    //////////////////////////////////////////////////////////////////////////////////////////
    
    xPos = 0;
    yPos = STATUSBAR_HEIGHT;
    self.view.backgroundColor = [UIColor whiteColor];
    
    navView = [[EFGNavigationView alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 65)];
    [navView initViewsWithColor:[EFGUtilities themeColor] title:@"Food" andBackText:@"cancel"];
    navView.delegate = self;
    [self.view addSubview:navView];
    
    yPos +=  navView.frame.size.height + 10;
    
    //Search Bar
    xPos = 15;
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 35)];
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchBar.placeholder = @"Search For Food";
    searchBar.delegate = self;
    [self.view addSubview:searchBar];
    
    yPos += searchBar.frame.size.height + 10;
    
    //RECEnt food Container
    
    UIView *recentFoodContainer = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 40)];
    recentFoodContainer.backgroundColor = [EFGUtilities themeColor];
    [self.view addSubview:recentFoodContainer];
    
    
    //Recent Food Titile
    recentFoodLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,0, self.view.frame.size.width, 40)];
    recentFoodLabel.text = @"RECENT FOODS";
    recentFoodLabel.textColor = [UIColor whiteColor];
    recentFoodLabel.backgroundColor = [EFGUtilities themeColor];
    recentFoodLabel.font = [UIFont systemFontOfSize:16];
    [recentFoodContainer addSubview:recentFoodLabel];
    yPos += recentFoodLabel.frame.size.height + 10;
    
    if (recentFoodArray == nil || recentFoodArray.count == 0) {
        emptyLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, yPos, self.view.frame.size.width - 2 * xPos, 0)];
        emptyLabel.text = @"Foods which you are recently added to your diary will appear here.";
        emptyLabel.textColor = [UIColor grayColor];
        emptyLabel.font = [UIFont systemFontOfSize:13];
        emptyLabel.numberOfLines = 0;
        emptyLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [emptyLabel sizeToFit];
        [self.view addSubview:emptyLabel];
        
        yPos += emptyLabel.frame.size.height + 20;
    }else{
        emptyFoodTable = [[UITableView alloc] initWithFrame:CGRectMake(10, yPos, self.view.frame.size.width - 2 * 10, self.view.frame.size.height - yPos - 2 * 40 - 3 * 15) style:UITableViewStylePlain];
        emptyFoodTable.delegate = self;
        emptyFoodTable.dataSource = self;
        [emptyFoodTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell_id"];
        [self.view addSubview:emptyFoodTable];
        yPos += emptyFoodTable.frame.size.height + 15;
    }
    
    //Create a Food
    xPos = 15;
    createFood = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 40)];
    [createFood setTitle:@"CREATE A FOOD" forState:UIControlStateNormal];
    [createFood setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    createFood.titleLabel.font = [UIFont systemFontOfSize:16];
    createFood.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    createFood.layer.cornerRadius = 6.0;
    [createFood addTarget:self action:@selector(createFoodTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:createFood];
    yPos += createFood.frame.size.height + 15;
    
    //Quick Add Calories
    
    quickAddCalories = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 40)];
    [quickAddCalories setTitle:@"QUICK ADD CALORIES" forState:UIControlStateNormal];
    [quickAddCalories setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    quickAddCalories.titleLabel.font = [UIFont systemFontOfSize:16];
    quickAddCalories.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    quickAddCalories.layer.cornerRadius = 6.0;
    [quickAddCalories addTarget:self action:@selector(quickAddTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:quickAddCalories];
    yPos += quickAddCalories.frame.size.height + 15;
}

#pragma mark - Search Delegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)s{
    [s resignFirstResponder];
}

- (void)createFoodTapped{
    EFGCreateFoodVC *createFoodView = [[EFGCreateFoodVC alloc] init];
    createFoodView.delgate = self;
    [createFoodView initViewControllerWithMealType:[self getStringForType:mealType]];
    [self presentViewController:createFoodView animated:YES completion:nil];
}

-(void)createFoodDataGot:(NSDictionary *)data{
    
    EFGCreateFoodOthers *others = [[EFGCreateFoodOthers alloc] init];
    [others initControllerWithPreviousData:data];
    [self presentViewController:others animated:NO completion:^{
        
    }];
}

- (void)quickAddTapped{
    EFGFoodQuickAddVC *quickadd = [[EFGFoodQuickAddVC alloc] init];
    [quickadd initViewController:[self getStringForType:mealType]];
    [self presentViewController:quickadd animated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return recentFoodArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_id" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell_id"];
    }
    cell.textLabel.text = [recentFoodArray objectAtIndex:indexPath.row][@"product_name"];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(void)backNavigationTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)getStringForType:(MealType)type{
    NSString *returnString;
    switch (type) {
        case MealTypeBreakFast:
            returnString = @"breakfast";
            break;
        case MealTypeLunch:
            returnString = @"lunch";
            break;
        case MealTypeSnacks:
            returnString = @"snacks";
            break;
        case MealTypeDinner:
            returnString = @"dinner";
            break;
        default:
            returnString = @"breakfast";
            break;
    }
    return returnString;
}



@end
