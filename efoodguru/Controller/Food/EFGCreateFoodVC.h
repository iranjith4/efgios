//
//  EFGCreateFoodVC.h
//  efoodguru
//
//  Created by Ranjithkumar on 04/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFGNavigationView.h"

@protocol EFGCreateFoodProtocol <NSObject>

- (void)createFoodDataGot :(NSDictionary *)data;

@end

@interface EFGCreateFoodVC : UIViewController <EFGNavigationProtocol>

- (void)initViewControllerWithMealType : (NSString *)mType;
@property (nonatomic, strong) id <EFGCreateFoodProtocol> delgate;


@end
