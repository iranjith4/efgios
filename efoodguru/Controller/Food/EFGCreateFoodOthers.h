//
//  EFGCreateFoodOthers.h
//  efoodguru
//
//  Created by Ranjithkumar on 17/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGCreateFoodOthers : UIViewController

- (void)initControllerWithPreviousData:(NSDictionary *)pData;

@end
