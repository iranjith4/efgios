//
//  EFGFoodVC.h
//  efoodguru
//
//  Created by Ranjithkumar on 03/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFGNavigationView.h"
#import "EFGCreateFoodVC.h"

typedef enum : NSUInteger {
    MealTypeBreakFast,
    MealTypeLunch,
    MealTypeSnacks,
    MealTypeDinner
} MealType;

@interface EFGFoodVC : UIViewController <EFGNavigationProtocol, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate,EFGCreateFoodProtocol>

- (void)initWithFoodType:(MealType)type;

@end
