//
//  EFGCreateFoodVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 04/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGCreateFoodVC.h"
#import "EFGUtilities.h"
#import "EFGTitleLabel.h"
#import "EFGTextField.h"

@implementation EFGCreateFoodVC{
    float xPos;
    float yPos;
    EFGNavigationView *navigationView;
    EFGTitleLabel *newFood;
    EFGTitleLabel *servings;
    EFGTextField *brandName;
    EFGTextField *description;
    EFGTextField *servingSize;
    EFGTextField *servingPerContainer;
    NSString *mealType;
    UIButton *createFood;
}

- (void) viewDidLoad{
    [super viewDidLoad];
}

- (void)initViewControllerWithMealType : (NSString *)mType{
    mealType = mType;
    self.view.backgroundColor = [UIColor whiteColor];
    xPos = 0;
    yPos = STATUSBAR_HEIGHT;
    
    navigationView = [[EFGNavigationView alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 60)];
    [navigationView initViewsWithColor:[EFGUtilities themeColor] title:@"Create Food" andBackText:@"Back"];
    navigationView.delegate = self;
    [self.view addSubview:navigationView];
    yPos += navigationView.frame.size.height + 10;
    
    newFood = [[EFGTitleLabel alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 40) andTitle:@"New Food"];
    [self.view addSubview:newFood];
    yPos += newFood.frame.size.height;
    
    brandName = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 60) labelText:@"Brand Name" placeHolder:@"ex.Campbell's"];
    [self.view addSubview:brandName];
    yPos += brandName.frame.size.height;
    
    description = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 60) labelText:@"Description" placeHolder:@"ex.Chicken Soup"];
    [self.view addSubview:description];
    yPos += description.frame.size.height;
    
    servings = [[EFGTitleLabel alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 40) andTitle:@"Servings"];
    [self.view addSubview:servings];
    yPos += servings.frame.size.height;
    
    servingSize = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 60) labelText:@"Serving Size" placeHolder:@"ex.1 cup"];
    [self.view addSubview:servingSize];
    yPos += servingSize.frame.size.height;
    
    servingPerContainer = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 60) labelText:@"Servings Per Container" placeHolder:@"1"];
    [self.view addSubview:servingPerContainer];
    yPos += servingPerContainer.frame.size.height;

    createFood = [[UIButton alloc] initWithFrame:CGRectMake(10, yPos, self.view.frame.size.width - 2 * 10, 40)];
    [createFood setTitle:@"CFREATE FOOD" forState:UIControlStateNormal];
    [createFood setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    createFood.titleLabel.font = [UIFont systemFontOfSize:16];
    createFood.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    createFood.layer.cornerRadius = 6.0;
    [createFood addTarget:self action:@selector(createFoodTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:createFood];
    yPos += createFood.frame.size.height + 15;
}

- (void)createFoodTapped{
    NSDictionary *data = @{
                           @"username" : [EFGUtilities getUserUsername],
                           @"password" : [EFGUtilities getUserPassword],
                           @"brand_name" : brandName.text.text,
                           @"description" : description.text.text,
                           @"serving_size": servingSize.text.text,
                           @"serving_size_uom" : @"g",//TODO: Need to be get from the UI
                           @"servings_per_container" : servingPerContainer.text.text,
                           @"meal_time" : mealType,
                           @"date_time" : [EFGUtilities getCurrentFormattedDate]
                           };

    [self dismissViewControllerAnimated:NO completion:^{
            [self.delgate createFoodDataGot:data];
    }];
}

-(void)backNavigationTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
