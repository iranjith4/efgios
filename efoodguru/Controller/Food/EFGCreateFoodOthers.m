//
//  EFGCreateFoodOthers.m
//  efoodguru
//
//  Created by Ranjithkumar on 17/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGCreateFoodOthers.h"
#import "EFGUtilities.h"
#import "EFGNavigationView.h"
#import "EFGTitleLabel.h"
#import "EFGTextField.h"
#import "EFGAPIConnector.h"

@interface EFGCreateFoodOthers () <EFGNavigationProtocol>

@end

@implementation EFGCreateFoodOthers{
    float xPos;
    float yPos;
    EFGNavigationView *navigationView;
    UIScrollView *scrollView;
    EFGTitleLabel *title;
    EFGTextField *calories;
    EFGTextField *totalCarbohydrates;
    EFGTextField *protein;
    EFGTextField *totalFat;
    EFGTextField *saturatedFat;
    EFGTextField *polyunsaturatedFat;
    EFGTextField *monosaturated;
    EFGTextField *transFat;
    EFGTextField *cholesterol;
    EFGTextField *sodium;
    EFGTextField *potassium;
    EFGTextField *dietaryFiber;
    EFGTextField *sugars;
    EFGTextField *vitaminA;
    EFGTextField *vitaminC;
    EFGTextField *calcium;
    EFGTextField *iron;
    UIButton *submit;
    NSDictionary *previousData;
    
}

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor whiteColor];
    xPos = 0;
    yPos = STATUSBAR_HEIGHT;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)initControllerWithPreviousData:(NSDictionary *)pData{
    previousData = pData;
    [self loadNavigationView];
    [self addScrollView];
    [self addElements];
    [self addSubmitButton];
}

- (void)loadNavigationView{
    navigationView = [[EFGNavigationView alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 60)];
    [navigationView initViewsWithColor:[EFGUtilities themeColor] title:@"Others Food" andBackText:@"Back"];
    navigationView.delegate = self;
    [self.view addSubview:navigationView];
    
    yPos += navigationView.frame.size.height;
}

- (void)addScrollView{
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, self.view.frame.size.height - yPos- 60)];
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.scrollEnabled = YES;
    [self.view addSubview:scrollView];
}

- (void)addElements{
    yPos = 0;
    float fieldHeight = 45;
    float space = 15;
    // Resetting the Ypos
    title = [[EFGTitleLabel alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) andTitle:@"Nutrition Facts"];
    [scrollView addSubview:title];
    yPos += title.frame.size.height + 10;
    
    
    calories = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Calories" placeHolder:@"Required"];
    [scrollView addSubview:calories];
    yPos+= calories.frame.size.height + space;
    [self addLineinScrollView];
    
    totalCarbohydrates = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Total Carbohydrates (g)" placeHolder:@"Optional"];
    [scrollView addSubview:totalCarbohydrates];
    yPos+= totalCarbohydrates.frame.size.height + space;
    [self addLineinScrollView];
    
    protein = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Protein (g)" placeHolder:@"Optional"];
    [scrollView addSubview:protein];
    yPos+= protein.frame.size.height + space;
    [self addLineinScrollView];
    
    totalFat = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Total Fat (g)" placeHolder:@"Optional"];
    [scrollView addSubview:totalFat];
    yPos+= totalFat.frame.size.height + space;
    [self addLineinScrollView];
    
    saturatedFat = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Saturated Fat (g)" placeHolder:@"Optional"];
    [scrollView addSubview:saturatedFat];
    yPos+= saturatedFat.frame.size.height + space;
    [self addLineinScrollView];
    
    polyunsaturatedFat = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Polyunsaturated Fat (g)" placeHolder:@"Optional"];
    [scrollView addSubview:polyunsaturatedFat];
    yPos+= polyunsaturatedFat.frame.size.height + space;
    [self addLineinScrollView];
    
    
    monosaturated = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Monosaturated (g)" placeHolder:@"Optional"];
    [scrollView addSubview:monosaturated];
    yPos+= monosaturated.frame.size.height + space;
    [self addLineinScrollView];
    
    
    transFat = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Trans Fat (g)" placeHolder:@"Optional"];
    [scrollView addSubview:transFat];
    yPos+= transFat.frame.size.height + space;
    [self addLineinScrollView];
    
    cholesterol = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Cholesterol (mg)" placeHolder:@"Optional"];
    [scrollView addSubview:cholesterol];
    yPos+= cholesterol.frame.size.height + space;
    [self addLineinScrollView];
    
    sodium = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Sodium (mg)" placeHolder:@"Optional"];
    [scrollView addSubview:sodium];
    yPos+= sodium.frame.size.height + space;
    [self addLineinScrollView];
    
    potassium = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Potassium (mg)" placeHolder:@"Optional"];
    [scrollView addSubview:potassium];
    yPos+= potassium.frame.size.height + space;
    [self addLineinScrollView];
    
    dietaryFiber = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Dietary Fiber (g)" placeHolder:@"Optional"];
    [scrollView addSubview:dietaryFiber];
    yPos+= dietaryFiber.frame.size.height + space;
    [self addLineinScrollView];
    
    sugars = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Sugars (g)" placeHolder:@"Optional"];
    [scrollView addSubview:sugars];
    yPos+= sugars.frame.size.height + space;
    [self addLineinScrollView];
    
    vitaminA = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Vitamin A (%)" placeHolder:@"Optional"];
    [scrollView addSubview:vitaminA];
    yPos+= vitaminA.frame.size.height + space;
    [self addLineinScrollView];
    
    vitaminC = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Vitamin C (%)" placeHolder:@"Optional"];
    [scrollView addSubview:vitaminC];
    yPos+= vitaminC.frame.size.height + space;
    [self addLineinScrollView];
    
    calcium = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Calcium (g)" placeHolder:@"Optional"];
    [scrollView addSubview:calcium];
    yPos+= calcium.frame.size.height + space;
    [self addLineinScrollView];
    
    iron = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, fieldHeight) labelText:@"Iron (g)" placeHolder:@"Optional"];
    [scrollView addSubview:iron];
    yPos+= iron.frame.size.height + space;
    [self addLineinScrollView];
    
    
    [self setScrollSize];
}

- (void)addSubmitButton{
    xPos = 15;
    submit = [[UIButton alloc] initWithFrame:CGRectMake(xPos, self.view.frame.size.height - 50, self.view.frame.size.width - 2 * xPos, 40)];
    [submit setTitle:@"SUBMIT" forState:UIControlStateNormal];
    [submit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submit.titleLabel.font = [UIFont systemFontOfSize:16];
    submit.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    submit.layer.cornerRadius = 6.0;
    [submit addTarget:self action:@selector(submitTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submit];
}

- (void)submitTapped{
    
    /*
     Mandatory | username,password,brand_name,
     description,serving_size ,serving_size_uom,
     servings_per_container,meal_time,calories,date_time
     
     Not rmandatory | total_carbohydrates,protein,
     total_fat,saturated_fat,polyunsaturated_fat,monounsaturated,trans_fat,
     cholesterol,sodium,potassium,dietary_fiber,sugars,
     vitamin_A,vitamin_C,calcium,iron
     
     */
    
    if ([self validateField]) {
        NSMutableDictionary *postDict = [[NSMutableDictionary alloc] initWithDictionary:previousData];
        if (calories.text.text != nil && ![calories.text.text  isEqual: @""]) {
            [postDict setObject:calories.text.text forKey:@"calories"];
        }
        
        if (totalCarbohydrates.text.text != nil && ![totalCarbohydrates.text.text  isEqual: @""]) {
            [postDict setObject:totalCarbohydrates.text.text forKey:@"total_carbohydrates"];
        }
        
        if (protein.text.text != nil && ![protein.text.text  isEqual: @""]) {
            [postDict setObject:protein.text.text forKey:@"protein"];
        }
        
        if (totalFat.text.text != nil && ![totalFat.text.text  isEqual: @""]) {
            [postDict setObject:totalFat.text.text forKey:@"total_fat"];
        }
        
        if (saturatedFat.text.text != nil && ![saturatedFat.text.text  isEqual: @""]) {
            [postDict setObject:saturatedFat.text.text forKey:@"saturated_fat"];
        }
        
        if (polyunsaturatedFat.text.text != nil && ![polyunsaturatedFat.text.text  isEqual: @""]) {
            [postDict setObject:polyunsaturatedFat.text.text forKey:@"polyunsaturated_fat"];
        }
        
        if (monosaturated.text.text != nil && ![monosaturated.text.text  isEqual: @""]) {
            [postDict setObject:monosaturated.text.text forKey:@"monounsaturated"];
        }
        
        if (transFat.text.text != nil && ![transFat.text.text  isEqual: @""]) {
            [postDict setObject:transFat.text.text forKey:@"trans_fat"];
        }
        
        if (cholesterol.text.text != nil && ![cholesterol.text.text  isEqual: @""]) {
            [postDict setObject:cholesterol.text.text forKey:@"cholesterol"];
        }
        
        if (sodium.text.text != nil && ![sodium.text.text  isEqual: @""]) {
            [postDict setObject:sodium.text.text forKey:@"sodium"];
        }
        
        if (potassium.text.text != nil && ![potassium.text.text  isEqual: @""]) {
            [postDict setObject:potassium.text forKey:@"potassium"];
        }
        
        if (dietaryFiber.text.text != nil && ![dietaryFiber.text.text  isEqual: @""]) {
            [postDict setObject:dietaryFiber.text.text forKey:@"dietary_fiber"];
        }
        
        if (sugars.text.text != nil && ![sugars.text.text  isEqual: @""]) {
            [postDict setObject:sugars.text.text forKey:@"sugars"];
        }
        
        if (vitaminA.text.text != nil && ![vitaminA.text.text  isEqual: @""]) {
            [postDict setObject:vitaminA.text.text forKey:@"vitamin_A"];
        }
        
        if (vitaminC.text.text != nil && ![vitaminC.text.text  isEqual: @""]) {
            [postDict setObject:vitaminC.text.text forKey:@"vitamin_C"];
        }
        
        if (calcium.text.text != nil && ![calcium.text.text  isEqual: @""]) {
            [postDict setObject:calcium.text.text forKey:@"calcium"];
        }
        
        if (iron.text.text != nil && ![iron.text.text  isEqual: @""]) {
            [postDict setObject:iron.text.text forKey:@"iron"];
        }
        //Calling the API
        
        [EFGUtilities showProgress];
        
        [[EFGAPIConnector sharedManager] saveUserFood:postDict success:^(NSDictionary *dict, NSURLResponse *resp) {
            [self dismissViewControllerAnimated:YES completion:^{
            }];
            [EFGUtilities hideProgress];
            
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [EFGUtilities hideProgress];
            
        }];
    }
}

- (BOOL)validateField{
    if (calories.text.text != nil && ![calories.text.text  isEqual: @""]) {
        return YES;
    }else{
        [EFGUtilities showAlertWithTitleVC:@"Error" message:@"Please fill all the Mandatory fields." actionTitle:@"Ok" inViewController:self];
        return NO;
    }
}

- (void)addLineinScrollView{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [scrollView addSubview:line];
    yPos += 3;
}

- (void)setScrollSize{
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, yPos);
}

-(void)backNavigationTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
