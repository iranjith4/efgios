//
//  EFGFoodQuickAddVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 04/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGFoodQuickAddVC.h"
#import "EFGTextField.h"
#import "EFGUtilities.h"
#import "EFGAPIConnector.h"

@implementation EFGFoodQuickAddVC{
    float xPos;
    float yPos;
    EFGNavigationView *navigationView;
    EFGTextField *calories;
    EFGTextField *carbohydrates;
    EFGTextField *fat;
    EFGTextField *protein;
    UIButton *doneButton;
    NSString *mealType;
}


- (void) viewDidLoad{
    [super viewDidLoad];
}

- (void)initViewController:(NSString *)mtype{
    mealType = mtype;
    self.view.backgroundColor = [UIColor whiteColor];
    xPos = 0;
    yPos = STATUSBAR_HEIGHT;
    
    navigationView = [[EFGNavigationView alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 60)];
    [navigationView initViewsWithColor:[EFGUtilities themeColor] title:@"Create Food" andBackText:@"Back"];
    navigationView.delegate = self;
    [self.view addSubview:navigationView];
    yPos += navigationView.frame.size.height + 10;
    
    calories = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 60) labelText:@"Calories" placeHolder:@"Enter Calorie"];
    [calories setTextFieldBorderStyle:UITextBorderStyleNone];
    [self.view addSubview:calories];
    yPos += calories.frame.size.height;
    
    carbohydrates = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 60) labelText:@"Carbohydrates (g)" placeHolder:@"Enter Carbohydrate"];
    [carbohydrates setTextFieldBorderStyle:UITextBorderStyleNone];
    [self.view addSubview:carbohydrates];
    yPos += carbohydrates.frame.size.height;
    
    fat = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 60) labelText:@"Fat (g)" placeHolder:@"Enter Fat"];
    [fat setTextFieldBorderStyle:UITextBorderStyleNone];
    [self.view addSubview:fat];
    yPos += fat.frame.size.height;
    
    protein = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 60) labelText:@"Protein (g)" placeHolder:@"Enter Protein"];
    [protein setTextFieldBorderStyle:UITextBorderStyleNone];
    [self.view addSubview:protein];
    yPos += protein.frame.size.height;
    
    doneButton = [[UIButton alloc] initWithFrame:CGRectMake(10, yPos, self.view.frame.size.width - 2 * 10, 40)];
    [doneButton setTitle:@"DONE" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.titleLabel.font = [UIFont systemFontOfSize:16];
    doneButton.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    [doneButton addTarget:self action:@selector(doneButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    doneButton.layer.cornerRadius = 6.0;
    [self.view addSubview:doneButton];
    yPos += doneButton.frame.size.height + 15;
}

-(void)backNavigationTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)doneButtonTapped{
    if ([self validateFields]) {
        
        NSDictionary *params = @{
                                 @"username" : [EFGUtilities getUserUsername],
                                 @"password" : [EFGUtilities getUserPassword],
                                 @"calories" : calories.text.text,
                                 @"carbohydrates" : carbohydrates.text.text,
                                 @"fat" : fat.text.text,
                                 @"protein" : protein.text.text,
                                 @"date_time" : [EFGUtilities getCurrentFormattedDate],
                                 @"meal_time" : mealType
                                 };
        
        [EFGUtilities showProgress];
        [[EFGAPIConnector sharedManager] quickAddCalories:params success:^(NSDictionary *dict, NSURLResponse *resp) {
            NSLog(@"%@",dict);
            [EFGUtilities hideProgress];
            [self dismissViewControllerAnimated:YES completion:^{
                [EFGUtilities showAlertWithTitle:@"Done" message:@"Calories Added" actionTitle:@"Ok" inViewController:self];
            }];
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [EFGUtilities hideProgress];
            
        }];
    }
}

- (BOOL)validateFields{
    if (calories.text.text != nil && ![calories.text.text  isEqual: @""] && carbohydrates.text.text != nil && ![carbohydrates.text.text  isEqual: @""] && fat.text.text != nil && ![fat.text.text  isEqual: @""] && protein.text.text != nil && ![protein.text.text  isEqual: @""]) {
        return YES;
    }else{
        return NO;
    }
}



@end
