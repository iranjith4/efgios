//
//  EFGLogoutController.m
//  efoodguru
//
//  Created by Ranjithkumar on 22/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGLogoutController.h"
#import "EFGUtilities.h"
#import "MMDrawerBarButtonItem.h"

@interface EFGLogoutController ()

@end

@implementation EFGLogoutController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupMenuController];
    [self loadPage];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Reveal Menu Bar

- (void)setupMenuController{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}
- (void)loadPage {
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *logoutButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width * 0.80, 50)];
    [logoutButton setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutButton.backgroundColor = [EFGUtilities themeButtonRedColor];
    [logoutButton addTarget:self action:@selector(logoutTapped) forControlEvents:UIControlEventTouchUpInside];
    logoutButton.layer.cornerRadius = 6.0;
    logoutButton.center = self.view.center;
    [self.view addSubview:logoutButton];
}

- (void)logoutTapped{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"LOGOUT_CALLED" object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
