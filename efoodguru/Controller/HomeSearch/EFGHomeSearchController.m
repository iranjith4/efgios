//
//  EFGHomeSearchController.m
//  efoodguru
//
//  Created by Ranjithkumar on 25/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGHomeSearchController.h"
#import "EFGUtilities.h"
#import "EFGAPIConnector.h"
#import "SVProgressHUD.h"
#import "TSMessage.h"

@interface EFGHomeSearchController (){
    float xPos;
    float yPos;
    UIButton *closeButton;
    UITableView *searchTable;
    NSArray *searchArray;
}

@end

@implementation EFGHomeSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initSetUpForQuery:(NSString *)query{
    xPos = 0;
    yPos = STATUSBAR_HEIGHT + NAVIGATIONBAR_HEIGHT + 10;
    
    searchArray = [[NSArray alloc] init];
    
    closeButton = [[UIButton alloc] initWithFrame:CGRectMake(15, yPos, self.view.frame.size.width -15, 40)];
    [closeButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [closeButton setTitleColor:[EFGUtilities themeButtonBlueColor] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeController) forControlEvents:UIControlEventTouchUpInside];
    [closeButton sizeToFit];
    [self.view addSubview:closeButton];
    yPos += closeButton.frame.size.height + 5;
    
    searchTable = [[UITableView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, self.view.frame.size.height - yPos - 30) style:UITableViewStylePlain];
    searchTable.delegate = self;
    searchTable.dataSource = self;
    searchTable.rowHeight = UITableViewAutomaticDimension;
    [self.view addSubview:searchTable];
    
    
    [SVProgressHUD show];
    [[EFGAPIConnector sharedManager] productSearchResult:query success:^(NSDictionary *dict, NSURLResponse *resp) {
        NSLog(@"succ %@",dict);
        [SVProgressHUD dismiss];
        if ([dict[@"response"][@"success"] intValue] == 0) {
            [self dismissViewControllerAnimated:YES completion:^{
                [TSMessage showNotificationWithTitle:@"Sorry, No Match found for your search." type:TSMessageNotificationTypeWarning];
            }];
        }else{
            searchArray = dict[@"data"][@"productsArray"];
            if (searchArray.count) {
                [searchTable reloadData];
            }else{
                [self dismissViewControllerAnimated:YES completion:^{
                    [TSMessage showNotificationWithTitle:@"Sorry, No Match found for your search." type:TSMessageNotificationTypeWarning];
                }];
            }
        }
    } failure:^(NSHTTPURLResponse *resp, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"fail %@",resp);
    }];
}

- (void)closeController{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    cell.textLabel.text = [searchArray objectAtIndex:indexPath.row][@"product_name"];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return searchArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate selectedUPC:[searchArray objectAtIndex:indexPath.row][@"upc"]];
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
