//
//  EFGHomeSearchController.h
//  efoodguru
//
//  Created by Ranjithkumar on 25/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EFGSearchControlProtocol <NSObject>

- (void)selectedUPC:(NSString *)upc;

@end

@interface EFGHomeSearchController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) id <EFGSearchControlProtocol> delegate;
- (void)initSetUpForQuery:(NSString *)query;

@end
