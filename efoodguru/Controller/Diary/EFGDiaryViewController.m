//
//  EFGDiaryViewController.m
//  efoodguru
//
//  Created by Ranjithkumar on 08/01/16.
//  Copyright © 2016 EFG. All rights reserved.
//

#import "EFGDiaryViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"
#import "EFGTimeIntervalSelector.h"
#import "EFGUtilities.h"
#import "THDatePickerViewController.h"
#import "EFGAPIConnector.h"
#import "EFGGoalsStatus.h"
#import "EFGDataPresentLabel.h"
#import "EFGFoodVC.h"
#import "EFGWaterVC.h"

@interface EFGDiaryViewController ()<EFGTimeIntervalProtocol,THDatePickerDelegate>

@end

@implementation EFGDiaryViewController{
    float xPos;
    float yPos;
    UIScrollView *scroll;
    EFGTimeIntervalSelector *timeSelector;
    NSString *dateForAPI;
    EFGGoalsStatus *calorieView;
    NSMutableArray *labels;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Diary";
    scroll.backgroundColor = [UIColor whiteColor];
    [self setupMenuController];
    [self setupViewController];
    [self addCalorieStatusView];
    [self addLabel];
    [self setScrollSize];
    [self callDiaryAPI];
    // Do any additional setup after loading the view.
}

- (void)setupViewController{
    yPos = 0;
    xPos = 0;
    labels = [[NSMutableArray alloc] init];
    dateForAPI = [EFGUtilities getCurrentFormattedDate:[NSDate date] inFormat:@"yyyy-MM-dd"];
    
    scroll = [[UIScrollView alloc] initWithFrame:self.view.frame];
    scroll.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:scroll];
    
    timeSelector = [[EFGTimeIntervalSelector alloc] initWithFrame:CGRectMake(0, yPos, scroll.frame.size.width, 50)];
    timeSelector.backgroundColor = [EFGUtilities themeColor];
    [timeSelector.back setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [timeSelector.forward setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [timeSelector.timeInterval setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    timeSelector.delegate = self;
    [scroll addSubview:timeSelector];
    [timeSelector setType:TimeTypeDate];
    yPos += timeSelector.frame.size.height + 5;
}

- (void)addCalorieStatusView{
    xPos = 15;
    calorieView = [[EFGGoalsStatus alloc] initWithFrame:CGRectMake(xPos, yPos, scroll.frame.size.width * 0.90, scroll.frame.size.height * 0.15)];
    [scroll addSubview:calorieView];
    calorieView.layer.cornerRadius = 4.0;
    calorieView.layer.borderColor = [EFGUtilities themeColor].CGColor;
    calorieView.layer.borderWidth = 1.0;
    [calorieView setGoal:@""];
    [calorieView setFood:@""];
    [calorieView setExercise:@""];
    [calorieView setRemaining:@""];
    yPos += calorieView.frame.size.height + 10;
}

- (void)addLabel{
    xPos = 0;
    NSArray *titleArray = @[@"Breakfast",@"Lunch",@"Snacks",@"Dinner",@"Water"];
    NSArray *buttonArray = @[@"Add Breakfast",@"Add Lunch",@"Add Snacks",@"Add Dinner",@"Add Water"];
    int tag = 0;
    for (NSString *title in titleArray) {
        EFGDataPresentLabel *label = [[EFGDataPresentLabel alloc] initWithFrame:CGRectMake(xPos, yPos, scroll.frame.size.width - 2 * xPos, 50) leftData:title rightData:@"0"];
        label.backgroundColor = [EFGUtilities themeColor];
        label.leftData.textColor = [UIColor whiteColor];
        label.rightData.textColor = [UIColor whiteColor];
        label.tag = tag;
        [scroll addSubview:label];
        yPos += label.frame.size.height + 5;
        
        UIButton *add = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, scroll.frame.size.width - 2* xPos, 40)];
        [add setTitle:[buttonArray objectAtIndex:tag] forState:UIControlStateNormal];
        [add setTitleColor:[EFGUtilities themeColor] forState:UIControlStateNormal];
        [add addTarget:self action:@selector(addFoodData:) forControlEvents:UIControlEventTouchUpInside];
        add.titleLabel.textAlignment = NSTextAlignmentLeft;
        add.tag = tag;
        [scroll addSubview:add];
        yPos += add.frame.size.height + 5;
        tag++;
        
        [labels addObject:label];
    }
}

- (void)setScrollSize{
    scroll.contentSize = CGSizeMake(scroll.frame.size.width, yPos);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadDayView:(NSString *)date{
    NSLog(@"API Date : %@",date);
    dateForAPI = date;
    [self callDiaryAPI];
}


-(void)timeTitleTapped{
    THDatePickerViewController *datePicker = [THDatePickerViewController datePicker];
    datePicker.date = [NSDate date];
    datePicker.delegate = self;
    [datePicker setAllowClearDate:NO];
    [datePicker setClearAsToday:YES];
    [datePicker setAutoCloseOnSelectDate:YES];
    [datePicker setAllowSelectionOfSelectedDate:YES];
    [datePicker setDisableFutureSelection:YES];
    [datePicker setSelectedBackgroundColor:[UIColor colorWithRed:125/255.0 green:208/255.0 blue:0/255.0 alpha:1.0]];
    [datePicker setCurrentDateColor:[UIColor colorWithRed:242/255.0 green:121/255.0 blue:53/255.0 alpha:1.0]];
    [datePicker setDateHasItemsCallback:^BOOL(NSDate *date) {
        int tmp = (arc4random() % 30)+1;
        if(tmp % 5 == 0)
            return YES;
        return NO;
    }];
    //[self.datePicker slideUpInView:scroll withModalColor:[UIColor lightGrayColor]];
    [self presentSemiViewController:datePicker withOptions:@{
                                                             KNSemiModalOptionKeys.pushParentBack    : @(NO),
                                                             KNSemiModalOptionKeys.animationDuration : @(0.3),
                                                             KNSemiModalOptionKeys.shadowOpacity     : @(0.3),
                                                             }];
}


-(void)datePickerCancelPressed:(THDatePickerViewController *)datePicker{
    [datePicker dismissSemiModalViewWithCompletion:^{
        
    }];
}

- (void)datePickerDonePressed:(THDatePickerViewController *)datePicker{
    [timeSelector changeToDate:datePicker.date];
    [datePicker dismissSemiModalViewWithCompletion:^{
        
    }];
}

#pragma mark - Reveal Menu Bar

- (void)setupMenuController{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma mark - Network Call and UI Update after

- (void)callDiaryAPI{
    NSDictionary *params = @{
                             @"username" : [EFGUtilities getUserUsername],
                             @"password" : [EFGUtilities getUserPassword],
                             @"date_time" : dateForAPI,
                             };
    [[EFGAPIConnector sharedManager] getDiaryDetails:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        if ([dict[@"response"][@"success"] intValue] == 1) {
            [self updateRemainingCalories:dict[@"data"][@"remaining_calories"]];
            [self updateLabelData:dict[@"data"][@"diary_details"]];
        }else{
            [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went Wrong. Please Try again little later." actionTitle:@"Ok" inViewController:self type:2];
        }
    } failure:^(NSHTTPURLResponse *resp, NSError *error) {
        [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went Wrong. Please Try again little later." actionTitle:@"Ok" inViewController:self type:0];
    }];
}

- (void)updateRemainingCalories:(NSDictionary *)data{
    [calorieView setGoal:[NSString stringWithFormat:@"%@",data[@"goal"]]];
    [calorieView setExercise:[NSString stringWithFormat:@"%@",data[@"exercise"]]];
    [calorieView setFood:[NSString stringWithFormat:@"%@",data[@"food"]]];
    [calorieView setRemaining:[NSString stringWithFormat:@"%@",data[@"remaining"]]];
}

- (void)updateLabelData:(NSDictionary *)data{
    int a = 0;
    for (EFGDataPresentLabel *l in labels) {
        switch (a) {
            case 0:
                l.rightData.text = [NSString stringWithFormat:@"%@",data[@"breakfast"]];
                break;
                
            case 1:
                l.rightData.text = [NSString stringWithFormat:@"%@",data[@"lunch"]];
                break;
                
            case 2:
                l.rightData.text = [NSString stringWithFormat:@"%@",data[@"snacks"]];
                break;
                
            case 3:
                l.rightData.text = [NSString stringWithFormat:@"%@",data[@"dinner"]];
                break;
                
            case 4:
                l.rightData.text = [NSString stringWithFormat:@"%@",data[@"water"]];
                break;
                
            default:
                break;
        }
        a++;
    }
}

- (void)addFoodData:(UIButton *)sender{
    int tag = (int)sender.tag;
    EFGFoodVC *food = [[EFGFoodVC alloc] init];
    switch (tag) {
        case 0:{
            [food initWithFoodType:MealTypeBreakFast];
        }
            
            break;
        case 1:{
            [food initWithFoodType:MealTypeLunch];
        }
            
            break;
            
        case 2:{
            [food initWithFoodType:MealTypeSnacks];
        }
            
            break;
            
        case 3:{
            [food initWithFoodType:MealTypeDinner];
        }
            break;
            
        case 4:{
            //add Water
        }
            
        default:
            [food initWithFoodType:MealTypeBreakFast];
            break;
    }
    
    
    if (tag == 4) {
        //open water
        EFGWaterVC *waterVC = [[EFGWaterVC alloc] init];
        [self presentViewController:waterVC animated:YES completion:nil];
    }else{
        [self presentViewController:food animated:YES completion:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
