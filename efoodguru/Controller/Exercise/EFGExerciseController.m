//
//  EFGExerciseController.m
//  efoodguru
//
//  Created by Ranjithkumar on 25/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGExerciseController.h"
#import "EFGUtilities.h"
#import "EFGAPIConnector.h"
#import "EFGAddExerciseVC.h"

@interface EFGExerciseController (){
    float xPos;
    float yPos;
    ExerciseType type;
    UISegmentedControl *segment;
    UISearchBar *searchBar;
    UITableView *searchTable;
    NSArray *searchData;
    UIButton *createNewButton;
    NSArray *cardioData;
    NSArray *strengthData;
    NSArray *myActivities;
}

@end

@implementation EFGExerciseController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadViewControllerFor:(ExerciseType)t{
    type = t;
    [self getAPIData];
}

- (void)getAPIData{
    NSDictionary *params = @{
        @"username"  : [EFGUtilities getUserUsername],
        @"password" : [EFGUtilities getUserPassword],
        @"category" : [self getStringForExerciseType:type],
    };
    
    [EFGUtilities showProgress];
    [[EFGAPIConnector sharedManager] getAdminExercise:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        [EFGUtilities hideProgress];
        NSLog(@"%@",dict);
        if ([dict[@"response"][@"success"] boolValue]) {
            [self parseDatas:dict];
            [self initController];
        }else{
            [EFGUtilities serverError];
        }
    } failure:^(NSHTTPURLResponse *resp, NSError *err) {
        [EFGUtilities hideProgress];
        [EFGUtilities serverError];
    }];
}

- (void)parseDatas : (NSDictionary *)d{
    NSArray *exercises = d[@"data"];
    NSMutableArray *cardioMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray *strengthMutableArray = [[NSMutableArray alloc] init];
    for (NSDictionary *exercise in exercises) {
        if ([exercise[@"category"] isEqualToString:@"Cardio"]) {
            [cardioMutableArray addObject:exercise];
        }else if ([exercise[@"category"] isEqualToString:@"Strength"]){
            [strengthMutableArray addObject:exercise];
        }
    }
    cardioData = [cardioMutableArray mutableCopy];
    strengthData = [strengthMutableArray mutableCopy];
}

- (void) initController{
    self.view.backgroundColor = [UIColor whiteColor];
    xPos = 0;
    yPos = NAVIGATIONBAR_HEIGHT + STATUSBAR_HEIGHT + 10 ;
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(10, yPos, self.view.frame.size.width - 20, 35)];
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchBar.placeholder = @"Search for Exercise";
    searchBar.delegate = self;
    searchBar.tintColor = [EFGUtilities themeColorSegmentRed];
    [self.view addSubview:searchBar];
    
    yPos += searchBar.frame.size.height + 10;
    
    segment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"All",@"My Activities",nil]];
    segment.frame = CGRectMake(20, yPos, self.view.frame.size.width - 40, 30);
    segment.tintColor = [EFGUtilities themeColorSegmentRed];
    [segment addTarget:self action:@selector(segmentChanged:) forControlEvents: UIControlEventValueChanged];
    segment.selectedSegmentIndex = 0;
    [self.view addSubview:segment];
    yPos += segment.frame.size.height + 10;
    
    searchData = [[NSArray alloc] init];
    
    searchTable = [[UITableView alloc] initWithFrame:CGRectMake(15, yPos, self.view.frame.size.width - 30, self.view.frame.size.height - yPos - 100)];
    searchTable.dataSource = self;
    searchTable.delegate = self;
    searchTable.layer.cornerRadius = 7.0;
    searchTable.layer.borderColor = [UIColor blackColor].CGColor;
    searchTable.layer.borderWidth = 1.0;
    [self.view addSubview:searchTable];
    
    yPos += searchTable.frame.size.height + 20;
    
    createNewButton = [[UIButton alloc] initWithFrame:CGRectMake(15, yPos, self.view.frame.size.width - 30, 40)];
    createNewButton.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    [createNewButton setTitle:@"Create New Activity" forState:UIControlStateNormal];
    [createNewButton addTarget:self action:@selector(createNewExercise) forControlEvents:UIControlEventTouchUpInside];
    createNewButton.layer.cornerRadius = 7.0;
    [self.view addSubview:createNewButton];
}

#pragma mark - Search Delegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)s{
    [s resignFirstResponder];
}

- (void)createNewExercise{
    EFGAddExerciseVC *addExercise = [[EFGAddExerciseVC alloc] init];
    [addExercise initViewsWithExerciseType:[self getStringForExerciseType:type]];
    [self presentViewController:addExercise animated:NO completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (segment.selectedSegmentIndex == 0) {
        if (type == ExerciseTypeCardiac) {
            return cardioData.count;
        }else{
            return strengthData.count;
        }
    }else{
        return myActivities.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    if (segment.selectedSegmentIndex == 0) {
        if (type == ExerciseTypeCardiac) {
            cell.textLabel.text = [cardioData objectAtIndex:indexPath.row][@"exercise_name"];
        }else{
            cell.textLabel.text = [strengthData objectAtIndex:indexPath.row][@"exercise_name"];
        }
    }else{
        cell.textLabel.text = [myActivities objectAtIndex:indexPath.row];
    }
    return cell;
}

- (void)segmentChanged:(UISegmentedControl *)seg{
    [searchTable reloadData];
}

- (NSString *)getStringForExerciseType:(ExerciseType)t{
    switch (t) {
        case ExerciseTypeCardiac:
            return @"cardio";
            break;
        case ExerciseTypeStrength:
            return @"strength";
            break;
        default:
            return @"cardio";
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
