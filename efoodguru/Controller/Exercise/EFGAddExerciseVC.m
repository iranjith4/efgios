//
//  EFGAddExerciseVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 01/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGAddExerciseVC.h"
#import "EFGUtilities.h"
#import "EFGNavigationView.h"
#import "EFGAPIConnector.h"

@interface EFGAddExerciseVC ()

@end

@implementation EFGAddExerciseVC{
    UITextField *activityName;
    UILabel * calorieSpentLabel;
    UILabel * timeLabel;
    UITextField * calorieSpent;
    UITextField * time;
    UILabel * note;
    UIButton *doneButton;
    float xPos;
    float yPos;
    NSString *exerciseType;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
}

- (void)initViewsWithExerciseType:(NSString *)type {
    exerciseType = type;
    xPos = 0;
    yPos = 0;
    
    EFGNavigationView *navigationBar = [[EFGNavigationView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 80)];
    [navigationBar initViewsWithColor:[EFGUtilities themeColor] title:nil andBackText:@"< Fitness"];
    navigationBar.delegate = self;
    [self.view addSubview:navigationBar];
    
    yPos += navigationBar.frame.size.height + 10;
    
    activityName = [[UITextField alloc] initWithFrame:CGRectMake(15, yPos, self.view.frame.size.width - 30, 40)];
    activityName.placeholder = @"Type Activity Name Here";
    activityName.textAlignment = NSTextAlignmentCenter;
    activityName.delegate = self;
    [self.view addSubview:activityName];
    
    yPos += activityName.frame.size.height + 2;
    
    [self addALine];
    
    yPos += 30;
    
    
    calorieSpentLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, yPos, (self.view.frame.size.width - 30) * 0.40, 35)];
    calorieSpentLabel.text = @"Calories Spent";
    calorieSpentLabel.textColor = [UIColor darkGrayColor];
    calorieSpentLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:calorieSpentLabel];
    
    xPos = calorieSpentLabel.frame.origin.x + calorieSpentLabel.frame.size.width + 5;
    calorieSpent = [[UITextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - 15, 35)];
    calorieSpent.placeholder = @"200 kCal";
    calorieSpent.textColor = [UIColor grayColor];
    calorieSpent.delegate = self;
    calorieSpent.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    calorieSpent.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:calorieSpent];
    
    [self addALineForField:calorieSpent];
    
    yPos +=  65;
    
    timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, yPos, (self.view.frame.size.width - 30) * 0.40, 35)];
    timeLabel.text = @"Time";
    timeLabel.textColor = [UIColor darkGrayColor];
    timeLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:timeLabel];
    
    xPos = timeLabel.frame.origin.x + timeLabel.frame.size.width + 5;
    time = [[UITextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - 15, 35)];
    time.placeholder = @"20 Mins";
    time.textColor = [UIColor grayColor];
    time.delegate = self;
    time.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    time.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:time];
    
    [self addALineForField:time];
    
    yPos += time.frame.size.height + 60;
    
    note = [[UILabel alloc] initWithFrame:CGRectMake(15, yPos, self.view.frame.size.width - 15, 0)];
    note.text = @"* Provide time in Minutes. Remember 1h = 60 mins \n\n\n ** Please select your average heartrate for precise calories.";
    note.font = [UIFont systemFontOfSize:15];
    note.textColor = [UIColor grayColor];
    note.numberOfLines = 0;
    note.lineBreakMode = NSLineBreakByWordWrapping;
    [note sizeToFit];
    [self.view addSubview:note];
    
    yPos += note.frame.size.height + 15;
    
    doneButton = [[UIButton alloc] initWithFrame:CGRectMake(15, yPos, self.view.frame.size.width - 30, 40)];
    [doneButton setTitle:@"Create New Activity" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    doneButton.layer.cornerRadius = 6.0;
    [doneButton addTarget:self action:@selector(doneSelected) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:doneButton];
}

- (void)doneSelected{
    if ([self validateFields]) {
        NSDictionary *dict = @{
                               @"username" : [EFGUtilities getUserUsername],
                               @"password" : [EFGUtilities getUserPassword],
                               @"exercise_name" : activityName.text,
                               @"duration" : time.text,
                               @"calories_burnt" : calorieSpent.text,
                               @"date_time" : [EFGUtilities getCurrentFormattedDate],
                               @"category" : exerciseType
                               };
        NSLog(@"Adding Custom Exercise : %@",dict);
        [[EFGAPIConnector sharedManager] saveUserCustomExercise:dict success:^(NSDictionary *dict, NSURLResponse *resp) {
            NSLog(@"Ex : %@",dict);
            if ([dict[@"response"][@"success"] boolValue]) {
                [self dismissViewControllerAnimated:YES completion:^{
                    [EFGUtilities showAlertWithTitle:@"Success !" message:dict[@"data"] actionTitle:@"ok" inViewController:self type:1];
                }];
            }else{
                [self dismissViewControllerAnimated:YES completion:^{
                   [EFGUtilities serverError];
                }];
                
            }
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [self dismissViewControllerAnimated:YES completion:^{
                [EFGUtilities serverError];
            }];
        }];
    }
}

- (BOOL)validateFields{
    if (![activityName.text isEqual: @""] && activityName.text != nil) {
        if (![calorieSpent.text isEqual:@""] && calorieSpent.text != nil) {
            if (![time.text isEqual:@""] && time.text != nil) {
                return YES;
            }else{
                [EFGUtilities showAlertWithTitleVC:@"Error !" message:@"Please Fill the Time." actionTitle:@"Ok" inViewController:self];
                return NO;
            }
            
        }else{
            [EFGUtilities showAlertWithTitleVC:@"Error !" message:@"Please Fill the Calories." actionTitle:@"Ok" inViewController:self];
            return NO;
        }
        
    }else{
        [EFGUtilities showAlertWithTitleVC:@"Error !" message:@"Activiy Name is Empty." actionTitle:@"Ok" inViewController:self];
        return NO;
    }
}

- (void)addALine{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(15, yPos, self.view.frame.size.width - 30, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:line];
    yPos += line.frame.size.height;
}

- (void)addALineForField : (UITextField *)textField{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(textField.frame.origin.x, textField.frame.origin.y + textField.frame.size.height, textField.frame.size.width, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:line];
    yPos += line.frame.size.height;
}

-(void)backNavigationTapped{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - Text Field Delegated
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
