//
//  EFGAddExerciseVC.h
//  efoodguru
//
//  Created by Ranjithkumar on 01/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFGNavigationView.h"

@interface EFGAddExerciseVC : UIViewController <EFGNavigationProtocol, UITextFieldDelegate>

- (void)initViewsWithExerciseType:(NSString *)type ;

@end
