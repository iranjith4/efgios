//
//  EFGExerciseController.h
//  efoodguru
//
//  Created by Ranjithkumar on 25/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    ExerciseTypeCardiac,
    ExerciseTypeStrength,
} ExerciseType;

@interface EFGExerciseController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

- (void)loadViewControllerFor:(ExerciseType)t;


@end
