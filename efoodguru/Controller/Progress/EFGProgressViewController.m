//
//  EFGProgressViewController.m
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGProgressViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"
#import "EFGTitleLabel.h"
#import "EFGUtilities.h"
#import "PNChart.h"
#import "SBPickerSelector.h"
#import "EFGAPIConnector.h"

@interface EFGProgressViewController ()<SBPickerSelectorDelegate>{
    
}

@end

@implementation EFGProgressViewController{
    float xPos;
    float yPos;
    EFGTitleLabel *historyLabel;
    PNLineChart * lineChart;
    UIButton *monthButton;
    NSString *currentMonth;
    NSString *currentYear;
    NSString *displayMonth;
    NSString *displayTimeText;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self initViewController];
    [self setupMenuController];
    [self updateProgressData];
}

- (void)initViewController {
    
    //Title
    self.title = @"Progress";
    
    yPos += STATUSBAR_HEIGHT + NAVIGATIONBAR_HEIGHT;
    xPos = 0;
    
    historyLabel = [[EFGTitleLabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 45) andTitle:@"HISTORY"];
    [self.view addSubview:historyLabel];
    
    yPos += historyLabel.frame.size.height + 10;
    xPos = 15;
    
    // Month Button
    //Segment Control
//    monthSelector = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"1M",@"2M",@"3M",nil]];
//    monthSelector.frame = CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 35);
//    monthSelector.tintColor = [EFGUtilities themeColor];
//    monthSelector.selectedSegmentIndex = 0;
//    [monthSelector addTarget:self action:@selector(monthChanged:) forControlEvents:UIControlEventValueChanged];
//    [self.view addSubview:monthSelector];
    
   // yPos += monthSelector.frame.size.height + 10;
    
    [self setupDates:[NSDate date]];
    monthButton = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 40)];
    [monthButton setTitleColor:[EFGUtilities themeColor] forState:UIControlStateNormal];
    monthButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [monthButton setTitle:displayTimeText forState:UIControlStateNormal];
    [monthButton addTarget:self action:@selector(openDateSelector) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:monthButton];
    yPos += monthButton.frame.size.height + 10;
    [self addCharts];
}

- (void)setupDates:(NSDate *)dt{
    currentYear = [EFGUtilities getLocalYear:dt];
    currentMonth = [EFGUtilities getLocalMonth:dt];
    displayMonth = [EFGUtilities getLocalMonthName:dt];
    displayTimeText = [NSString stringWithFormat:@"%@, %@",displayMonth,currentYear];
}

- (void)openDateSelector{
    SBPickerSelector *picker = [[SBPickerSelector alloc] init];
    picker.pickerType = SBPickerSelectorTypeDate;
    picker.datePickerType = SBPickerSelectorDateTypeOnlyMonthAndYear;
    picker.delegate = self;
    [picker showPickerOver:self];
}

- (void)addCharts{
    lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(0, yPos, SCREEN_WIDTH, 200.0)];
    [lineChart setXLabels:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"]];
    
    // Line Chart No.1
    NSArray * data01Array = @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0];;
    PNLineChartData *data01 = [PNLineChartData new];
    data01.color = [EFGUtilities themeColor];
    data01.itemCount = lineChart.xLabels.count;
    data01.inflexionPointStyle = PNLineChartPointStyleCircle;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [data01Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    
    lineChart.chartData = @[data01];
    [lineChart strokeChart];
    [self.view addSubview:lineChart];
}

- (void)updateProgressData{
    NSDictionary *params = @{
                             @"username" : [EFGUtilities getUserUsername],
                             @"password" : [EFGUtilities getUserPassword],
                             @"date_time" : [EFGUtilities getCurrentFormattedDate],
                             @"month" : currentMonth,
                             @"year" : currentYear
                             };
    [[EFGAPIConnector sharedManager] getProgressMonthView:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        if ([dict[@"response"][@"success"] intValue] == 1) {
            [self updateGraph:dict[@"data"][@"graph_data"]];
        }else{
            [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went Wrong. Please Try again little later." actionTitle:@"Ok" inViewController:self type:2];
        }
    } failure:^(NSHTTPURLResponse *resp, NSError *error) {
        [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went Wrong. Please Try again little later." actionTitle:@"Ok" inViewController:self type:0];
        
    }];
}

- (void)updateGraph:(NSArray *)graphData{
    NSArray *keys;
    NSArray *values;
    if (graphData.count) {
        NSMutableDictionary *dt = [[NSMutableDictionary alloc] init];
        for (NSDictionary *d in graphData) {
            NSString *date = [d[@"date"] substringFromIndex: [d[@"date"] length] - 2];
            [dt setObject:[NSNumber numberWithFloat:[d[@"calories"] floatValue]] forKey:date];
        }
        keys = dt.allKeys;
        values = dt.allValues;
    }else{
        keys = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"];
        values = @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0];
        [EFGUtilities showAlertWithTitle:@"No Data" message:@"Sorry ! No Calorie data found for this month." actionTitle:@"OK" inViewController:self type:2];
    }
    
    PNLineChartData *data01 = [PNLineChartData new];
    data01.color = PNFreshGreen;
    data01.itemCount = values.count;
    data01.inflexionPointStyle = PNLineChartPointStyleCircle;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [values[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    [lineChart setXLabels:keys];
    [lineChart updateChartData:@[data01]];
}

//- (void) monthChanged:(UISegmentedControl *)sender{
//    
//    
//    switch (sender.selectedSegmentIndex) {
//        case 0:
//            //1Month
//        {
//            NSArray * data01Array = @[@60.1, @160.1, @126.4, @262.2, @186.2,@60.1, @160.1, @126.4, @262.2, @186.2];
//            PNLineChartData *data01 = [PNLineChartData new];
//            data01.color = PNFreshGreen;
//            data01.itemCount = data01Array.count;
//            data01.inflexionPointStyle = PNLineChartPointStyleCircle;
//            data01.getData = ^(NSUInteger index) {
//                CGFloat yValue = [data01Array[index] floatValue];
//                return [PNLineChartDataItem dataItemWithY:yValue];
//            };
//            [lineChart setXLabels:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"]];
//            [lineChart updateChartData:@[data01]];
//        }
//            
//            
//            break;
//            
//        case 1:
//            //2Month
//        {
//            NSArray * data01Array = @[@23.1, @14.1, @75.4, @154.2, @186.2,@60.1, @160.1, @126.4, @262.2, @186.2];
//            PNLineChartData *data01 = [PNLineChartData new];
//            data01.color = PNFreshGreen;
//            data01.itemCount = data01Array.count;
//            data01.inflexionPointStyle = PNLineChartPointStyleCircle;
//            data01.getData = ^(NSUInteger index) {
//                CGFloat yValue = [data01Array[index] floatValue];
//                return [PNLineChartDataItem dataItemWithY:yValue];
//            };
//            [lineChart setXLabels:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"]];
//            [lineChart updateChartData:@[data01]];
//        }
//            break;
//            
//        case 2:
//            //3Month
//        {
//            NSArray * data01Array = @[@60.1, @160.1, @126.4, @262.2, @186.2,@60.1, @10.1, @16.4, @22.2, @86.2];
//            PNLineChartData *data01 = [PNLineChartData new];
//            data01.color = [EFGUtilities themeColor];
//            data01.inflexionPointStyle = PNLineChartPointStyleCircle;
//            data01.itemCount = lineChart.xLabels.count;
//            data01.getData = ^(NSUInteger index) {
//                CGFloat yValue = [data01Array[index] floatValue];
//                return [PNLineChartDataItem dataItemWithY:yValue];
//            };
//            [lineChart setXLabels:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"]];
//            [lineChart updateChartData:@[data01]];
//        }
//            
//            break;
//            
//        default:
//            break;
//    }
//}

#pragma mark - Picker View Delegate

-(void)pickerSelector:(SBPickerSelector *)selector dateSelected:(NSDate *)date{
    NSLog(@"%@ and %@",[EFGUtilities getLocalMonth:date],[EFGUtilities getLocalMonthName:date]);
    [self setupDates:date];
    [monthButton setTitle:displayTimeText forState:UIControlStateNormal];
    [self updateProgressData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Reveal Menu Bar

- (void)setupMenuController{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
