//
//  EFGGoalCalorieMacroVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 02/01/16.
//  Copyright © 2016 EFG. All rights reserved.
//

#import "EFGGoalCalorieMacroVC.h"
#import "EFGUtilities.h"
#import "EFGTextField.h"
#import "EFGAPIConnector.h"

@interface EFGGoalCalorieMacroVC ()<UITextFieldDelegate>

@end

@implementation EFGGoalCalorieMacroVC{
    float xPos;
    float yPos;
    UIScrollView *scroll;
    EFGTextField *calories;
    EFGTextField *carbohydrates;
    EFGTextField *protein;
    EFGTextField *fat;
    UIButton *saveButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initViewController];
    [self addTextFields];
    [self getCalorieMacroData];
    // Do any additional setup after loading the view.
}

- (void)initViewController{
    self.title = @"Calories and Macros";
    xPos = 0;
    yPos = 0;
    
    //Adding the Scroll View
    scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, self.view.frame.size.height - yPos)];
    scroll.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:scroll];
}

- (void)addTextFields{
    yPos = 0;
    
    calories = [[EFGTextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 50) labelText:@"Calorie" placeHolder:@"0"];
    calories.text.delegate = self;
    calories.text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [scroll addSubview:calories];
    yPos += calories.frame.size.height + 5;
    [self addALine];
    
    carbohydrates = [[EFGTextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 50) labelText:@"Carbohydrates" placeHolder:@"0"];
    carbohydrates.text.delegate = self;
    carbohydrates.text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [scroll addSubview:carbohydrates];
    yPos += carbohydrates.frame.size.height + 5;
    [self addALine];
    
    protein = [[EFGTextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 50) labelText:@"Protein" placeHolder:@"0"];
    protein.text.delegate = self;
    protein.text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [scroll addSubview:protein];
    yPos += protein.frame.size.height + 5;
    [self addALine];
    
    fat = [[EFGTextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 50) labelText:@"Fat" placeHolder:@"0"];
    fat.text.delegate = self;
    fat.text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [scroll addSubview:fat];
    yPos += fat.frame.size.height + 5;
    [self addALine];
    
    yPos += 10;
    xPos = 15;
    
    saveButton = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 40)];
    [saveButton setTitle:@"SAVE DATA" forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    saveButton.titleLabel.font = [UIFont systemFontOfSize:16];
    saveButton.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    saveButton.layer.cornerRadius = 6.0;
    [saveButton addTarget:self action:@selector(saveDataToServer) forControlEvents:UIControlEventTouchUpInside];
    [scroll addSubview:saveButton];
    yPos += saveButton.frame.size.height + 15;
    
    [self setScrollSize];
}

- (void)setScrollSize{
    scroll.contentSize = CGSizeMake(scroll.frame.size.width, yPos);
}


- (void)addALine{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [scroll addSubview:line];
    yPos += line.frame.size.height + 5;
}

#pragma mark - API Methods
- (void)getCalorieMacroData{
    NSDictionary *params = @{
                             @"username" : [EFGUtilities getUserUsername],
                             @"password" : [EFGUtilities getUserPassword],
                             @"date_time" : [EFGUtilities getCurrentFormattedDate],
                             };
    [EFGUtilities showProgress];
    [[EFGAPIConnector sharedManager] getGoalCalorieMacroData:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        if ([dict[@"response"][@"success"] intValue] == 1) {
            //Update the UI
            [self updateUI:dict[@"data"]];
        }else{
            //Put the Placeholder
            [self emptyAllFields];
        }
        [EFGUtilities hideProgress];
    } failure:^(NSHTTPURLResponse *resp, NSError *error) {
        [EFGUtilities hideProgress];
        [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went wrong. Please try again later." actionTitle:@"OK" inViewController:self type:0];
    }];
}

- (void)emptyAllFields{
    calories.text.text = @"";
    carbohydrates.text.text = @"";
    protein.text.text = @"";
    fat.text.text = @"";
}

- (void)updateUI:(NSDictionary *)data{
    if (data[@"calories"] != [NSNull null] && data[@"calories"] != nil) {
        calories.text.text = data[@"calories"];
    }else{
    }
    if (data[@"carbohydrates"] != [NSNull null] && data[@"carbohydrates"] != nil) {
        carbohydrates.text.text = data[@"carbohydrates"];
    }
    if (data[@"protein"] != [NSNull null] && data[@"protein"] != nil) {
        protein.text.text = data[@"protein"];
    }
    if (data[@"fat"] != [NSNull null] && data[@"fat"] != nil) {
        fat.text.text = data[@"fat"];
    }
}

- (void)saveDataToServer{
    if ([self validateFields]) {
        NSDictionary *params = @{
                                 @"username" : [EFGUtilities getUserUsername],
                                 @"password" : [EFGUtilities getUserPassword],
                                 @"date_time" : [EFGUtilities getCurrentFormattedDate],
                                 @"calories" : calories.text.text,
                                 @"carbohydrates" : carbohydrates.text.text,
                                 @"protein" : protein.text.text,
                                 @"fat" : fat.text.text,
                                 };
        [EFGUtilities showProgress];
        [[EFGAPIConnector sharedManager] saveGoalCalorieMacroData:params success:^(NSDictionary *dict, NSURLResponse *resp) {
            if ([dict[@"response"][@"success"] intValue] == 1) {
                [EFGUtilities showAlertWithTitle:@"Saved !" message:@"Additional Goal data is saved." actionTitle:@"OK" inViewController:self type:1];
            }else{
                [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went wrong. Please try again later." actionTitle:@"OK" inViewController:self type:0];
            }
            [EFGUtilities hideProgress];
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [EFGUtilities hideProgress];
            [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went wrong. Please try again later." actionTitle:@"OK" inViewController:self type:0];
        }];
    }
}

- (BOOL)validateFields{
    if (calories.text.text != nil && ![calories.text.text  isEqual: @""] && carbohydrates.text.text != nil && ![carbohydrates.text.text  isEqual: @""] && protein.text.text != nil && ![protein.text.text  isEqual: @""] && fat.text.text != nil && ![fat.text.text  isEqual: @""]) {
        return YES;
    }else{
        [EFGUtilities showAlertWithTitle:@"Error" message:@"Please fill all the fields." actionTitle:@"OK" inViewController:self type:2];
        return NO;
    }
}
#pragma mark - Text field delagates

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL) textField: (UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle];
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    NSNumber * number = [nf numberFromString:newString];
    if (number)
        return YES;
    else
        return NO;
}




#pragma mark - Life Cycle Methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
