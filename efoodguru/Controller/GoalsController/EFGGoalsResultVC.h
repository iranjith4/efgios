//
//  EFGGoalsResultVC.h
//  efoodguru
//
//  Created by Ranjithkumar on 02/01/16.
//  Copyright © 2016 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGGoalsResultVC : UIViewController

- (void) initViewControllerWithData:(NSDictionary *)dict;
- (void) initViewController;

@end
