    //
//  EFGGoalsResultVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 02/01/16.
//  Copyright © 2016 EFG. All rights reserved.
//

#import "EFGGoalsResultVC.h"
#import "EFGUtilities.h"
#import "EFGTextField.h"
#import "EFGTitleLabel.h"
#import "EFGGoalAdditionalNutrients.h"
#import "EFGGoalCalorieMacroVC.h"
#import "EFGAPIConnector.h"
#import "ActionSheetStringPicker.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"

@interface EFGGoalsResultVC () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@end

@implementation EFGGoalsResultVC{
    float xPos;
    float yPos;
    UIScrollView *scroll;
    
    EFGTextField *age;  //1
    EFGTextField *currentWeight;   //2
    EFGTextField *height;   //3
    EFGTextField *weightGoal;   //4
    EFGTextField *dailyActivity; //5
    NSArray *menuNames;
    
    UIButton *saveButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    xPos = 0;
    yPos = NAVIGATIONBAR_HEIGHT + STATUSBAR_HEIGHT;
    self.title = @"Goals";
    [self setupMenuController];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initViewControllerWithData:(NSDictionary *)dict {
    [self initViewController];
}

- (void) initViewController{
    //Initializing Values
    menuNames = [NSArray arrayWithObjects:@"Calorie & Macronutrient Goals",@"Additional Nutrient Goals", nil];
    
    //Adding a base scroll view
    scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, self.view.frame.size.height)];
    scroll.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:scroll];

    [self addTextFields];
    [self addButtons];
    [self getUserGoals];
    
    //Adding the TextFields
}

- (void)addTextFields{
//    NSArray *array = [NSArray arrayWithObjects:@"Age",@"Current Weight",@"Height",@"Goal Weight",@"Daily Activity",nil];
//    xPos = 10;
//    for (NSString *textFieldName in array){
//        
//    }
    float spacing = 4;
    float fieldHeight = 50;
    xPos = 10;
    
    
    //Age
    age = [[EFGTextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, fieldHeight) labelText:@"Age" placeHolder:@"Age"];
    age.text.text = @"Loading...";
    age.text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [scroll addSubview:age];
    yPos += age.frame.size.height + spacing;
    [self addALine];
    
    
    //Current Weight
    currentWeight = [[EFGTextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, fieldHeight) labelText:@"Weight" placeHolder:@"Weight"];
    currentWeight.text.text = @"Loading...";
    currentWeight.text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [scroll addSubview:currentWeight];
    yPos += currentWeight.frame.size.height + spacing;
    [self addALine];
    
    //height
    height = [[EFGTextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, fieldHeight) labelText:@"Height" placeHolder:@"Height"];
    height.text.text = @"Loading...";
    height.text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [scroll addSubview:height];
    yPos += height.frame.size.height + spacing;
    [self addALine];
    
    //Weight Goal
    weightGoal = [[EFGTextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 50) labelText:@"Weight Goal" placeHolder:@"Weight Goal"];
    weightGoal.text.text = @"Loading...";
    weightGoal.text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [scroll addSubview:weightGoal];
    yPos += weightGoal.frame.size.height + spacing;
    [self addALine];
    
    //Daily Activity
    
    dailyActivity = [[EFGTextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 50) labelText:@"Daily Activity" placeHolder:@"Daily Activity"];
    dailyActivity.text.text = @"Loading...";
    dailyActivity.text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    dailyActivity.text.delegate = self;
    [scroll addSubview:dailyActivity];
    yPos += dailyActivity.frame.size.height + spacing;
    [self addALine];
    
}

- (void)addButtons{
    yPos += 15;
    
    EFGTitleLabel *nutritionGoals = [[EFGTitleLabel alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width,40) andTitle:@"Nutrition Goals"];
    [scroll addSubview:nutritionGoals];
    yPos += nutritionGoals.frame.size.height + 4;
    
    
    UITableView *menuTable = [[UITableView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 100) style:UITableViewStylePlain];
    [menuTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"menu_cell"];
    menuTable.backgroundColor = [UIColor whiteColor];
    menuTable.delegate = self;
    menuTable.dataSource = self;
    [scroll addSubview:menuTable];
    [menuTable sizeToFit];
    
    yPos += menuTable.frame.size.height  + 5;
    xPos = 15;
    
    saveButton = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 40)];
    [saveButton setTitle:@"SAVE DATA" forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    saveButton.titleLabel.font = [UIFont systemFontOfSize:16];
    saveButton.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    saveButton.layer.cornerRadius = 6.0;
    [saveButton addTarget:self action:@selector(saveUserGoals) forControlEvents:UIControlEventTouchUpInside];
    [scroll addSubview:saveButton];
    yPos += saveButton.frame.size.height + 15;
    
}

- (void)getUserGoals{
    NSDictionary *params = @{
                             @"username" : [EFGUtilities getUserUsername],
                             @"password" : [EFGUtilities getUserPassword],
                             @"date_time" : [EFGUtilities getCurrentFormattedDate],
                             };
    [EFGUtilities showProgress];
    [[EFGAPIConnector sharedManager] getGoalData:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        if ([dict[@"response"][@"success"] intValue] == 1) {
            [self updateUIData:dict[@"data"]];
        }else{
            [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went wrong with the server.Please try again later." actionTitle:@"OK" inViewController:self type:2];
        }
        [EFGUtilities hideProgress];
    } failure:^(NSHTTPURLResponse *resp, NSError *error) {
        [EFGUtilities hideProgress];
        [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went wrong with the server.Please try again later." actionTitle:@"OK" inViewController:self type:0];
    }];
}

- (void)updateUIData:(NSDictionary *)dict{
    age.text.text = dict[@"age"];
    currentWeight.text.text = dict[@"current_weight"];
    dailyActivity.text.text = dict[@"daily_activity"];
    weightGoal.text.text = dict[@"goal_weight"];
    height.text.text = dict[@"height"];
}

- (void)saveUserGoals{
    
    NSDictionary *params = @{
                             @"username" : [EFGUtilities getUserUsername],
                             @"password" : [EFGUtilities getUserPassword],
                             @"date_time" : [EFGUtilities getCurrentFormattedDate],
                             @"age" : age.text.text,
                             @"current_weight" : currentWeight.text.text,
                             @"height" : height.text.text,
                             @"goal_weight" : weightGoal.text.text,
                             @"daily_activity" : dailyActivity.text.text
                             };
    [EFGUtilities showProgress];
    [[EFGAPIConnector sharedManager] saveGoalData:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        [EFGUtilities hideProgress];
        if ([dict[@"response"][@"success"] intValue] == 1){
            [EFGUtilities showAlertWithTitle:@"Saved" message:@"Your Goals are saved." actionTitle:@"OK" inViewController:self type:1];
        }else{
            [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went wrong. Please try again later." actionTitle:@"OK" inViewController:self type:0];
        }
    } failure:^(NSHTTPURLResponse *resp, NSError *error) {
        [EFGUtilities hideProgress];
    }];
}

#pragma mark - Table View Methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menu_cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menu_cell"];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = [menuNames objectAtIndex:indexPath.row];
    return cell;
 }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return menuNames.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        //Move to Calorie & Macro Nutrient Goals
        EFGGoalCalorieMacroVC *caloriePage = [[EFGGoalCalorieMacroVC alloc] init];
        [self.navigationController pushViewController:caloriePage animated:YES];
    }else if (indexPath.row == 1){
        //Move to Additional Nutrient Goals
        EFGGoalAdditionalNutrients *additionalNutrients = [[EFGGoalAdditionalNutrients alloc] init];
        [self.navigationController pushViewController:additionalNutrients animated:YES];
    }
}

- (void)setScrollSize{
    scroll.contentSize = CGSizeMake(scroll.frame.size.width, yPos);
}

- (void)addALine{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [scroll addSubview:line];
    yPos += line.frame.size.height + 5;
}

#pragma mark - Picker for Activity

- (void)showActivityPicker{
    NSArray *optionsArray = [NSArray arrayWithObjects:@"Sedentary or No Activity",@"Light",@"Moderate",@"High",@"Extreme",nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Daily Activity"
                                            rows:optionsArray
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           dailyActivity.text.text = selectedValue;
                                           [dailyActivity resignFirstResponder];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         [dailyActivity resignFirstResponder];
                                     }
                                          origin:self.view];
}

#pragma mark - Reveal Menu Bar

- (void)setupMenuController{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}


#pragma mark -  Text Field Delegates
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == dailyActivity.text) {
        [self showActivityPicker];
        [textField resignFirstResponder];
    }
}

- (BOOL) textField: (UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle];
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    NSNumber * number = [nf numberFromString:newString];
    if (number)
        return YES;
    else
        return NO;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
