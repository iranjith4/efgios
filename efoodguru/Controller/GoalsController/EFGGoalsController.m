//
//  EFGGoalsController.m
//  efoodguru
//
//  Created by Ranjithkumar on 11/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGGoalsController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"
#import "EFGProgressView.h"
#import "EFGUtilities.h"
#import "EFGQuestionSingleView.h"
#import "EFGGoalActivityLevelView.h"
#import "EFGAPIConnector.h"
#import "EFGGoalsResultVC.h"

@implementation EFGGoalsController{
    EFGProgressView *progressView;
    float xPos;
    float yPos;
    UIButton *next;
    UIButton *back;
    UIScrollView *scrollView;
    EFGQuestionSingleView *ques1;
    EFGQuestionSingleView *ques2;
    EFGQuestionSingleView *ques3;
    EFGQuestionSingleView *ques4;
    EFGGoalActivityLevelView *ques5;
    int currentQuestion;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self initViewController];
    [self setQuestions];
    [self setupMenuController];
}

- (void)initViewController{
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Goals";
    
    yPos = STATUSBAR_HEIGHT + NAVIGATIONBAR_HEIGHT;
    xPos = 0;
    
    back = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width * 0.15, 60)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    xPos = self.view.frame.size.width * 0.15;
    
    //Setting Up ProgressViews
    progressView = [[EFGProgressView alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 60)];
    progressView.numberOfPages = 5;
    [progressView initProgressBar];
    [progressView makeProgress:1];
    [self.view addSubview:progressView];
    
    xPos += progressView.frame.size.width;
    
    next = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width * 0.15, 60)];
    [next setTitle:@"Next" forState:UIControlStateNormal];
    [next setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [next addTarget:self action:@selector(nextButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:next];
    
    yPos += progressView.frame.size.height;
}

- (void)setQuestions {
    currentQuestion = 1;
    //Load Initially with First Question
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, self.view.frame.size.height - yPos)];
    scrollView.backgroundColor = [EFGUtilities themeBackgroundGray];
    [self.view addSubview:scrollView];
    [self loadQuestionNumber:currentQuestion];
    
}

- (void)nextButtonPressed{
    currentQuestion++;
    if (currentQuestion > 0 && currentQuestion <= 6) {
        [self loadQuestionNumber:currentQuestion];
    }else{
        currentQuestion--;
    }
}

- (void)backButtonPressed{
    currentQuestion--;
    if (currentQuestion > 0  && currentQuestion <= 6) {
        [self loadQuestionNumber:currentQuestion];
    }else{
        currentQuestion++;
    }
}

- (void)loadQuestionNumber:(int)qNo{
    [progressView makeProgress:qNo];
    switch (qNo) {
        case 1:{
            if (!ques1) {
                ques1 = [[EFGQuestionSingleView alloc] initWithFrame:CGRectMake(0, 15, self.view.frame.size.width, 400)];
                [ques1 setUpViewWithQuestion:@"What is your age ?" labelPlaceHolder:@"Age"];
                ques1.backgroundColor = [EFGUtilities themeBackgroundGray];
                [scrollView addSubview:ques1];
            }
            [scrollView bringSubviewToFront:ques1];
            ques1.hidden = NO;
            ques2.hidden = YES;
            ques3.hidden = YES;
            ques4.hidden = YES;
            ques5.hidden = YES;
        }
            break;
        case 2:{
            if (!ques2) {
                ques2 = [[EFGQuestionSingleView alloc] initWithFrame:CGRectMake(0, 15, self.view.frame.size.width, 400)];
                [ques2 setUpViewWithQuestion:@"What is your Weight ?" labelPlaceHolder:@"Weight"];
                ques2.backgroundColor = [EFGUtilities themeBackgroundGray];
                [scrollView addSubview:ques2];
            }
            [scrollView bringSubviewToFront:ques2];
            ques1.hidden = YES;
            ques2.hidden = NO;
            ques3.hidden = YES;
            ques4.hidden = YES;
            ques5.hidden = YES;
        }
            
            break;
        case 3:{
            if (!ques3) {
                ques3 = [[EFGQuestionSingleView alloc] initWithFrame:CGRectMake(0, 15, self.view.frame.size.width, 400)];
                [ques3 setUpViewWithQuestion:@"What is your Height ?" labelPlaceHolder:@"Height"];
                ques3.backgroundColor = [EFGUtilities themeBackgroundGray];
                [scrollView addSubview:ques3];
            }
            [scrollView bringSubviewToFront:ques3];
            ques1.hidden = YES;
            ques2.hidden = YES;
            ques3.hidden = NO;
            ques4.hidden = YES;
            ques5.hidden = YES;
        }
            
            break;
        case 4:{
            if (!ques4) {
                ques4 = [[EFGQuestionSingleView alloc] initWithFrame:CGRectMake(0, 15, self.view.frame.size.width, 400)];
                [ques4 setUpViewWithQuestion:@"What is your Weight Goal ?" labelPlaceHolder:@"Weight Goal"];
                ques4.backgroundColor = [EFGUtilities themeBackgroundGray];
                [scrollView addSubview:ques4];
            }
            [scrollView bringSubviewToFront:ques4];
            ques1.hidden = YES;
            ques2.hidden = YES;
            ques3.hidden = YES;
            ques4.hidden = NO;
            ques5.hidden = YES;
        }
            
            break;
        case 5:{
            if (!ques5) {
                ques5 = [[EFGGoalActivityLevelView alloc] initWithFrame:CGRectMake(0, 15, self.view.frame.size.width, self.view.frame.size.height - yPos)];
                ques5.backgroundColor = [EFGUtilities themeBackgroundGray];
                [scrollView addSubview:ques5];
            }
            [scrollView bringSubviewToFront:ques5];
            ques1.hidden = YES;
            ques2.hidden = YES;
            ques3.hidden = YES;
            ques4.hidden = YES;
            ques5.hidden = NO;
        }
            
            break;
        case 6:{
            //Save Goals data right here.
            [self saveGoalsData];
        }
            break;
            
        default:
            break;
    }
}

- (void)saveGoalsData{
    
//    age,current_weight,height,goal_weight,
//    daily_activity
    if ([self validateAllFields]) {
        NSDictionary *params = @{
                                 @"username" :  [EFGUtilities getUserUsername],
                                 @"password" : [EFGUtilities getUserPassword],
                                 @"date_time" : [EFGUtilities getCurrentFormattedDate],
                                 @"age" : ques1.answerField.text,
                                 @"current_weight" : ques2.answerField.text,
                                 @"height" : ques3.answerField.text,
                                 @"goal_weight" : ques4.answerField.text,
                                 @"daily_activity" : ques5.answerData
                                 };
        [EFGUtilities showProgress];
        
        [[EFGAPIConnector sharedManager] saveGoalData:params success:^(NSDictionary *dict, NSURLResponse *resp) {
            [EFGUtilities hideProgress];
            if ([dict[@"response"][@"success"] intValue] == 1){
                [EFGUtilities setIsGoalsEntered:YES];
                EFGGoalsResultVC *goalsResult = [[EFGGoalsResultVC alloc] init];
                [goalsResult initViewController];
                [self.navigationController pushViewController:goalsResult animated:YES];
                currentQuestion = 5;
            }else{
                [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went wrong. Please try again later." actionTitle:@"OK" inViewController:self type:0];
            }
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [EFGUtilities hideProgress];
        }];
    }
}

- (BOOL)validateAllFields{
    if (ques1.answerField.text != nil && ![ques1.answerField.text  isEqual: @""] && ques2.answerField.text != nil && ![ques2.answerField.text  isEqual: @""] && ques3.answerField.text != nil && ![ques3.answerField.text  isEqual: @""] && ques4.answerField.text != nil && ![ques4.answerField.text  isEqual: @""] && ques5.answerData != nil && ![ques5.answerData  isEqual: @""]) {
        return YES;
    }else{
        [EFGUtilities showAlertWithTitle:@"Error" message:@"Please fill all fields" actionTitle:@"OK" inViewController:self];
        return NO;
    }
}

#pragma mark - Reveal Menu Bar

- (void)setupMenuController{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

@end
