//
//  EFGGoalAdditionalNutrients.m
//  efoodguru
//
//  Created by Ranjithkumar on 02/01/16.
//  Copyright © 2016 EFG. All rights reserved.
//

#import "EFGGoalAdditionalNutrients.h"
#import "EFGUtilities.h"
#import "EFGTextField.h"
#import "EFGAPIConnector.h"

@interface EFGGoalAdditionalNutrients ()<UITextFieldDelegate>

@end

@implementation EFGGoalAdditionalNutrients{
    float xPos;
    float yPos;
    UIScrollView *scroll;
    NSArray *fieldNames;
    NSArray *fieldAPINames;
    NSArray *fieldPlaceholders;
    NSMutableArray *fields;
    UIButton *saveButton;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self addNotifications];
    [self initController];
    [self addTextFields];
    [self getAdditionalGoalData];
    // Do any additional setup after loading the view.
}

- (void)addNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void) initController{
    //Initiallations
    
    fields = [[NSMutableArray alloc] init];
    
    yPos = 0;//STATUSBAR_HEIGHT + NAVIGATIONBAR_HEIGHT;
    xPos = 0;
    
    scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width - yPos, self.view.frame.size.height - yPos)];
    scroll.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:scroll];
}

- (void)addTextFields{
    yPos = 15;
    fieldNames = [[NSArray alloc] initWithObjects:@"Saturated Fat (g)",@"Polyunsaturated Fat (g)",@"MonoUnsaturated Fat (g)",@"Trans Fat (g)",@"Cholesterol (g)",@"Sodium (g)",@"Potassium (g)", @"Fiber (g)",@"Sugar (g)",@"Vitamin A (%)",@"Vitamin C (%)",@"Calcium (%)",@"Iron (%)",nil];
    
    fieldPlaceholders = [[NSArray alloc] initWithObjects:@"0g",@"0g",@"0g",@"0g",@"0g",@"0g",@"0g",@"0g",@"0g",@"0%",@"0%",@"0%",@"0%",nil];
    
    fieldAPINames = [[NSArray alloc] initWithObjects:@"saturated_fat",@"polyunsaturated_fat",@"monounsaturated_fat",@"trans_fat", @"cholesterol", @"sodium",@"potassium",@"fiber",@"sugar",@"Vitamin_A",@"Vitamin_C",@"calcium",@"iron", nil];
    
    int x = 0;
    for (NSString *fieldName in fieldNames){
        EFGTextField *textField = [[EFGTextField alloc] initWithFrame:CGRectMake(xPos, yPos, scroll.frame.size.width, 50) labelText:fieldName placeHolder:[fieldPlaceholders objectAtIndex:x]];
        [scroll addSubview:textField];
        [fields addObject:textField];
        textField.text.delegate = self;
        textField.text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        textField.tag = x;
        yPos += textField.frame.size.height + 4;
        [self addALine];
        x++;
    }
    
    xPos = 15;
    
    saveButton = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 40)];
    [saveButton setTitle:@"SAVE DATA" forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    saveButton.titleLabel.font = [UIFont systemFontOfSize:16];
    saveButton.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    saveButton.layer.cornerRadius = 6.0;
    [saveButton addTarget:self action:@selector(saveAdditionalGoalData) forControlEvents:UIControlEventTouchUpInside];
    [scroll addSubview:saveButton];
    yPos += saveButton.frame.size.height + 15;

    //setting scroll size
    [self setScrollSize];
}

#pragma mark - API Methods
- (void)getAdditionalGoalData{
    NSDictionary *params = @{
                             @"username" : [EFGUtilities getUserUsername],
                             @"password" : [EFGUtilities getUserPassword],
                             @"date_time" : [EFGUtilities getCurrentFormattedDate],
                             };
    [EFGUtilities showProgress];
    [[EFGAPIConnector sharedManager] getGoalAdditionalData:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        if ([dict[@"response"][@"success"] intValue] == 1) {
            //Update the UI with the data
            [self updateData:dict[@"data"]];
        }else{
            
        }
        [EFGUtilities hideProgress];
    } failure:^(NSHTTPURLResponse *resp, NSError *error) {
        //leave
        [EFGUtilities hideProgress];
        
    }];
}

- (void)saveAdditionalGoalData{
    if ([self validateFields]) {
        NSArray *array = [fields mutableCopy];
        NSDictionary *paramsFixed = @{
                                      @"username" : [EFGUtilities getUserUsername],
                                      @"password" : [EFGUtilities getUserPassword],
                                      @"date_time" : [EFGUtilities getCurrentFormattedDate],
                                      };
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:paramsFixed];
        int index = 0;
        for (EFGTextField *txt in array) {
            [params setObject:txt.text.text forKey:[fieldAPINames objectAtIndex:index]];
            index++;
        }
        NSLog(@"Total Dict : %@",params);
        [EFGUtilities showProgress];
        [[EFGAPIConnector sharedManager] saveGoalAdditionalData:[params mutableCopy] success:^(NSDictionary *dict, NSURLResponse *resp) {
            [EFGUtilities hideProgress];
            if ([dict[@"response"][@"success"] intValue] == 1) {
                [EFGUtilities showAlertWithTitle:@"Saved !" message:dict[@"data"] actionTitle:@"OK" inViewController:self type:1];
            }else{
                [EFGUtilities showAlertWithTitle:@"Failed !" message:@"Something went wrong. Please try again later." actionTitle:@"OK" inViewController:self type:2];
            }
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [EFGUtilities hideProgress];
            [EFGUtilities showAlertWithTitle:@"Failed !" message:@"Something went wrong. Please try again later." actionTitle:@"OK" inViewController:self type:0];
        }];
    }
}

- (BOOL)validateFields{
    NSArray *array = [fields mutableCopy];
    for (EFGTextField *txt in array) {
        if (txt.text.text == nil || [txt.text.text  isEqual: @""]) {
            [EFGUtilities showAlertWithTitle:@"Oh oh !" message:@"Please fill all the fields !" actionTitle:@"OK" inViewController:self type:0];
            return NO;
        }
    }
    return YES;
}

- (void)updateData:(NSDictionary *)data{
    int index = 0;
    for (NSString *fieldAPIName in fieldAPINames) {
        if (data[fieldAPIName] != [NSNull null] && data[fieldAPIName] != nil) {
            EFGTextField *t = [fields objectAtIndex:index];
            t.text.text = data[fieldAPIName];
        }
        index++;
    }
}

- (void)addALine{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [scroll addSubview:line];
    yPos += line.frame.size.height + 5;
}

- (void)setScrollSize{
    scroll.contentSize = CGSizeMake(scroll.frame.size.width, yPos);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Text Field Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -  Keyboard Delegate methods


- (void)keyboardDidShow: (NSNotification *) notif{
     NSArray *array = [fields mutableCopy];
    for (EFGTextField *t in array) {
        if (t.tag > 5) {
            if (t.text.isFirstResponder) {
                UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
                CGSize keyboardSize = [[[notif userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
                if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight ) {
                    CGSize origKeySize = keyboardSize;
                    keyboardSize.height = origKeySize.width;
                    keyboardSize.width = origKeySize.height;
                }
                UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
                scroll.contentInset = contentInsets;
                scroll.scrollIndicatorInsets = contentInsets;
                
                // If active text field is hidden by keyboard, scroll it so it's visible
                // Your application might not need or want this behavior.
                CGRect rect = scroll.frame;
                rect.size.height -= keyboardSize.height;
                NSLog(@"Rect Size Height: %f", rect.size.height);
                
                if (!CGRectContainsPoint(rect, t.frame.origin)) {
                    CGPoint point = CGPointMake(0, t.frame.origin.y - keyboardSize.height);
                    NSLog(@"Point Height: %f", point.y);
                    [scroll setContentOffset:point animated:YES];
                }            }
        }
    }
    
}

- (void)keyboardDidHide: (NSNotification *) notif{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(NAVIGATIONBAR_HEIGHT + STATUSBAR_HEIGHT, 0, 0, 0);
    scroll.contentInset = contentInsets;
    scroll.scrollIndicatorInsets = contentInsets;
}


- (BOOL) textField: (UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle];
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    NSNumber * number = [nf numberFromString:newString];
    if (number)
        return YES;
    else
        return NO;
}



@end
