//
//  EFGProfileVC.h
//  efoodguru
//
//  Created by Ranjithkumar on 02/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGCameraViewController.h"

@interface EFGProfileVC : UIViewController <UITextFieldDelegate, TGCameraDelegate>

@end
