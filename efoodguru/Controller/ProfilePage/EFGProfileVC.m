//
//  EFGProfileVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 02/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGProfileVC.h"
#import "EFGUtilities.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"
#import "EFGAPIConnector.h"

#define ELEMENT_HEIGHT 40
#define LABEL_FONT_SIZE 17
#define FIELD_X self.view.frame.size.width * 0.45

@interface EFGProfileVC (){
    UIScrollView *scrollView;
    UIImageView *coverImage;
    UIImageView *profileImage;
    UILabel *emailLabel;
    UITextField *emailField;
    UILabel *passwordLabel;
    UITextField *passwordField;
    UILabel *confirmPasswordLabel;
    UITextField *confirmPasswordField;
    UILabel *userNameLabel;
    UITextField *userNameField;
    UILabel *zipCode;
    UITextField *zipCodeField;
    UIButton *signUpButton;
    float xPos;
    float yPos;

}

@end

@implementation EFGProfileVC

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor whiteColor];
    [super viewDidLoad];
    self.title = @"Edit Profile";
    [self addNotifications];
    [self initViewController];
    [self setupMenuController];
    // Do any additional setup after loading the view.
}

- (void)addNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}

- (void)initViewController{
    
    xPos = 0;
    scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.scrollEnabled = YES;
    [self.view addSubview:scrollView];
    
    //Setting Cover Image
    coverImage = [[UIImageView alloc] init];
    coverImage.frame = CGRectMake(xPos, yPos, self.view.frame.size.width, 80 * DEVICE_HEIGHT_RATIO);
    coverImage.backgroundColor = [UIColor grayColor];
    coverImage.image = [UIImage imageNamed:@"avatar.jpg"];
    [scrollView addSubview:coverImage];
    yPos += coverImage.frame.size.height;
    xPos = 10 * DEVICE_WIDTH_RATIO;
    
    //Setting Profile Image
    profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, 0, self.view.frame.size.width * 0.35, self.view.frame.size.width * 0.35)];
    profileImage.image = [UIImage imageNamed:@"avatar.jpg"];
    profileImage.center = CGPointMake(profileImage.center.x, yPos);
    profileImage.backgroundColor = [UIColor lightGrayColor];
    [scrollView addSubview:profileImage];
    
    UIButton *changeImage = [[UIButton alloc] initWithFrame:CGRectMake(0, profileImage.frame.size.height - 40, profileImage.frame.size.width, 40)];
    [changeImage setTitle:@"Change" forState:UIControlStateNormal];
    [changeImage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [changeImage addTarget:self action:@selector(tappedPhoto) forControlEvents:UIControlEventTouchUpInside];
    [profileImage addSubview:changeImage];
    
    yPos = profileImage.frame.origin.y + profileImage.frame.size.height + 10 *DEVICE_HEIGHT_RATIO;
    
    // Email
    emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
    emailLabel.text = @"Email";
    emailLabel.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
    emailLabel.textColor = [EFGUtilities themeLabelBlackShade];
    [scrollView addSubview:emailLabel];
    
    emailField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
    emailField.center = CGPointMake(emailField.center.x, emailLabel.center.y);
    emailField.borderStyle = UITextBorderStyleRoundedRect;
    emailField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailField.autocorrectionType = UITextAutocorrectionTypeNo;
    emailField.spellCheckingType = UITextSpellCheckingTypeNo;
    emailField.keyboardType = UIKeyboardTypeEmailAddress;
    emailField.placeholder = [EFGUtilities getUserEmail];
    emailField.delegate = self;
    [scrollView addSubview:emailField];
    yPos += emailField.frame.size.height;
    [self addALine];
    
    // Password
    passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
    passwordLabel.text = @"Password";
    passwordLabel.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
    passwordLabel.textColor = [EFGUtilities themeLabelBlackShade];
    [scrollView addSubview:passwordLabel];
    
    passwordField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
    passwordField.center = CGPointMake(passwordField.center.x, passwordLabel.center.y);
    passwordField.borderStyle = UITextBorderStyleRoundedRect;
    passwordField.secureTextEntry = YES;
    passwordField.delegate = self;
    [scrollView addSubview:passwordField];
    yPos += passwordField.frame.size.height;
    [self addALine];
    
    // confirm Password
    confirmPasswordLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
    confirmPasswordLabel.text = @"Conf. Password";
    confirmPasswordLabel.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
    confirmPasswordLabel.textColor = [EFGUtilities themeLabelBlackShade];
    [scrollView addSubview:confirmPasswordLabel];
    
    confirmPasswordField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
    confirmPasswordField.center = CGPointMake(confirmPasswordField.center.x, confirmPasswordLabel.center.y);
    confirmPasswordField.borderStyle = UITextBorderStyleRoundedRect;
    confirmPasswordField.secureTextEntry = YES;
    confirmPasswordField.delegate = self;
    [scrollView addSubview:confirmPasswordField];
    yPos += confirmPasswordField.frame.size.height;
    [self addALine];
    
    // Country is removed. So commenting out this line.
    //    countryLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
    //    countryLabel.text = @"Country";
    //    countryLabel.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
    //    countryLabel.textColor = [EFGUtilities themeLabelBlackShade];
    //    [scrollView addSubview:countryLabel];
    //
    //    countryField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
    //    countryField.center = CGPointMake(countryField.center.x, countryLabel.center.y);
    //    countryField.borderStyle = UITextBorderStyleRoundedRect;
    //    countryField.delegate = self;
    //    [scrollView addSubview:countryField];
    //    yPos += countryField.frame.size.height;
    //    [self addALine];
    
    // Zip Code
    zipCode = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
    zipCode.text = @"Zipcode";
    zipCode.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
    zipCode.textColor = [EFGUtilities themeLabelBlackShade];
    [scrollView addSubview:zipCode];
    
    zipCodeField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
    zipCodeField.center = CGPointMake(zipCodeField.center.x, zipCode.center.y);
    zipCodeField.borderStyle = UITextBorderStyleRoundedRect;
    zipCodeField.placeholder = [EFGUtilities getUserZipCode];
    zipCodeField.delegate = self;
    zipCodeField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [scrollView addSubview:zipCodeField];
    yPos += zipCodeField.frame.size.height;
    [self addALine];
    
    yPos += 10;
    
    //Sign Up Button
    signUpButton = [[UIButton alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width * 0.80, ELEMENT_HEIGHT)];
    signUpButton.center = CGPointMake(self.view.center.x, signUpButton.center.y);
    [signUpButton setTitle:@"Save" forState:UIControlStateNormal];
    signUpButton.titleLabel.font = [EFGUtilities fontWithBold:18];
    [signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    signUpButton.backgroundColor = [EFGUtilities themeButtonRedColor];
    [signUpButton addTarget:self action:@selector(saveTapped) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:signUpButton];
    
    yPos += 15;
    [self setScrollContentHeight];

}

- (void)tappedPhoto {
    TGCameraNavigationController *navigationController =
    [TGCameraNavigationController newWithCameraDelegate:self];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)saveTapped{
    if ([self validate]) {
        NSString *userName = [EFGUtilities getUserUsername];
        NSString *password;
        if (passwordField.text != nil && ![passwordField.text isEqualToString:@""]) {
            password = passwordField.text;
        }else{
            password = [EFGUtilities getUserPassword];
        }
        
        NSString *email;
        if (emailField.text != nil && ![emailField.text isEqualToString:@""]) {
            email = emailField.text;
        }else{
            email = [EFGUtilities getUserEmail];
        }
        
        NSString *zip;
        if (zipCodeField.text != nil && ![zipCodeField.text isEqualToString:@""]) {
            zip = zipCodeField.text;
        }else{
            zip = [EFGUtilities getUserZipCode];
        }
        
        NSDictionary *d = @{
                            @"username" : userName,
                            @"password" : password,
                            @"email" : email,
                            @"zip_code" : zip
                            };
        
        
        [[EFGAPIConnector sharedManager] editProfileWith:d success:^(NSDictionary *dict, NSURLResponse *resp) {
    
            if ([dict[@"response"][@"success"] intValue] == 1) {
                [EFGUtilities setUserPassword:password];
                [EFGUtilities setUserEmail:email];
                [EFGUtilities setUserZipCode:zip];
            }else{
                [EFGUtilities showAlertWithTitle:@"Something Wrong !" message:@"Please Try agaon later." actionTitle:@"OK" inViewController:self];
            }
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [EFGUtilities showAlertWithTitle:@"Something Wrong !" message:@"Please Try agaon later." actionTitle:@"OK" inViewController:self];
        }];
    }
}

- (void)getUserDetails{
    NSDictionary *params = @{
                             @"username" : userNameField.text,
                             @"password" : passwordField.text
                             };
    [[EFGAPIConnector sharedManager] getUserDetails:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        NSLog(@"userdata : %@",dict);
        if ([dict[@"response"][@"success"] boolValue]) {
            [EFGUtilities setLoggedIn:YES];
            [EFGUtilities saveUserDetails:dict];
        }else{
            [EFGUtilities showAlertWithTitle:@"Error" message:@"Something Went Wrong. Please try again Later." actionTitle:@"OK" inViewController:self];
        }
    } failure:^(NSHTTPURLResponse *resp, NSError *err) {
        [EFGUtilities showAlertWithTitle:@"Error" message:@"Something Went Wrong. Please try again Later." actionTitle:@"OK" inViewController:self];
    }];
}

- (BOOL)validate{
    if ([emailField.text isEqualToString:@""]) {
        
    }else{
        if ([EFGUtilities validateEmail:emailField.text]){

        }else{
            [EFGUtilities showAlertWithTitle:@"Email" message:@"Email is not valid. Please check the email." actionTitle:@"Ok" inViewController:self];
            return NO;
        }
    }
    
    if ([passwordField.text isEqualToString:@""] && [confirmPasswordField.text isEqualToString: @""]) {

        
    }else{
        if ([passwordField.text isEqualToString:confirmPasswordField.text]) {
            
        }else{
            [EFGUtilities showAlertWithTitle:@"Password Error !" message:@"Passwords Donot match." actionTitle:@"Ok" inViewController:self];
            return NO;
        }
    }
    return YES;
}

- (void)setScrollContentHeight{
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, yPos + NAVIGATIONBAR_HEIGHT + STATUSBAR_HEIGHT);
}

- (void)addALine{
    yPos += 5;
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 2)];
    line.backgroundColor = [UIColor grayColor];
    [scrollView addSubview:line];
    yPos += line.frame.size.height + 5;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - text field delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)keyboardDidShow: (NSNotification *) notif{
    if (emailField.isFirstResponder) {
        
    }else{
        NSDictionary *keyboardInfo = [notif userInfo];
        NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
        float keyboardHeight = keyboardFrameBeginRect.size.height;
        scrollView.contentOffset = CGPointMake(0, keyboardHeight - self.navigationController.navigationBar.frame.size.height - 20);
    }
}

- (void)keyboardDidHide: (NSNotification *) notif{
    if (emailField.isFirstResponder) {
        
    }else{
        scrollView.contentOffset = CGPointMake(0, - self.navigationController.navigationBar.frame.size.height - 20);
    }
}


#pragma mark - Reveal Menu Bar

- (void)setupMenuController{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma mark - TGCameraDelegate required

- (void)cameraDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidTakePhoto:(UIImage *)image
{
    profileImage.image = image;
    coverImage.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidSelectAlbumPhoto:(UIImage *)image
{
    profileImage.image = image;
    coverImage.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
