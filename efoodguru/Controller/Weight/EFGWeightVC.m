//
//  EFGWaterVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 04/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGWeightVC.h"
#import "EFGUtilities.h"
#import "EFGTextField.h"
#import "EFGAPIConnector.h"

@implementation EFGWeightVC{
    float xPos;
    float yPos;
    EFGNavigationView *navigationView;
    EFGTextField *weight;
    UIButton *setWeight;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self initUI];
}

- (void)initUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    xPos = 0;
    yPos = STATUSBAR_HEIGHT;
    
    navigationView = [[EFGNavigationView alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 60)];
    [navigationView initViewsWithColor:[EFGUtilities themeColor] title:@"Add Weight" andBackText:@"Cancel"];
    navigationView.delegate = self;
    [self.view addSubview:navigationView];
    yPos += navigationView.frame.size.height + 20;
    
    weight = [[EFGTextField alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 60) labelText:@"Weight (lbs)" placeHolder:@"Weight in Pounds"];
    [weight setTextFieldBorderStyle:UITextBorderStyleNone];
    [self.view addSubview:weight];
    yPos += weight.frame.size.height + 15;
    
    xPos = 15;
    
    setWeight = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 40)];
    [setWeight setTitle:@"SET" forState:UIControlStateNormal];
    [setWeight setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    setWeight.titleLabel.font = [UIFont systemFontOfSize:16];
    setWeight.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    setWeight.layer.cornerRadius = 6.0;
    [setWeight addTarget:self action:@selector(setWeighttapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:setWeight];
    yPos += setWeight.frame.size.height + 15;
}

- (void)setWeighttapped{
    if (weight.text.text != nil && ![weight.text.text  isEqual: @""]) {
        NSDictionary *params = @{
                                 @"username" : [EFGUtilities getUserUsername],
                                 @"password" : [EFGUtilities getUserPassword],
                                 @"date_time" : [EFGUtilities getCurrentFormattedDate],
                                 @"keyValues" : @{
                                         @"weight" : weight.text.text
                                         }
                                 };
        [EFGUtilities showProgress];
        [[EFGAPIConnector sharedManager] saveWeight:params success:^(NSDictionary *dict, NSURLResponse *resp) {
            NSLog(@"%@",dict);
            [EFGUtilities hideProgress];
            [self backNavigationTapped];
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [EFGUtilities hideProgress];
            [self backNavigationTapped];
        }];
    }else{
        [EFGUtilities showAlertWithTitleVC:@"Error !" message:@"Please enter weight." actionTitle:@"Ok" inViewController:self];
    }
}

-(void)backNavigationTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
