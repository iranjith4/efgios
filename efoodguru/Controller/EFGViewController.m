//
//  ViewController.m
//  efoodguru
//
//  Created by Ranjithkumar on 05/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGViewController.h"
#import "EFGStartingViewController.h"
#import "EFGUtilities.h"
#import "EFGHomeViewController.h"
#import "EFGMenuVC.h"
#import "MMDrawerController.h"
#import "MMDrawerVisualState.h"

@interface EFGViewController (){
    EFGStartingViewController *startingViewController;
}

@end

@implementation EFGViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addNotificationObservers];
    [self loadScreens];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)loadScreens{
    if ([EFGUtilities isLoggedIn]) {
        //Navigate to the Home Screen
        [self gotoHome];
    }else{
        //Navigate to the StaringViewController
        startingViewController = [[EFGStartingViewController alloc] init];
        [self.navigationController pushViewController:startingViewController animated:NO];
    }
}


- (void)addNotificationObservers{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoHome) name:@"GO_TO_HOME_CONTROLLER" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout) name:@"LOGOUT_CALLED" object:nil];
}

- (void)gotoHome{
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    UIViewController * leftSideDrawerViewController = [[EFGMenuVC alloc] init];
    
    UIViewController * centerViewController = [[EFGHomeViewController alloc] init];
    
   // UIViewController * rightSideDrawerViewController = [[UIViewController alloc] init];
    
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:centerViewController];
    [navigationController setRestorationIdentifier:@"MMExampleCenterNavigationControllerRestorationKey"];
   // UINavigationController * rightSideNavController = [[UINavigationController alloc] initWithRootViewController:rightSideDrawerViewController];
    //[rightSideNavController setRestorationIdentifier:@"MMExampleRightNavigationControllerRestorationKey"];
    UINavigationController * leftSideNavController = [[UINavigationController alloc] initWithRootViewController:leftSideDrawerViewController];
    [leftSideNavController setRestorationIdentifier:@"MMExampleLeftNavigationControllerRestorationKey"];
    self.drawerController = [[MMDrawerController alloc]
                             initWithCenterViewController:navigationController
                             leftDrawerViewController:leftSideNavController
                             rightDrawerViewController:nil];
    [self.drawerController setShowsShadow:NO];
    [self.drawerController setRestorationIdentifier:@"MMDrawer"];
    [self.drawerController setMaximumRightDrawerWidth:200.0];
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    [self.drawerController
     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
         MMDrawerControllerDrawerVisualStateBlock block;
         block = [MMDrawerVisualState slideVisualStateBlock];
         if(block){
             block(drawerController, drawerSide, percentVisible);
         }
     }];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:self.drawerController animated:NO];
}

- (void)logout{
    [self.navigationController popToRootViewControllerAnimated:NO];
    startingViewController = [[EFGStartingViewController alloc] init];
    [self.navigationController pushViewController:startingViewController animated:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

@end
