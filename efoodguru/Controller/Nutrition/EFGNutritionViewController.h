//
//  EFGNutritionViewController.h
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFGTimeIntervalSelector.h"
#import "THDatePickerViewController.h"


typedef enum : NSUInteger {
    EFGNutritionCategoryTypeCalorie = 0,
    EFGNutritionCategoryTypeNutrition = 1,
    EFGNutritionCategoryTypeMacros = 2,
} EFGNutritionCategoryType;

@interface EFGNutritionViewController : UIViewController <EFGTimeIntervalProtocol,THDatePickerDelegate>

@end
