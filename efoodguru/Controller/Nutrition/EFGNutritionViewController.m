//
//  EFGNutritionViewController.m
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGNutritionViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"
#import "EFGUtilities.h"
#import "PNChart.h"
#import "EFGNutritionCalorieView.h"
#import "EFGNutritionMacroView.h"
#import "EFGNutritionNutritionView.h"
#import "ActionSheetStringPicker.h"
#import "EFGAPIConnector.h"


@interface EFGNutritionViewController (){
    float xPos;
    float yPos;
    UISegmentedControl *categorySelector;
    EFGTimeIntervalSelector *timeInterval;
    UIScrollView *scrollView;
    EFGNutritionCategoryType type;
    EFGNutritionCalorieView *calorieView;
    EFGNutritionMacroView *macroView;
    EFGNutritionNutritionView *nutritionView;
    NSString *dateForAPI;
    NSString *startDateForAPI;
    TimeType timeType;
}

@end

@implementation EFGNutritionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initViewController];
    [self setupMenuController];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initViewController{
    dateForAPI = [EFGUtilities getCurrentFormattedDate:[NSDate date] inFormat:@"yyyy-MM-dd"];
    startDateForAPI = [EFGUtilities getCurrentFormattedDate:[NSDate date] inFormat:@"yyyy-MM-dd"];
    timeType = TimeTypeDay;
    
    self.view.backgroundColor = [EFGUtilities themeBackgroundGray];
    self.title = @"Nutrition";
    type  = EFGNutritionCategoryTypeCalorie;
    
    xPos = 15;
    yPos = STATUSBAR_HEIGHT + NAVIGATIONBAR_HEIGHT + 10;
    
    //Adding Segment
    categorySelector = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Calories",@"Nutrients",@"Macros",nil]];
    categorySelector.selectedSegmentIndex = 0;
    categorySelector.frame = CGRectMake(xPos, yPos, self.view.frame.size.width - 2 * xPos, 35);
    categorySelector.tintColor = [EFGUtilities themeColor];
    [categorySelector addTarget:self action:@selector(categoryChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:categorySelector];
    yPos += categorySelector.frame.size.height + 10;
    xPos = 0;
    
    timeInterval = [[EFGTimeIntervalSelector alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 45)];
    timeInterval.backgroundColor = [EFGUtilities themeColor];
    timeInterval.delegate = self;
    [timeInterval.back setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [timeInterval.timeInterval setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [timeInterval.forward setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:timeInterval];
    yPos += timeInterval.frame.size.height + 10;
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(xPos, yPos,self.view.frame.size.width,self.view.frame.size.height - yPos)];
    [self.view addSubview:scrollView];
    
    [self loadPage:EFGNutritionCategoryTypeCalorie];
}

- (void)categoryChanged:(UISegmentedControl *)sender{
    switch (sender.selectedSegmentIndex) {
        case 0:
            type = EFGNutritionCategoryTypeCalorie;
            break;
            
        case 1:
            type = EFGNutritionCategoryTypeNutrition;
            break;
            
        case 2:
            type = EFGNutritionCategoryTypeMacros;
            break;
            
        default:
            break;
    }
    [self loadPage:type];
}

- (void)loadPage:(EFGNutritionCategoryType)nutType{
    switch (nutType) {
        case EFGNutritionCategoryTypeCalorie:
        {
            [self loadCalorieView];
        }
            break;
            
        case EFGNutritionCategoryTypeNutrition:
        {
            [self loadNutrientsView];
        }
            break;
            
        case EFGNutritionCategoryTypeMacros:
        {
            [self loadMacrosView];
        }
            break;
            
        default:
            break;
    }
    [self callAPI];
}


- (void)loadCalorieView{
    nutritionView.hidden = YES;
    macroView.hidden = YES;
    calorieView.hidden = NO;
    if (!calorieView) {
        calorieView = [[EFGNutritionCalorieView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 0)];
        [scrollView addSubview:calorieView];
    }else{
        [scrollView bringSubviewToFront:calorieView];
    }
    
    [self resizeScroll:calorieView.frame.size.height];
}

- (void)loadNutrientsView{
    nutritionView.hidden = NO;
    macroView.hidden = YES;
    calorieView.hidden = YES;
    if (!nutritionView) {
        nutritionView = [[EFGNutritionNutritionView alloc] initWithFrame:CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
        [scrollView addSubview:nutritionView];
    }else{
        [scrollView bringSubviewToFront:nutritionView];
    }
    [self resizeScroll:nutritionView.frame.size.height];
}

- (void)loadMacrosView{
    nutritionView.hidden = YES;
    macroView.hidden = NO;
    calorieView.hidden = YES;
    if (!macroView) {
        macroView = [[EFGNutritionMacroView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 0)];
        [scrollView addSubview:macroView];
    }else{
        [scrollView bringSubviewToFront:macroView];
    }
    
    [self resizeScroll:macroView.frame.size.height];
}

- (void)resizeScroll: (CGFloat)height{
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, height);
}

#pragma mark - Time Interval Selector Methods

- (void)timeTitleTapped{
    NSArray *optionsArray = [NSArray arrayWithObjects:@"Day View",@"Week View",@"Select Date",nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Duration"
                                            rows:optionsArray
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           
                                           switch (selectedIndex) {
                                               case 0:
                                                   //Day View
                                                   timeType = TimeTypeDay;
                                                   [timeInterval setType:TimeTypeDay];
                                                   break;
                                                   
                                                case 1:
                                                   //Week View
                                                   timeType = TimeTypeWeek;
                                                   [timeInterval setType:TimeTypeWeek];
                                                   break;
                                                   
                                               case 2:{
                                                   //Show the date picker
                                                   timeType = TimeTypeDate;
                                                   [timeInterval setType:TimeTypeDate];
                                                   THDatePickerViewController *datePicker = [THDatePickerViewController datePicker];
                                                   datePicker.date = [NSDate date];
                                                   datePicker.delegate = self;
                                                   [datePicker setAllowClearDate:NO];
                                                   [datePicker setClearAsToday:YES];
                                                   [datePicker setAutoCloseOnSelectDate:YES];
                                                   [datePicker setAllowSelectionOfSelectedDate:YES];
                                                   [datePicker setDisableFutureSelection:YES];
                                                   [datePicker setSelectedBackgroundColor:[UIColor colorWithRed:125/255.0 green:208/255.0 blue:0/255.0 alpha:1.0]];
                                                   [datePicker setCurrentDateColor:[UIColor colorWithRed:242/255.0 green:121/255.0 blue:53/255.0 alpha:1.0]];
                                                   
                                                   [datePicker setDateHasItemsCallback:^BOOL(NSDate *date) {
                                                       int tmp = (arc4random() % 30)+1;
                                                       if(tmp % 5 == 0)
                                                           return YES;
                                                       return NO;
                                                   }];
                                                   //[self.datePicker slideUpInView:self.view withModalColor:[UIColor lightGrayColor]];
                                                   [self presentSemiViewController:datePicker withOptions:@{
                                                                                                                 KNSemiModalOptionKeys.pushParentBack    : @(NO),
                                                                                                                 KNSemiModalOptionKeys.animationDuration : @(0.3),
                                                                                                                 KNSemiModalOptionKeys.shadowOpacity     : @(0.3),
                                                                                                                 }];

                                                   break;
                                               }
                                                   
                                               default:
                                                   break;
                                           }
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         
                                     }
                                          origin:self.view];
}

- (void)loadWeekViewFrom:(NSString *)fromDate toDate:(NSString *)toDate{
    NSLog(@"FROM : %@ TO %@",fromDate,toDate);
    dateForAPI = toDate;
    startDateForAPI = fromDate;
    [self callAPI];
}

- (void)loadDayView:(NSString *)date{
    NSLog(@"DATE : %@",date);
    dateForAPI = date;
    [self callAPI];
}

#pragma mark - API Caller
- (void)callAPI{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[EFGUtilities getUserUsername] forKey:@"username"];
    [params setObject:[EFGUtilities getUserPassword] forKey:@"password"];
    [params setObject:[EFGUtilities getCurrentFormattedDate] forKey:@"date_time"];
    
    switch (type) {
        case EFGNutritionCategoryTypeCalorie:
        {
            if (timeType == TimeTypeDate || timeType == TimeTypeDay) {
                [params setObject:dateForAPI forKey:@"query_date"];
                [[EFGAPIConnector sharedManager] getNutritionCalorieDay:[params mutableCopy] success:^(NSDictionary *dict, NSURLResponse *resp) {
                    if ([dict[@"response"][@"success"] intValue] == 1) {
                        [self updateDailyCalorie:dict[@"data"]];
                    }
                } failure:^(NSHTTPURLResponse *resp, NSError *error) {
                    
                }];
                
            }else if(timeType == TimeTypeWeek){
                [params setObject:dateForAPI forKey:@"to_date"];
                [params setObject:startDateForAPI forKey:@"from_date"];
                [params setObject:@"total" forKey:@"type"];
                [[EFGAPIConnector sharedManager] getNutritionCalorieWeek:[params mutableCopy] success:^(NSDictionary *dict, NSURLResponse *resp) {
                    if ([dict[@"response"][@"success"] intValue] == 1) {
                        [self updateWeeklyCalorie:dict[@"data"]];
                    }
                } failure:^(NSHTTPURLResponse *resp, NSError *error) {
                    
                }];
            }
        }
            break;
            
        case EFGNutritionCategoryTypeNutrition:
        {
            if (timeType == TimeTypeDate || timeType == TimeTypeDay) {
                [params setObject:dateForAPI forKey:@"query_date"];
                [[EFGAPIConnector sharedManager] getNutritionNutritionDay:[params mutableCopy] success:^(NSDictionary *dict, NSURLResponse *resp) {
                    if ([dict[@"response"][@"success"] intValue] == 1) {
                        [self updateDailyNutrition:dict[@"data"]];
                    }
                } failure:^(NSHTTPURLResponse *resp, NSError *error) {
                    
                }];
                
            }else if(timeType == TimeTypeWeek){
                [params setObject:dateForAPI forKey:@"to_date"];
                [params setObject:startDateForAPI forKey:@"from_date"];
                [params setObject:@"total" forKey:@"type"];
                [[EFGAPIConnector sharedManager] getNutritionNutritionWeek:[params mutableCopy] success:^(NSDictionary *dict, NSURLResponse *resp) {
                    if ([dict[@"response"][@"success"] intValue] == 1) {
                        [self updateWeeklyNutrition:dict[@"data"]];
                    }
                } failure:^(NSHTTPURLResponse *resp, NSError *error) {
                    
                }];
            }
        }
            break;
            
        case EFGNutritionCategoryTypeMacros:
        {
            if (timeType == TimeTypeDate || timeType == TimeTypeDay) {
                [params setObject:dateForAPI forKey:@"query_date"];
                [[EFGAPIConnector sharedManager] getNutritionMacroDay:[params mutableCopy] success:^(NSDictionary *dict, NSURLResponse *resp) {
                    if ([dict[@"response"][@"success"] intValue] == 1) {
                        [self updateDailyMacro:dict[@"data"]];
                    }
                } failure:^(NSHTTPURLResponse *resp, NSError *error) {
                    
                }];
            }else if(timeType == TimeTypeWeek){
                [params setObject:dateForAPI forKey:@"to_date"];
                [params setObject:startDateForAPI forKey:@"from_date"];
                [params setObject:@"total" forKey:@"type"];
                [[EFGAPIConnector sharedManager] getNutritionMacroWeek:[params mutableCopy] success:^(NSDictionary *dict, NSURLResponse *resp) {
                    if ([dict[@"response"][@"success"] intValue] == 1) {
                        [self updateWeeklyMacro:dict[@"data"]];
                    }
                } failure:^(NSHTTPURLResponse *resp, NSError *error) {
                    
                }];

            }
        }
            break;
            
        default:
            break;
    }
    
}


//- (NSArray *)calculatePercentageForValues:(NSArray *)array{
//    int total = 0;
//    for (NSNumber *n in array) {
//        total = total + n.intValue;
//    }
//    NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
//    if (total != 0) {
//        for (NSNumber *n in array) {
//            [mutableArray addObject:[NSNumber numberWithFloat:(n.intValue * 100) / total]];
//        }
//    }else{
//        for (int i = 0; i< array.count;i++) {
//            [mutableArray addObject:[NSNumber numberWithFloat:0]];
//        }
//    }
//    return [mutableArray mutableCopy];
//}

#pragma mark - Update UI Methods
- (void)updateDailyCalorie : (NSDictionary *)dict{
    NSLog(@"updating CalorieDaily");
    //Calculating Percentage
    
    NSNumber *breakfastValue = [NSNumber numberWithFloat:[dict[@"diary_details"][@"breakfast"] floatValue]];
    NSNumber *lunchValue = [NSNumber numberWithFloat:[dict[@"diary_details"][@"lunch"] floatValue]];
    NSNumber *snacksValue = [NSNumber numberWithFloat:[dict[@"diary_details"][@"snacks"] floatValue]];
    NSNumber *dinnerValue = [NSNumber numberWithFloat:[dict[@"diary_details"][@"dinner"] floatValue]];

    NSDictionary *v = @{
                           @"breakfast" : breakfastValue,
                           @"lunch" : lunchValue,
                           @"snacks" : snacksValue,
                           @"dinner" : dinnerValue,
                           @"goal" : dict[@"calorie_details"][@"goal"],
                           @"net_calories" : dict[@"calorie_details"][@"net_calories"],
                           @"total_calories" : dict[@"calorie_details"][@"total_calories"]
                           };
    [calorieView updateView:v];
    
}

- (void)updateWeeklyCalorie : (NSDictionary *)dict{
    NSMutableDictionary *master = [[NSMutableDictionary alloc] init];
    NSArray *timeArray = [NSArray arrayWithObjects:@"breakfast_graph",@"dinner_graph",@"lunch_graph",@"snacks_graph",nil];
    for (NSString *s in timeArray) {
        NSArray *a = dict[@"graph_data"][s];
        for (NSDictionary *d in a) {
            float alreadyValue;
            if ([master objectForKey:d[@"date"]] != [NSNull null] && [master objectForKey:d[@"date"]] != nil) {
                alreadyValue = [[master objectForKey:d[@"date"]] floatValue];
            }else{
                alreadyValue = 0;
            }
            [master setObject:[NSNumber numberWithFloat:[d[@"calorie"] floatValue] + alreadyValue] forKey:d[@"date"]];
        }
    }
    
    NSDictionary *additionalData = @{
                                     @"total" : dict[@"calorie_details"][@"Calories Under Weekly Goal"],
                                     @"avg" : dict[@"calorie_details"][@"Daily Average"],
                                     @"goal" : dict[@"calorie_details"][@"Goal"]
                                     };
    
    [calorieView updateBarChart:[master mutableCopy] andAdditionalData:additionalData];
    NSLog(@"updating CalorieWeekly");
}

- (void)updateDailyNutrition : (NSArray *)dict{
    NSMutableArray *ar = [[NSMutableArray alloc] init];
    for (NSDictionary *d in dict) {
        NSString *goal, *left, *name, *total;
        if (d[@"goal"]!= [NSNull null] && d[@"goal"] != nil) {
            goal = [NSString stringWithFormat:@"%@",d[@"goal"]];
        }else{
            goal = @"0";
        }
        if (d[@"left"]!= [NSNull null] && d[@"left"] != nil) {
            left = [NSString stringWithFormat:@"%@",d[@"left"]];
        }else{
            left = @"0";
        }
        if (d[@"total"]!= [NSNull null] && d[@"total"] != nil) {
            total = [NSString stringWithFormat:@"%@",d[@"total"]];
        }else{
            total = @"0";
        }
        name = d[@"name"];
        
        
        NSDictionary *di = @{
                             @"name" : name,
                             @"avg" : total,
                             @"goal" : goal,
                             @"left" : left
                             };
        
        [ar addObject:di];
    }
    [nutritionView updateData:[ar mutableCopy]];
}

- (void)updateWeeklyNutrition : (NSDictionary *)dict{
    NSMutableArray *ar = [[NSMutableArray alloc] init];
    for (NSDictionary *d in dict) {
        NSString *goal, *left, *name, *total;
        if (d[@"goal"]!= [NSNull null] && d[@"goal"] != nil) {
            goal = [NSString stringWithFormat:@"%@",d[@"goal"]];
        }else{
            goal = @"0";
        }
        if (d[@"left"]!= [NSNull null] && d[@"left"] != nil) {
            left = [NSString stringWithFormat:@"%@",d[@"left"]];
        }else{
            left = @"0";
        }
        if (d[@"avg"]!= [NSNull null] && d[@"avg"] != nil) {
            total = [NSString stringWithFormat:@"%@",d[@"avg"]];
        }else{
            total = @"0";
        }
        name = d[@"name"];
        
        
        NSDictionary *di = @{
                             @"name" : name,
                             @"avg" : total,
                             @"goal" : goal,
                             @"left" : left
                             };
        
        [ar addObject:di];
    }
    [nutritionView updateData:[ar mutableCopy]];
    NSLog(@"updating NutritionWeekly");
}

- (void)updateDailyMacro : (NSDictionary *)dict{
    [macroView updateView:dict];
    NSLog(@"updating MacroDaily");
}

- (void)updateWeeklyMacro : (NSDictionary *)dict{
    NSMutableDictionary *master = [[NSMutableDictionary alloc] init];
    NSArray *timeArray = [NSArray arrayWithObjects:@"carbohydrates_graph",@"fat_graph",@"proteins_graph",nil];
    NSArray *keys = [NSArray arrayWithObjects:@"carbohydrates",@"fat",@"prrotein",nil];
    int i = 0;
    for (NSString *s in timeArray) {
        NSArray *a = dict[@"graph_data"][s];
        for (NSDictionary *d in a) {
            float alreadyValue;
            if ([master objectForKey:d[@"date"]] != [NSNull null] && [master objectForKey:d[@"date"]] != nil) {
                alreadyValue = [[master objectForKey:d[@"date"]] floatValue];
            }else{
                alreadyValue = 0;
            }
            
            float v = 0;
            if (d[keys[i]] != [NSNull null] && d[keys[i]] != nil) {
                v = [d[keys[i]] floatValue];
            }
            
            [master setObject:[NSNumber numberWithFloat:v + alreadyValue] forKey:d[@"date"]];
        }
        i++;
    }
    
    NSDictionary *additionalData = @{
                                     @"carbohydrates" : dict[@"carbohydrates_avg"],
                                     @"proteins" : dict[@"proteins_avg"],
                                     @"fat" : dict[@"fat_avg"]
                                     };
    
    [macroView updateBarChart:[master mutableCopy] andAdditionalData:additionalData];

    NSLog(@"updating MacroWeekly");
}

#pragma mark - Reveal Menu Bar

- (void)setupMenuController{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma Date Picker Protocol
-(void)datePickerCancelPressed:(THDatePickerViewController *)datePicker{
    [datePicker dismissSemiModalViewWithCompletion:^{
        
    }];
}

- (void)datePickerDonePressed:(THDatePickerViewController *)datePicker{
    [timeInterval changeToDate:datePicker.date];
    [datePicker dismissSemiModalViewWithCompletion:^{
        
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
