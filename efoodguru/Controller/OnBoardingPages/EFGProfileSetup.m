//
//  EFGProfileSetup.m
//  efoodguru
//
//  Created by Ranjithkumar on 11/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGProfileSetup.h"
#import "EFGProgressView.h"
#import "EFGUtilities.h"
#import "EFGProfileQ1.h"
#import "EFGProfileQ2.h"
#import "EFGProfileQ3.h"
#import "EFGProfileQ4.h"
#import "EFGAPIConnector.h"
#import "EFGHomeViewController.h"
#import "SVProgressHUD.h"
#import "MMDrawerBarButtonItem.h"
#import "JSONModel.h"
#import "EFGProfileSetupModel.h"

@interface EFGProfileSetup ()

@property (nonatomic, strong) UIBarButtonItem *nextbutton;

@end

@implementation EFGProfileSetup{
    float xPos;
    float yPos;
    int currentView;
    EFGProgressView *progressBar;
    EFGProfileQ1 * question1;
    EFGProfileQ2 * question2;
    EFGProfileQ3 * question3;
    EFGProfileQ4 * question4;
    
    UIButton *back;
    UIButton *forward;
    
    //Avoidance Data
//    NSArray *q1Data;
//    NSArray *q2Data;
//    NSString *q3Data;
//    NSString *q4data;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationItem.hidesBackButton = YES;
    self.navigationItem.hidesBackButton = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initSetupWithNavBar:(BOOL)hamMenu{
    
    if (hamMenu) {
        [self setupMenuController];
    }else{
        self.navigationController.navigationBarHidden = YES;
    }
    
    self.view.backgroundColor = [EFGUtilities themeBackgroundGray];
    xPos = 0;
    yPos = STATUSBAR_HEIGHT + NAVIGATIONBAR_HEIGHT + 50;
    NSLog(@"height %f",NAVIGATIONBAR_HEIGHT);
    currentView = 1;
    [self addNavigationButtons];
    [self addProgress];
    [self setTitlePage:1];
   // [self addNextButton];
    [self showView:1];
}

- (void)addNavigationButtons{
    back = [[UIButton alloc] init];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    back.titleLabel.font = [UIFont systemFontOfSize:15];
    [back setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [back sizeToFit];
    CGRect frame = back.frame;
    frame.origin.x = 5;
    frame.origin.y = yPos + self.view.frame.size.height * 0.06 / 2 - frame.size.height / 2;
    frame.size.width += 10;
    back.frame = frame;
    [back addTarget:self action:@selector(backTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    forward = [[UIButton alloc] init];
    [forward setTitle:@"Next" forState:UIControlStateNormal];
    [forward setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    forward.titleLabel.font = [UIFont systemFontOfSize:15];
    [forward sizeToFit];
    frame = forward.frame;
    frame.origin.x = self.view.frame.size.width - frame.size.width - 10;
    frame.origin.y = yPos + self.view.frame.size.height * 0.06 / 2 - frame.size.height / 2;
    frame.size.width += 10;
    [forward addTarget:self action:@selector(nextPressed) forControlEvents:UIControlEventTouchUpInside];
    forward.frame = frame;
    [self.view addSubview:forward];
}

- (void)addProgress{
    progressBar = [[EFGProgressView alloc] initWithFrame:CGRectMake(back.frame.origin.x + back.frame.size.width + 5, yPos, self.view.frame.size.width - 2 * forward.frame.size.width - 20, self.view.frame.size.height * 0.06)];
    progressBar.backgroundColor = [EFGUtilities themeBackgroundGray];
    [progressBar initProgressBar];
    [self.view addSubview:progressBar];
    yPos += progressBar.frame.size.height;
}

- (void)setTitlePage:(int)page{
    self.title = [NSString stringWithFormat:@"%d",page];
}

- (void)addNextButton{
    self.nextbutton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(nextPressed)];
    self.navigationItem.rightBarButtonItem = self.nextbutton;
}

- (void)showView:(int)count{
    
    [progressBar makeProgress:count];
    [self setTitlePage:count];
    NSDictionary *param = @{
                            @"username" : [EFGUtilities getUserUsername],
                            @"password" : [EFGUtilities getUserPassword],
                            };
    switch (count) {
        case 1:
        {
            if (!question1) {
                [SVProgressHUD show];
                [[EFGAPIConnector sharedManager] getAvoidance:param success:^(NSDictionary *dict, NSURLResponse *resp) {
                    NSLog(@"DATA %@",dict);
                    [SVProgressHUD dismiss];
                    
                    NSString *dataToSend;
                    if (dict[@"data"][@"new"] != [NSNull null]  && dict[@"data"][@"new"] != nil) {
                        dataToSend = @"new";
                    }else{
                        dataToSend = @"old";
                    }
                    
                    question1 = [[EFGProfileQ1 alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, self.view.frame.size.height - yPos) andData:dict[@"data"][dataToSend]];
                    question1.backgroundColor = [EFGUtilities themeBackgroundGray];
                    question1.delegate = self;
                    [self.view addSubview:question1];
                    
                    
                } failure:^(NSHTTPURLResponse *resp, NSError *err) {
                    NSLog(@"DATA %@",resp);
                    [SVProgressHUD dismiss];
                }];
            }
            [self.view bringSubviewToFront:question1];
        }
            break;
        case 2:
        {
            NSArray *array = [NSArray arrayWithObjects:@"Fats, including saturated and trans fats", @"Carbohydrates, including sugar and dietary fiber",@"Net carbohydrates",@"Protein",@"Salt or Sodium",@"Cholesterol",@"Vitamins",@"Minerals",nil];
            NSArray *idArray = [NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",nil];
            NSMutableArray *data = [[NSMutableArray alloc] init];
            int t = 0;
            for (NSString *str in array) {
                NSDictionary *dict = @{
                                       @"avoidance_label" : str,
                                       @"avoidance_id" : [idArray objectAtIndex:t],
                                       };
                [data addObject:dict];
                t++;
            }
            if (!question2) {
                question2 = [[EFGProfileQ2 alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, self.view.frame.size.height - yPos) andData:data];
                question2.backgroundColor = [EFGUtilities themeBackgroundGray];
                [self.view addSubview:question2];
            }
            [self.view bringSubviewToFront:question2];
        }
            break;
        case 3:{
            if (!question3) {
                question3 = [[EFGProfileQ3 alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, self.view.frame.size.height - yPos)];
                question3.backgroundColor = [EFGUtilities themeBackgroundGray];
                [self.view addSubview:question3];
            }
            [self.view bringSubviewToFront:question3];
        }
            break;
        case 4:{
            if (!question4) {
                question4 = [[EFGProfileQ4 alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, self.view.frame.size.height - yPos)];
                question4.backgroundColor = [EFGUtilities themeBackgroundGray];
                [self.view addSubview:question4];
            }
            [self.view bringSubviewToFront:question4];
        }
            break;
        default:
            break;
    }
}

-(void)openSubSelectionWithData:(NSArray *)subAvoidance{
    NSLog(@"data : %@",subAvoidance);
    EFGAvoidanceSelection *avoidanceSelection = [[EFGAvoidanceSelection alloc] init];
    avoidanceSelection.delegate = self;
    [self presentViewController:avoidanceSelection animated:YES completion:nil];
    [avoidanceSelection loadViewControllerWithData:subAvoidance];
}

- (void)saveData:(NSArray *)ar forAvoidance:(NSString *)avdValue{
    [question1 saveSubAvoidanceValue:ar forAvoidance:avdValue];
}

- (void)nextPressed{
    switch (currentView) {
        case 1:{
            [self showView:2];
            currentView++;
        }
            break;
        case 2:
            [self showView:3];
            currentView++;
            break;
        case 3:
            [self showView:4];
            currentView++;
            break;
        case 4:
            [self saveAvoidance];
            break;
        default:
            break;
    }
}

-(void)backTapped{
    switch (currentView) {
        case 1:{

        }
            break;
        case 2:
            [self showView:1];
            currentView--;
            break;
        case 3:
            [self showView:2];
            currentView--;
            break;
        case 4:
            [self showView:3];
            currentView--;
            break;
        default:
            break;
    }
}

- (void)gotoHomeScreen{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GO_TO_HOME_CONTROLLER" object:nil];
}

- (void) saveAvoidance{
    if ([self validateAvoidance]) {
        NSDictionary *params = @{
                                 @"username" : [EFGUtilities getUserUsername],
                                 @"password" : [EFGUtilities getUserPassword],
                                 @"avoidance_list" : question1.answerData,
                                 @"nutrition_names" : question2.answerData,
                                 @"organic_food_preference" : question3.answerData,
                                 @"default_serving_size" : [question4 getAnswers]
                                 };
        
        [[EFGAPIConnector sharedManager] saveAvoidance:params success:^(NSDictionary *dict, NSURLResponse *resp) {
            [self gotoHomeScreen];
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [self gotoHomeScreen];
        }];
    }
}

- (BOOL)validateAvoidance{
    if (question1.answerData == nil) {
        question1.answerData = [[NSArray alloc] init];
    }
    if (question2.answerData == nil) {
        question2.answerData = [[NSDictionary alloc] init];
    }
    if (question3.answerData == nil) {
        question3.answerData = [NSNumber numberWithInt:0];
    }
    if (question4.answerData == nil) {
        question4.answerData = @"";
    }
    
    
    if (question1.answerData != nil && question2.answerData != nil && question3.answerData != nil && [question4 getAnswers] != nil) {
        return true;
    }else{
        [EFGUtilities showAlertWithTitle:@"Oh oh !" message:@"It Seems some questions are not answered. Please answer all questions." actionTitle:@"Okay" inViewController:self];
        return false;
    }
}

#pragma mark - Reveal Menu Bar

- (void)setupMenuController{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
