//
//  SignUpDetailVC.h
//  efoodguru
//
//  Created by Ranjithkumar on 09/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGCameraViewController.h"

@interface EFGSignUpDetailVC : UIViewController <UITextFieldDelegate, TGCameraDelegate, UIGestureRecognizerDelegate>

@end
