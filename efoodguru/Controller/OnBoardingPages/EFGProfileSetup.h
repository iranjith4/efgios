//
//  EFGProfileSetup.h
//  efoodguru
//
//  Created by Ranjithkumar on 11/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import "EFGProfileQ1.h"
#import "EFGAvoidanceSelection.h"

@interface EFGProfileSetup : UIViewController <EFGProfileQ1Protocol, EFGAvoidanceSubSelectionProtocol>

- (void) initSetupWithNavBar:(BOOL)hamMenu;

@end
