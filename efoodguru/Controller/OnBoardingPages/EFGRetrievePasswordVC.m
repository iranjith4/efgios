//
//  EFGRetrievePasswordVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 10/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGRetrievePasswordVC.h"
#import "EFGUtilities.h"

@interface EFGRetrievePasswordVC ()

@end

@implementation EFGRetrievePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    // Do any additional setup after loading the view.
}

- (void)initUI{
    self.title = @"Retrieve Password";
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:[EFGUtilities themeColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
