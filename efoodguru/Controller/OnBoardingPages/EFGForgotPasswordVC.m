//
//  ForgotPasswordVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 09/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGForgotPasswordVC.h"
#import "EFGRetrievePasswordVC.h"
#import "EFGUtilities.h"
#import "EFGAPIConnector.h"
#import "SVProgressHUD.h"

@interface EFGForgotPasswordVC ()
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;

@end

@implementation EFGForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    // Do any additional setup after loading the view.
}

- (void)initUI{
    self.title = @"Forgot Password";
    self.emailTextField.delegate = self;
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:[EFGUtilities themeColor]];
}

- (IBAction)submitButtonPressed:(id)sender {
    
    if ([EFGUtilities validateEmail:self.emailTextField.text]) {
        NSDictionary *params = @{
                                 @"email" : self.emailTextField.text
                                 };
        [SVProgressHUD show];
        [[EFGAPIConnector sharedManager] forgotPasswordWithParam:params success:^(NSDictionary *dict, NSURLResponse *resp) {
            [SVProgressHUD dismiss];
            
            BOOL success = [dict[@"response"][@"success"] boolValue];
            if (success) {
                EFGRetrievePasswordVC *retrivePasswordVC = [STORYBOARD instantiateViewControllerWithIdentifier:VC_RETRIVEPASSWORD];
                [self.navigationController pushViewController:retrivePasswordVC animated:YES];
            }else{
                [EFGUtilities showAlertWithTitle:@"Error" message:dict[@"data"][@"result"][@"message"] actionTitle:@"Ok" inViewController:self];
            }
            
        } failure:^(NSHTTPURLResponse *resp, NSError *error) {
            [SVProgressHUD dismiss];
            [EFGUtilities showAlertWithTitle:@"Error !" message:@"Something Went Wrong. Please Try again later." actionTitle:@"Ok" inViewController:self];
        }];

    }else{
        [EFGUtilities showAlertWithTitle:@"Email Error !" message:@"Email need to be a Valid One." actionTitle:@"Ok" inViewController:self];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
