//
//  EFGAvoidanceSelection.h
//  efoodguru
//
//  Created by Ranjithkumar on 02/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFGNavigationView.h"

@protocol EFGAvoidanceSubSelectionProtocol <NSObject>

- (void)saveData:(NSArray *)ar forAvoidance : (NSString *)avdValue;

@end

@interface EFGAvoidanceSelection : UIViewController <UITableViewDataSource, UITableViewDelegate,EFGNavigationProtocol>

@property (nonatomic) id <EFGAvoidanceSubSelectionProtocol> delegate;

- (void)loadViewControllerWithData : (NSArray *)array;

@end
