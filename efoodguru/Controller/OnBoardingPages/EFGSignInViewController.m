//
//  SignInViewController.m
//  efoodguru
//
//  Created by Ranjithkumar on 09/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGSignInViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "EFGUtilities.h"
#import "EFGProfileSetup.h"
#import "EFGAPIConnector.h"

@interface EFGSignInViewController ()
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UITextField *userNameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UIButton *logInButton;
@property (strong, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (strong, nonatomic) IBOutlet FBSDKLoginButton *fbLoginButton;

@end

@implementation EFGSignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self viewUISetup];
    // Do any additional setup after loading the view.
}

- (void)viewUISetup {
    self.title = @"Log In";
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:[EFGUtilities themeColor]];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLayoutSubviews{
    self.logInButton.layer.cornerRadius = 4.0;
    self.fbLoginButton.layer.cornerRadius = 4.0;
}

#pragma mark - Button Events

- (IBAction)loginButton:(id)sender {
    //Check for the API and move to the home controller
    //If the User Logs in, then we shows the EFGProfileSetup First
    //Handling Login
    
    if ([self validateAllfields]) {
        NSDictionary *dict = @{
                               @"username" : self.userNameField.text,
                               @"password" : self.passwordField.text
                               };
        [[EFGAPIConnector sharedManager] loginWithParams:dict success:^(NSDictionary *dict, NSURLResponse *resp) {
            NSLog(@"login %@",dict);
            BOOL success = [dict[@"response"][@"success"] boolValue];
            if (success) {
                //Get User Details
                [self getUserDetails];
            }else{
                [EFGUtilities showAlertWithTitle:@"Pleasse Check" message:dict[@"data"][@"result"][@"message"] actionTitle:@"OK" inViewController:self];
            }
        } failure:^(NSHTTPURLResponse *resp, NSError *err) {
            NSLog(@"resp");
            [EFGUtilities showAlertWithTitle:@"Pleasse Check" message:@"Something Went wrong. Please try later." actionTitle:@"OK" inViewController:self];
        }];
    }
}

- (void)getUserDetails{
    NSDictionary *params = @{
                             @"username" : self.userNameField.text,
                             @"password" : self.passwordField.text
                             };
    [[EFGAPIConnector sharedManager] getUserDetails:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        NSLog(@"userdata : %@",dict);
        if ([dict[@"response"][@"success"] boolValue]) {
            [EFGUtilities setLoggedIn:YES];
            [EFGUtilities saveUserDetails:dict];
            [self gotoHomeScreen];
        }else{
            [EFGUtilities showAlertWithTitle:@"Error" message:@"Something Went Wrong. Please try again Later." actionTitle:@"OK" inViewController:self];
        }
    } failure:^(NSHTTPURLResponse *resp, NSError *err) {
        NSLog(@"failure resp : %@",resp);
        [EFGUtilities showAlertWithTitle:@"Error" message:@"Something Went Wrong. Please try again Later." actionTitle:@"OK" inViewController:self];
    }];
}

- (void)gotoHomeScreen{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GO_TO_HOME_CONTROLLER" object:nil];
}

- (void)navigateToProfileSetup{
        EFGProfileSetup *profileSetup = [[EFGProfileSetup alloc] init];
        [profileSetup initSetupWithNavBar:NO];
        [self.navigationController pushViewController:profileSetup animated:YES];
}

- (IBAction)forgotPasswordButton:(id)sender {
    
}

- (IBAction)facebookLogin:(id)sender {
}

- (BOOL)validateAllfields{
    BOOL validation;
    NSString *message;
    if ([self.userNameField.text isEqualToString:@""] || [self.passwordField.text isEqualToString:@""]) {
        validation = NO;
        message = @"Email or Password Cannot be empty.";
    }else{
//        if ([EFGUtilities validateEmail:self.userNameField.text]) {
//            validation = YES;
//        }else{
//            validation = YES;
//            message = @"Email is not valid.Please check your email address.";
//        }
        validation = YES;
    }
    if (validation) {
        return YES;
    }
    [EFGUtilities showAlertWithTitle:@"Please Check" message:message actionTitle:@"Ok" inViewController:self];
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
