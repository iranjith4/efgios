//
//  EFGAvoidanceSelection.m
//  efoodguru
//
//  Created by Ranjithkumar on 02/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGAvoidanceSelection.h"
#import "EFGUtilities.h"
#import "EFGBasicTableViewCell.h"

@interface EFGAvoidanceSelection (){
    UITableView *table;
    NSArray *avoidanceList;
    UIButton *doneButton;
    NSMutableArray *selectionData;
}

@end

@implementation EFGAvoidanceSelection

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
}

- (void)loadViewControllerWithData : (NSArray *)array{
    avoidanceList = array;
    [self formAnswersArray];
    [self initValues];
}

- (void)formAnswersArray {
    selectionData = [[NSMutableArray alloc] init];
    for (NSDictionary *avoidance in avoidanceList) {
        NSString *selectionValue = @"false";
        if (avoidance[@"subavoidance_selection_value"] != [NSNull null] && avoidance[@"subavoidance_selection_value"] != nil ){
            if ([avoidance[@"avoidance_selection_value"] intValue] == 1) {
                selectionValue = @"true";
            }else{
                selectionValue = @"false";
            }
        }
        NSDictionary *dict = @{
                               @"subavoidance_label" :avoidance[@"subavoidance_label"],
                               @"subavoidance_id" :avoidance[@"subavoidancelist_id"],
                               @"subavoidance_selection" : selectionValue
                               };

        [selectionData addObject:dict];
    }
    
}

- (void)initValues{
    EFGNavigationView *navView = [[EFGNavigationView alloc] initWithFrame:CGRectMake(0, STATUSBAR_HEIGHT , self.view.frame.size.width, 80)];
    [navView initViewsWithColor:[EFGUtilities themeColor] title:nil andBackText:@"Cancel"];
    navView.delegate = self;
    [self.view addSubview:navView];
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(10, navView.frame.size.height + navView.frame.origin.y + 10, self.view.frame.size.width - 20, self.view.frame.size.height - (navView.frame.size.height + navView.frame.origin.y + 10) - 80 ) style:UITableViewStylePlain];
    [table registerClass:[EFGBasicTableViewCell class] forCellReuseIdentifier:@"cell_basic"];
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
    table.layer.cornerRadius = 6.0;
    table.layer.borderWidth = 1;
    table.layer.borderColor = [UIColor darkGrayColor].CGColor;
    table.allowsMultipleSelection = YES;
    table.separatorInset = UIEdgeInsetsZero;
    
    doneButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width * 0.20, table.frame.origin.y + table.frame.size.height + 20, self.view.frame.size.width * 0.60, 40)];
    doneButton.backgroundColor = [EFGUtilities themeButtonBlueColor];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.layer.cornerRadius = 6.0;
    [doneButton addTarget:self action:@selector(doneTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:doneButton];
}

- (void)doneTapped{
    NSString *s = @"";
    if (avoidanceList.count) {
       s = avoidanceList.lastObject[@"avoidancelist_id"];
    }
    [self.delegate saveData:selectionData.mutableCopy forAvoidance: s];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return avoidanceList.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGBasicTableViewCell *cell = (EFGBasicTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell_basic" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[EFGBasicTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell_basic"];
    }
    if ([[avoidanceList objectAtIndex:indexPath.row][@"subavoidance_selection_value"] boolValue]) {
        [tableView
         selectRowAtIndexPath:indexPath
         animated:TRUE
         scrollPosition:UITableViewScrollPositionNone
         ];
        
        [[tableView delegate]
         tableView:tableView
         didSelectRowAtIndexPath:indexPath
         ];
    }
    [cell setupCell:[avoidanceList objectAtIndex:indexPath.row] isSub:YES];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGBasicTableViewCell *cell = (EFGBasicTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.checkImage.image = [UIImage imageNamed:@"check_on"];
    NSDictionary *dict = @{
                           @"subavoidance_label" :[avoidanceList objectAtIndex:indexPath.row][@"subavoidance_label"],
                           @"subavoidance_id" : [avoidanceList objectAtIndex:indexPath.row][@"subavoidancelist_id"],
                           @"subavoidance_selection" : @"true"
                           };
    [selectionData setObject:dict atIndexedSubscript:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGBasicTableViewCell *cell = (EFGBasicTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.checkImage.image = [UIImage imageNamed:@"check_off"];
    NSDictionary *dict = @{
                           @"subavoidance_label" :[avoidanceList objectAtIndex:indexPath.row][@"subavoidance_label"],
                           @"subavoidance_id" : [avoidanceList objectAtIndex:indexPath.row][@"subavoidancelist_id"],
                           @"subavoidance_selection" : @"false"
                           };
    [selectionData setObject:dict atIndexedSubscript:indexPath.row];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

- (void)backNavigationTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
