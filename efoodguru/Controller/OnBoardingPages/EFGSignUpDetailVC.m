//
//  SignUpDetailVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 09/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGSignUpDetailVC.h"
#import "EFGUtilities.h"
#import "EFGAPIConnector.h"
#import "EFGProfileSetup.h"
#import "ActionSheetPicker.h"

#define ELEMENT_HEIGHT 40
#define LABEL_FONT_SIZE 17
#define FIELD_X self.view.frame.size.width * 0.45

@interface EFGSignUpDetailVC ()

@end

@implementation EFGSignUpDetailVC{
    UIScrollView *scrollView;
    UIImageView *coverImage;
    UIImageView *profileImage;
    UILabel *nameLabel;
    UITextField *nameField;
    UILabel *emailLabel;
    UITextField *emailField;
    UILabel *passwordLabel;
    UITextField *passwordField;
    UILabel *confirmPasswordLabel;
    UITextField *confirmPasswordField;
    UILabel *userNameLabel;
    UITextField *userNameField;
    UILabel *countryLabel;
    UITextField *countryField;
    UILabel *zipCode;
    UITextField *zipCodeField;
    UIButton *signUpButton;
    float xPos;
    float yPos;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addNotifications];
    [self initSetup];
    [self initViews];
    // Do any additional setup after loading the view.
}


- (void)addNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}

- (void) initSetup {
    
    self.title = @"Sign Up";
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:[EFGUtilities themeColor]];
    
    xPos = 0;
    scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.scrollEnabled = YES;
    [self.view addSubview:scrollView];
}

- (void) initViews{
    //Setting Cover Image
    coverImage = [[UIImageView alloc] init];
    coverImage.frame = CGRectMake(xPos, yPos, self.view.frame.size.width, 80 * DEVICE_HEIGHT_RATIO);
    coverImage.backgroundColor = [UIColor grayColor];
    coverImage.image = [UIImage imageNamed:@"avatar.jpg"];
    [scrollView addSubview:coverImage];
    
    yPos += coverImage.frame.size.height;
    xPos = 10 * DEVICE_WIDTH_RATIO;
    
    //Setting Profile Image
    profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, 0, self.view.frame.size.width * 0.35, self.view.frame.size.width * 0.35)];
    profileImage.center = CGPointMake(profileImage.center.x, yPos);
    profileImage.backgroundColor = [UIColor lightGrayColor];
    profileImage.image = [UIImage imageNamed:@"avatar.jpg"];
    profileImage.userInteractionEnabled = YES;
    profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
    profileImage.layer.borderWidth = 2.0;
    [scrollView addSubview:profileImage];
    
    UIButton *changeImage = [[UIButton alloc] initWithFrame:CGRectMake(0, profileImage.frame.size.height - 40, profileImage.frame.size.width, 40)];
    [changeImage setTitle:@"Change" forState:UIControlStateNormal];
    [changeImage setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [changeImage addTarget:self action:@selector(tappedPhoto) forControlEvents:UIControlEventTouchUpInside];
    [profileImage addSubview:changeImage];
    
    yPos = profileImage.frame.origin.y + profileImage.frame.size.height + 10 *DEVICE_HEIGHT_RATIO;
    
//    // Name
//    nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
//    nameLabel.text = @"Name";
//    nameLabel.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
//    nameLabel.textColor = [EFGUtilities themeLabelBlackShade];
//    [scrollView addSubview:nameLabel];
//    
//    nameField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
//    nameField.center = CGPointMake(nameField.center.x, nameLabel.center.y);
//    nameField.borderStyle = UITextBorderStyleRoundedRect;
//    nameField.delegate = self;
//    [scrollView addSubview:nameField];
//    yPos += nameField.frame.size.height;
//    [self addALine];
    
    // UserName
    userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
    userNameLabel.text = @"User Name";
    userNameLabel.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
    userNameLabel.textColor = [EFGUtilities themeLabelBlackShade];
    [scrollView addSubview:userNameLabel];
    
    userNameField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
    userNameField.center = CGPointMake(userNameField.center.x, userNameLabel.center.y);
    userNameField.borderStyle = UITextBorderStyleRoundedRect;
    userNameField.delegate = self;
    [scrollView addSubview:userNameField];
    yPos += userNameField.frame.size.height;
    [self addALine];
    
    // Email
    emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
    emailLabel.text = @"Email";
    emailLabel.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
    emailLabel.textColor = [EFGUtilities themeLabelBlackShade];
    [scrollView addSubview:emailLabel];
    
    emailField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
    emailField.center = CGPointMake(emailField.center.x, emailLabel.center.y);
    emailField.borderStyle = UITextBorderStyleRoundedRect;
    emailField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailField.autocorrectionType = UITextAutocorrectionTypeNo;
    emailField.spellCheckingType = UITextSpellCheckingTypeNo;
    emailField.keyboardType = UIKeyboardTypeEmailAddress;
    emailField.delegate = self;
    [scrollView addSubview:emailField];
    yPos += emailField.frame.size.height;
    [self addALine];
    
    // Password
    passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
    passwordLabel.text = @"Password";
    passwordLabel.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
    passwordLabel.textColor = [EFGUtilities themeLabelBlackShade];
    [scrollView addSubview:passwordLabel];
    
    passwordField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
    passwordField.center = CGPointMake(passwordField.center.x, passwordLabel.center.y);
    passwordField.borderStyle = UITextBorderStyleRoundedRect;
    passwordField.secureTextEntry = YES;
    passwordField.delegate = self;
    [scrollView addSubview:passwordField];
    yPos += passwordField.frame.size.height;
    [self addALine];
    
    // confirm Password
    confirmPasswordLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
    confirmPasswordLabel.text = @"Conf. Password";
    confirmPasswordLabel.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
    confirmPasswordLabel.textColor = [EFGUtilities themeLabelBlackShade];
    [scrollView addSubview:confirmPasswordLabel];
    
    confirmPasswordField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
    confirmPasswordField.center = CGPointMake(confirmPasswordField.center.x, confirmPasswordLabel.center.y);
    confirmPasswordField.borderStyle = UITextBorderStyleRoundedRect;
    confirmPasswordField.secureTextEntry = YES;
    confirmPasswordField.delegate = self;
    [scrollView addSubview:confirmPasswordField];
    yPos += confirmPasswordField.frame.size.height;
    [self addALine];
    
    // Country is removed. So commenting out this line.
//    countryLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
//    countryLabel.text = @"Country";
//    countryLabel.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
//    countryLabel.textColor = [EFGUtilities themeLabelBlackShade];
//    [scrollView addSubview:countryLabel];
//    
//    countryField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
//    countryField.center = CGPointMake(countryField.center.x, countryLabel.center.y);
//    countryField.borderStyle = UITextBorderStyleRoundedRect;
//    countryField.delegate = self;
//    [scrollView addSubview:countryField];
//    yPos += countryField.frame.size.height;
//    [self addALine];
    
    // Zip Code
    zipCode = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - FIELD_X, ELEMENT_HEIGHT)];
    zipCode.text = @"Zipcode";
    zipCode.font = [EFGUtilities fontWithBold:LABEL_FONT_SIZE];
    zipCode.textColor = [EFGUtilities themeLabelBlackShade];
    [scrollView addSubview:zipCode];
    
    zipCodeField = [[UITextField alloc] initWithFrame:CGRectMake(FIELD_X, 0, self.view.frame.size.width - FIELD_X - 10, ELEMENT_HEIGHT)];
    zipCodeField.center = CGPointMake(zipCodeField.center.x, zipCode.center.y);
    zipCodeField.borderStyle = UITextBorderStyleRoundedRect;
    zipCodeField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    zipCodeField.delegate = self;
    [scrollView addSubview:zipCodeField];
    yPos += zipCodeField.frame.size.height;
    [self addALine];
    
    yPos += 10;
    
    //Sign Up Button
    signUpButton = [[UIButton alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width * 0.80, ELEMENT_HEIGHT)];
    signUpButton.center = CGPointMake(self.view.center.x, signUpButton.center.y);
    [signUpButton setTitle:@"SIGN UP" forState:UIControlStateNormal];
    signUpButton.titleLabel.font = [EFGUtilities fontWithBold:18];
    [signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    signUpButton.backgroundColor = [EFGUtilities themeButtonRedColor];
    [signUpButton addTarget:self action:@selector(signupTapped) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:signUpButton];
    
    yPos += 15;
    
    [self setScrollContentHeight];
}

- (void)tappedPhoto{
    TGCameraNavigationController *navigationController =
    [TGCameraNavigationController newWithCameraDelegate:self];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)addALine{
    yPos += 5;
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 2)];
    line.backgroundColor = [UIColor grayColor];
    [scrollView addSubview:line];
    yPos += line.frame.size.height + 5;
}

- (void)setScrollContentHeight{
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, yPos + NAVIGATIONBAR_HEIGHT + STATUSBAR_HEIGHT);
}

- (void)navigateToProfileSetup{
    EFGProfileSetup *profileSetup = [[EFGProfileSetup alloc] init];
    [profileSetup initSetupWithNavBar:NO];
    [self.navigationController pushViewController:profileSetup animated:YES];
}

#pragma mark - APIConnection

- (void)signupTapped{
    if ([self validateFields]) {
        NSDictionary *registerDetails = @{
                                          @"name" : @"name",
                                          @"email" : emailField.text,
                                          @"username" : userNameField.text,
                                          @"password" : passwordField.text,
                                          @"country" : /*countryField.text*/@"United States",
                                          @"zip_code" : zipCodeField.text,
                                          @"fb_login" : [NSNumber numberWithBool:YES],
                                          @"image_url" : @"",
                                          @"date_time" : [EFGUtilities getCurrentFormattedDate]
                                          };
        [[EFGAPIConnector sharedManager] registerWithParams:registerDetails success:^(NSDictionary *dict, NSURLResponse *resp) {
            NSLog(@"register %@",dict);
            if ([dict[@"response"][@"success"] boolValue]) {
                //Get User Data
                [self getUserDetails];
                
            }else{
                [EFGUtilities showAlertWithTitle:@"Error !" message:dict[@"data"][@"result"][@"message"] actionTitle:@"OK" inViewController:self];
            }
        } failure:^(NSHTTPURLResponse *resp, NSError *err) {
            NSLog(@"signuperror: %@ ",resp);
            [EFGUtilities showAlertWithTitle:@"Error !" message:@"Something Went Wrong. Please Try Later." actionTitle:@"OK" inViewController:self];
        }];
    }
}

- (BOOL)validateFields{
    if (![emailField.text isEqualToString:@""] && ![emailField.text isEqualToString:@""] && ![passwordField.text isEqualToString:@""] && ![confirmPasswordField.text isEqualToString:@""] && ![userNameField.text isEqualToString:@""] && ![zipCode.text isEqualToString:@""]) {
        if ([EFGUtilities validateEmail:emailField.text]) {
            if ([passwordField.text isEqualToString:confirmPasswordField.text]) {
                return YES;
            }else{
                [EFGUtilities showAlertWithTitle:@"Passwords doesn't match." message:@"Password and Confirm Password Doesn't matches." actionTitle:@"OK" inViewController:self];
                return NO;
            }
        }else{
            [EFGUtilities showAlertWithTitle:@"Email Invalid" message:@"Please check your email address." actionTitle:@"OK" inViewController:self];
            return NO;
        }
    }else{
        [EFGUtilities showAlertWithTitle:@"Cannot be Empty" message:@"Fields cannot be empty" actionTitle:@"OK" inViewController:self];
        return NO;
    }
}

- (void)getUserDetails{
    NSDictionary *params = @{
                             @"username" : userNameField.text,
                             @"password" : passwordField.text
                           };
    [[EFGAPIConnector sharedManager] getUserDetails:params success:^(NSDictionary *dict, NSURLResponse *resp) {
        NSLog(@"userdata : %@",dict);
        if ([dict[@"response"][@"success"] boolValue]) {
            [EFGUtilities setLoggedIn:YES];
            [EFGUtilities saveUserDetails:dict];
            [self navigateToProfileSetup];
        }else{
            [EFGUtilities showAlertWithTitle:@"Error" message:@"Something Went Wrong. Please try again Later." actionTitle:@"OK" inViewController:self];
        }
    } failure:^(NSHTTPURLResponse *resp, NSError *err) {
        [EFGUtilities showAlertWithTitle:@"Error" message:@"Something Went Wrong. Please try again Later." actionTitle:@"OK" inViewController:self];
    }];
}


#pragma mark - Text Field Delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == countryField) {
        [self loadCountryPicker];
        [countryField resignFirstResponder];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)keyboardDidShow: (NSNotification *) notif{
    if (userNameField.isFirstResponder || emailField.isFirstResponder) {
        
    }else{
        NSDictionary *keyboardInfo = [notif userInfo];
        NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
        float keyboardHeight = keyboardFrameBeginRect.size.height;
        scrollView.contentOffset = CGPointMake(0, keyboardHeight - self.navigationController.navigationBar.frame.size.height - 20);
    }
}

- (void)keyboardDidHide: (NSNotification *) notif{
    if (userNameField.isFirstResponder || emailField.isFirstResponder) {
     
    }else{
        scrollView.contentOffset = CGPointMake(0, - self.navigationController.navigationBar.frame.size.height - 20);
    }
}


- (void) loadCountryPicker{
    NSArray *colors = [NSArray arrayWithObjects:@"United States", @"Canada",nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Country"
                                            rows:colors
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           countryField.text = selectedValue;
                                           [countryField resignFirstResponder];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         [countryField resignFirstResponder];
                                     }
                                          origin:self.view];
}

#pragma mark - TGCameraDelegate required

- (void)cameraDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidTakePhoto:(UIImage *)image
{
    profileImage.image = image;
    coverImage.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidSelectAlbumPhoto:(UIImage *)image
{
    profileImage.image = image;
    coverImage.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
