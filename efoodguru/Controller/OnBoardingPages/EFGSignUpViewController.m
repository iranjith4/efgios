//
//  SignUpViewController.m
//  efoodguru
//
//  Created by Ranjithkumar on 09/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGSignUpViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "EFGUtilities.h"

@interface EFGSignUpViewController ()
@property (strong, nonatomic) IBOutlet UIButton *signupWithEmail;
@property (strong, nonatomic) IBOutlet FBSDKLoginButton *signupWithFacebook;
@property (strong, nonatomic) IBOutlet UILabel *facebookNoteLabel;

@end

@implementation EFGSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadInitUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title = @"Sign Up";
    self.navigationController.navigationBarHidden = NO;
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:[EFGUtilities themeColor]];
}

- (void)loadInitUI{
    self.title = @"Sign Up";
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:[EFGUtilities themeColor]];
}

-(void)viewDidLayoutSubviews{
    self.signupWithEmail.layer.cornerRadius = 4.0;
    self.signupWithFacebook.layer.cornerRadius = 4.0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
