//
//  StartingViewController.m
//  efoodguru
//
//  Created by Ranjithkumar on 06/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGStartingViewController.h"
#import "EFGUtilities.h"
#import "EFGSignUpViewController.h"
#import "EFGSignInViewController.h"

@interface EFGStartingViewController ()<UIScrollViewDelegate>

@end

@implementation EFGStartingViewController{
    float xPos;
    float yPos;
    UIImageView *bgImage;
    UIButton *logInButton;
    UIButton *signupButton;
    UIButton *demoButton;
    UIPageControl *imagePageControl;
    UIScrollView *imageScroll;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetup];
    // Do any additional setup after loading the view.
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self addBackgroundImage];
    [self addSlidingImages];
    [self addButtons];
}

- (void) initialSetup{
    xPos = 0;
    yPos = self.view.frame.size.height * 0.14;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)addBackgroundImage{
    bgImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    bgImage.image = [UIImage imageNamed:@"splsh"];
    [self.view addSubview:bgImage];
}

- (void) addSlidingImages{
    NSArray *imagesArray = [NSArray arrayWithObjects:@"img1",@"img2",@"img3",@"img4",@"img5",nil];
    int numberOfSlidingImages = (int)imagesArray.count;
    imageScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(10, yPos, self.view.frame.size.width - 20, self.view.frame.size.height * 0.33)];
   float scrollContentX = 0;
    imageScroll.pagingEnabled = YES;
    imageScroll.scrollEnabled = YES;
    imageScroll.delegate = self;
   [self.view addSubview:imageScroll];
    
    for (int i = 0; i < numberOfSlidingImages; i++) {
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(scrollContentX, 0, imageScroll.frame.size.width, imageScroll.frame.size.height)];
        image.image = [UIImage imageNamed:[imagesArray objectAtIndex:i]];
        image.backgroundColor = [UIColor orangeColor];
        [imageScroll addSubview:image];
        scrollContentX += image.frame.size.width;
    }
    imageScroll.contentSize = CGSizeMake(scrollContentX, imageScroll.frame.size.height);
    
    imagePageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(10, imageScroll.frame.origin.y + imageScroll.frame.size.height + 10, imageScroll.frame.size.width - 20 , 30)];
    imagePageControl.numberOfPages = numberOfSlidingImages;
    [self.view addSubview:imagePageControl];
    
    demoButton = [[UIButton alloc] initWithFrame:CGRectMake(10, imagePageControl.frame.origin.y + imagePageControl.frame.size.height + 5, self.view.frame.size.width - 20, 40)];
    [demoButton setTitle:@"Click to see Demo" forState:UIControlStateNormal];
    [demoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:demoButton];
}

- (void)addButtons{
    UIView *buttonsBg = [[UIView alloc] initWithFrame:CGRectMake(15, self.view.frame.size.height * 0.70, self.view.frame.size.width - 30, self.view.frame.size.height * 0.25)];
    buttonsBg.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [self.view addSubview:buttonsBg];
    
    float buttonHeight = buttonsBg.frame.size.height / 2 - 12.5;
    
    signupButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 10 , buttonsBg.frame.size.width - 30, buttonHeight)];
    signupButton.backgroundColor = [EFGUtilities themeButtonBlueColor];
    [signupButton setTitle:@"SIGN UP" forState:UIControlStateNormal];
    signupButton.titleLabel.font = [EFGUtilities fontWithBold:16];
    [signupButton addTarget:self action:@selector(signupTapped) forControlEvents:UIControlEventTouchUpInside];
    [signupButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    signupButton.layer.cornerRadius = 4.0;
    [buttonsBg addSubview:signupButton];
    
    logInButton = [[UIButton alloc] initWithFrame:CGRectMake(15, signupButton.frame.origin.y + signupButton.frame.size.height + 5 , buttonsBg.frame.size.width - 30, buttonHeight)];
    logInButton.backgroundColor = [EFGUtilities themeButtonRedColor];
    [logInButton setTitle:@"LOG IN" forState:UIControlStateNormal];
    [logInButton addTarget:self action:@selector(loginTapped) forControlEvents:UIControlEventTouchUpInside];
    logInButton.titleLabel.font = [EFGUtilities fontWithBold:16];
    [logInButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logInButton.layer.cornerRadius = 4.0;
    [buttonsBg addSubview:logInButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == imageScroll){
        float pageWidth = scrollView.frame.size.width;
        float fractionalPage = scrollView.contentOffset.x / pageWidth;
        int page = fractionalPage;
        imagePageControl.currentPage = page;
    }
}

#pragma mark - Button Actions

- (void)loginTapped{
    EFGSignInViewController *signInController = [STORYBOARD instantiateViewControllerWithIdentifier:VC_SIGNINCONTROLLER];
    [self.navigationController pushViewController:signInController animated:YES];
}

- (void)signupTapped{
    EFGSignUpViewController *signUpController = [STORYBOARD instantiateViewControllerWithIdentifier:VC_SIGNUPCONTROLLER];
    [self.navigationController pushViewController:signUpController animated:YES];
}


@end
