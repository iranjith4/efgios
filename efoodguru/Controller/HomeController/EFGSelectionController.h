//
//  EFGSelectionController.h
//  efoodguru
//
//  Created by Ranjithkumar on 25/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectionControllerProtocol <NSObject>

- (void)selectedIndex:(int)index andSubIndex:(int)subIndex;

@end

@interface EFGSelectionController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) id <SelectionControllerProtocol> delegate;
- (void)initSetupWithNamesArray: (NSArray *)names title: (NSString *)title titleImage:(UIImage *)titleImg index:(int)ind;

@end
