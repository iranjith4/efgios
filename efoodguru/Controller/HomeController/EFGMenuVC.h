//
//  EFGMenuVC.h
//  efoodguru
//
//  Created by Ranjithkumar on 16/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"

@interface EFGMenuVC : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end
