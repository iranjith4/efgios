//
//  EFGMenuVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 16/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGMenuVC.h"
#import "EFGUtilities.h"
#import "EFGHomeViewController.h"
#import "EFGLogoutController.h"
#import "EFGProfileSetup.h"
#import "EFGProfileVC.h"
#import "EFGProgressViewController.h"
#import "EFGNutritionViewController.h"
#import "EFGGoalsController.h"
#import "EFGGoalsResultVC.h"
#import "EFGAPIConnector.h"
#import "EFGDiaryViewController.h"

@interface EFGMenuVC ()

@end

@implementation EFGMenuVC{
    NSArray *menuTitle;
    NSArray *menuSubtitle;
    NSArray *menuImage;
    UITableView *menuTable;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialSetup];
    [self setupTable];
    
    
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initialSetup{
    self.navigationController.navigationBarHidden = YES;
    menuTitle = [NSArray arrayWithObjects:[EFGUtilities getUserUsername],@"Home",@"User Profile",@"Nutririon", @"Goals",@"Progress",@"Diary", @"User Support",@"Share",@"LogOut",nil];
    menuImage = [NSArray arrayWithObjects:@"contact",@"home",@"contact",@"nutrition",@"goals",@"progress",@"progress",@"support",@"share",@"logout",nil];
   // menuSubTitle = [NSArray arrayWithObjects:@"View profile", @"" nil]
}

- (void)setupTable{
    menuTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    [menuTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"menu_cell"];
    menuTable.delegate = self;
    menuTable.dataSource = self;
    [self.view addSubview:menuTable];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return menuTitle.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menu_cell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"menu_cell"];
    }
    cell.textLabel.text = [menuTitle objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:19];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        cell.backgroundColor = [EFGUtilities themeColor];
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    
    cell.imageView.image = [UIImage imageNamed:[menuImage objectAtIndex:indexPath.row]];
    cell.detailTextLabel.text = @"Subtitle Here";
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 100;
    }else{
        return 80;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{
            EFGProfileVC *profileVC = [[EFGProfileVC alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:profileVC];
            [self.mm_drawerController setCenterViewController:nav withCloseAnimation:YES completion:nil];
        }
            break;
            
        case 1:{
            EFGHomeViewController *homeController = [[EFGHomeViewController alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:homeController];
            [self.mm_drawerController setCenterViewController:nav withCloseAnimation:YES completion:nil];
        }
            break;
        case 2:{
            EFGProfileSetup *profileSetup = [[EFGProfileSetup alloc] init];
            [profileSetup initSetupWithNavBar:YES];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:profileSetup];
            [self.mm_drawerController setCenterViewController:nav withCloseAnimation:YES completion:nil];
        }
            break;
        case 3:{
            EFGNutritionViewController *nutition = [[EFGNutritionViewController alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:nutition];
            [self.mm_drawerController setCenterViewController:nav withCloseAnimation:YES completion:nil];
        }
            break;
        case 4:{
            
            NSDictionary *params = @{
                                     @"username" : [EFGUtilities getUserUsername],
                                     @"password" : [EFGUtilities getUserPassword],
                                     @"date_time" : [EFGUtilities getCurrentFormattedDate],
                                     };
            [EFGUtilities showProgress];
            [[EFGAPIConnector sharedManager] getGoalData:params success:^(NSDictionary *dict, NSURLResponse *resp) {
                if ([dict[@"response"][@"success"] intValue] == 1) {
                    EFGGoalsResultVC *goalsResultVC = [[EFGGoalsResultVC alloc] init];
                    [goalsResultVC initViewController];
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:goalsResultVC];
                    [self.mm_drawerController setCenterViewController:nav withFullCloseAnimation:YES completion:nil];
                }else{
                    EFGGoalsController *goals = [[EFGGoalsController alloc] init];
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:goals];
                    [self.mm_drawerController setCenterViewController:nav withFullCloseAnimation:YES completion:nil];
                }
                [EFGUtilities hideProgress];
            } failure:^(NSHTTPURLResponse *resp, NSError *error) {
                [EFGUtilities hideProgress];
                [EFGUtilities showAlertWithTitle:@"Error" message:@"Something went wrong with the server.Please try again later." actionTitle:@"OK" inViewController:self type:0];
            }];
        }
            break;
        case 5:{
            EFGProgressViewController *prgress = [[EFGProgressViewController alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:prgress];
            [self.mm_drawerController setCenterViewController:nav withCloseAnimation:YES completion:nil];
            
        }
            break;
        case 6:{
            EFGDiaryViewController *diary = [[EFGDiaryViewController alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:diary];
            [self.mm_drawerController setCenterViewController:nav withCloseAnimation:YES completion:nil];
        }
            break;
        case 7:{
            
        }
            break;
        case 8:{

        }
            break;
            
        case 9 :{
            //Make the Logout
            EFGLogoutController *logout = [[EFGLogoutController alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:logout];
            [self.mm_drawerController setCenterViewController:nav withCloseAnimation:YES completion:nil];
        }
            
        default:
            break;
    }
}


@end
