//
//  EFGProductDetailVC.h
//  efoodguru
//
//  Created by Ranjithkumar on 16/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFGNavigationView.h"

#define ELEMENT_SPACE 8
#define ALLERGENT_SPACE 5
#define NUTRIENT_SPACE 5

@interface EFGProductDetailVC : UIViewController <EFGNavigationProtocol>

@property (nonatomic, strong) UIScrollView *scroll;
@property (nonatomic, strong) UILabel *productName;
@property (nonatomic, strong) UILabel *brandName;
@property (nonatomic, strong) UILabel *manaufacturer;

- (void)loadPageWithData: (NSDictionary *)dict;

@end
