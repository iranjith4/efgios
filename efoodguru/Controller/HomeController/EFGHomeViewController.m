//
//  EFGHomeViewController.m
//  efoodguru
//
//  Created by Ranjithkumar on 16/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGHomeViewController.h"
#import "EFGUtilities.h"
#import "EFGAPIConnector.h"
#import "EFGProductDetailVC.h"
#import "MTBBarcodeScanner.h"
#import "SVProgressHUD.h"
#import "EFGMenuVC.h"
#import "EFGSelectionController.h"
#import "EFGExerciseController.h"
#import "EFGHomeSearchController.h"
#import "MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"
#import "EFGFoodVC.h"
#import "EFGWaterVC.h"
#import "EFGWeightVC.h"
#import "EFGGoalsStatus.h"

@interface EFGHomeViewController ()

@end

@implementation EFGHomeViewController{
    float xPos;
    float yPos;
    UIView *barcodeView;
    UIScrollView *imageScroll;
    UIPageControl *imagePageControl;
    MTBBarcodeScanner *scanner;
    UIBarButtonItem *cancelScan;
    UISearchBar *searchbar;
    UIBarButtonItem *dummyButton;
    EFGGoalsStatus *calorieView;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.hidesBackButton = YES;
    [self updateCaloriesData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetup];
    [self setupMenuController];
    [self addBarCodeButton];
    [self addSearchBar];
    [self addCalorieStatusView];
    [self addSlidingImages];
    [self addFloatingButton];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initialSetup{
    xPos = 0;
    yPos = 0;
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Home";
    
    barcodeView = [[UIView alloc] initWithFrame:self.view.frame];
    
    cancelScan = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelScan)];
    dummyButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
    
    [self.view addSubview:barcodeView];
    
    
    UIView *redline1 = [[UIView alloc] initWithFrame:CGRectMake(0, barcodeView.frame.size.height * 0.20, barcodeView.frame.size.width, 1)];
    redline1.userInteractionEnabled = YES;
    redline1.backgroundColor = [UIColor redColor];
    [barcodeView addSubview:redline1];
    
    UIView *redline2 = [[UIView alloc] initWithFrame:CGRectMake(0, barcodeView.frame.size.height * 0.70, barcodeView.frame.size.width, 1)];
    redline2.backgroundColor = [UIColor redColor];
    [barcodeView addSubview:redline2];
    barcodeView.backgroundColor = [UIColor whiteColor];
    barcodeView.hidden = YES;
}

- (void)cancelScan{
    barcodeView.hidden = YES;
    [scanner stopScanning];
    self.navigationItem.rightBarButtonItem = nil;
}

#pragma mark - Reveal Menu Bar

- (void)setupMenuController{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:NO];
}

-(void)leftDrawerButtonPress:(id)sender{
    if (searchbar.isFirstResponder) {
        [searchbar resignFirstResponder];
    }
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma mark - UI Elements

- (void)addFloatingButton{
    
    CGRect floatFrame = CGRectMake(self.view.frame.size.width - 80, self.view.frame.size.height - 80, 60, 60);
    UIScrollView *scroll = [[UIScrollView alloc] init];
    VCFloatingActionButton *addButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"plus"] andPressedImage:[UIImage imageNamed:@"cross"] withScrollview:scroll];
    [self.view addSubview:addButton];
    addButton.layer.cornerRadius = addButton.frame.size.width / 2;
    addButton.layer.masksToBounds = YES;
    addButton.delegate = self;
    addButton.imageArray = @[@"water",@"weight",@"food",@"exercise"];
    addButton.labelArray = @[@"Water",@"Weight",@"Food",@"Exercise"];
}

- (void)addCalorieStatusView{
    calorieView = [[EFGGoalsStatus alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width * 0.90, self.view.frame.size.height * 0.15)];
    [self.view addSubview:calorieView];
    calorieView.layer.cornerRadius = 4.0;
    calorieView.layer.borderColor = [EFGUtilities themeColor].CGColor;
    calorieView.layer.borderWidth = 1.0;
    [calorieView setGoal:@""];
    [calorieView setFood:@""];
    [calorieView setExercise:@""];
    [calorieView setRemaining:@""];
    yPos += calorieView.frame.size.height + 20;
}

- (void)updateCaloriesData{
    NSDictionary *params = @{
                             @"username" : [EFGUtilities getUserUsername],
                             @"password" : [EFGUtilities getUserPassword],
                             @"date_time" : [EFGUtilities getCurrentFormattedDate]
                             };
    [[EFGAPIConnector sharedManager] getCaloriesRemaining:params success:^(NSDictionary *dict, NSURLResponse *response) {
        
        if ([dict[@"response"][@"success"] intValue] == 1) {
            //Updating Remaining Caloiris Value
            NSDictionary *data = dict[@"data"];
            [calorieView setGoal:[NSString stringWithFormat:@"%@",data[@"goal"]]];
            [calorieView setExercise:[NSString stringWithFormat:@"%@",data[@"exercise"]]];
            [calorieView setFood:[NSString stringWithFormat:@"%@",data[@"food"]]];
            [calorieView setRemaining:[NSString stringWithFormat:@"%@",data[@"remaining"]]];
        }
        
    } failure:^(NSHTTPURLResponse *response, NSError *error) {
        
    }];
}



- (void) addSlidingImages{
    NSArray *blogImages = [NSArray arrayWithObjects:@"blog1",@"blog2",@"blog3",nil];
    int numberOfSlidingImages = blogImages.count;
    imageScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(10, yPos, self.view.frame.size.width - 20, self.view.frame.size.height * 0.33)];
    float scrollContentX = 0;
    imageScroll.pagingEnabled = YES;
    imageScroll.scrollEnabled = YES;
    imageScroll.delegate = self;
    [self.view addSubview:imageScroll];
    
    for (int i = 0; i < numberOfSlidingImages; i++) {
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(scrollContentX, 0, imageScroll.frame.size.width, imageScroll.frame.size.height)];
        image.image = [UIImage imageNamed:[blogImages objectAtIndex:i]];
        image.backgroundColor = [UIColor orangeColor];
        [imageScroll addSubview:image];
        scrollContentX += image.frame.size.width;
    }
    imageScroll.contentSize = CGSizeMake(scrollContentX, imageScroll.frame.size.height);
    
    imagePageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(10, imageScroll.frame.origin.y + imageScroll.frame.size.height + 10, imageScroll.frame.size.width - 20 , 30)];
    imagePageControl.numberOfPages = numberOfSlidingImages;
    [self.view addSubview:imagePageControl];
    
}

#pragma mark - Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == imageScroll){
        float pageWidth = scrollView.frame.size.width;
        float fractionalPage = scrollView.contentOffset.x / pageWidth;
        int page = fractionalPage;
        imagePageControl.currentPage = page;
    }
}


#pragma mark - Search Bar

- (void)addSearchBar{
    searchbar = [[UISearchBar alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width - xPos - 15, 40)];
    [self.view addSubview:searchbar];
    searchbar.searchBarStyle = UISearchBarStyleMinimal;
    searchbar.backgroundColor = [UIColor clearColor];
    searchbar.tintColor = [UIColor grayColor];
    searchbar.showsCancelButton = YES;
    searchbar.delegate = self;
    searchbar.placeholder = @"Search for Food Avoidance";
    yPos += searchbar.frame.size.height + 15;
    xPos = 15;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    EFGHomeSearchController *homeSearch = [[EFGHomeSearchController alloc] init];
    homeSearch.delegate = self;
    [homeSearch initSetUpForQuery:searchBar.text];
    [self presentViewController:homeSearch animated:YES completion:nil];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

#pragma mark - Bar Code Methods

- (void)addBarCodeButton{
    xPos = 15;
    yPos = STATUSBAR_HEIGHT + NAVIGATIONBAR_HEIGHT;
    
    UIButton *barcodeButton = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width * 0.15, self.view.frame.size.width * 0.15)];
    [barcodeButton setImage:[UIImage imageNamed:@"barcode_icon"] forState:UIControlStateNormal];
    [barcodeButton addTarget:self action:@selector(barcodeOpen) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:barcodeButton];
    
    xPos += barcodeButton.frame.size.width + 5;
}

- (void)barcodeOpen{
    [self.view bringSubviewToFront:barcodeView];
    scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:barcodeView];
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            barcodeView.hidden = NO;
            self.navigationItem.rightBarButtonItem = cancelScan;
            self.title = @"Scan";
            [scanner startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                barcodeView.hidden = YES;
                self.navigationItem.rightBarButtonItem = dummyButton;
                [scanner stopScanning];
                self.title = @"Home";
                NSLog(@"barcode : %@",code.stringValue);
                NSString * codeString;
                if(code.stringValue!=nil){
                    if([code.type isEqualToString:AVMetadataObjectTypeEAN13Code]){
                        if ([code.stringValue hasPrefix:@"0"] && [code.stringValue length] > 1)
                            codeString = [code.stringValue substringFromIndex:1];
                        NSLog(@"generated : %@",codeString);
                    }
                }else{
                    codeString = code.stringValue;
                }

                [self productData:codeString];
            }];
            
        } else {
            //TODO : The user denied access to the camera
        }
    }];
}

- (void)productData:(NSString *)upc{
    [SVProgressHUD show];
    NSDictionary *dict = @{
                           @"username" : [EFGUtilities getUserUsername],
                           @"password" : [EFGUtilities getUserPassword],
                           @"upc" : upc
                           };
    [[EFGAPIConnector sharedManager] productDataForUPCWithParam:dict success:^(NSDictionary *dict, NSURLResponse *resp) {
        
        if ([dict[@"response"][@"success"] intValue] == 1) {
            EFGProductDetailVC *productPage = [[EFGProductDetailVC alloc] init];
            [productPage loadPageWithData:dict];
            //[self.navigationController pushViewController:productPage animated:YES];
            [self presentViewController:productPage animated:YES completion:nil];
        }else{
            [EFGUtilities showAlertWithTitle:@"No Product." message:@"No Product for this Barcode." actionTitle:@"Ok" inViewController:self];
        }

        [SVProgressHUD dismiss];
        
    } failure:^(NSHTTPURLResponse *resp, NSError *err) {
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - Floating Button Methods

-(void)didSelectMenuOptionAtIndex:(NSInteger)row{
    NSArray *namesArray;
    UIImage *titleImage;
    NSString *title;
    switch (row) {
        case 0:
        {
            //Water
            EFGWaterVC *waterVC = [[EFGWaterVC alloc] init];
            [self presentViewController:waterVC animated:YES completion:nil];
        }
            break;
        case 1:
        {
            //Weight
            EFGWeightVC *weightVC = [[EFGWeightVC alloc] init];
            [self presentViewController:weightVC animated:YES completion:nil];
        }
            break;
        case 2:
        {
            //Food
            namesArray = [NSArray arrayWithObjects:@"Breakfast",@"Lunch",@"Dinner",@"Snacks",nil];
            title = @"Meals";
            titleImage = [UIImage imageNamed:@""];
        }
            break;
        case 3:
        {
            //Exercise
            namesArray = [NSArray arrayWithObjects:@"Cardiovascular",@"Strength",nil];
            title = @"Exercise";
            titleImage = [UIImage imageNamed:@""];
        }
            break;
            
        default:
            break;
    }

    if (row == 2 || row == 3) {
        EFGSelectionController *selectionController = [[EFGSelectionController alloc] init];
        [selectionController initSetupWithNamesArray:namesArray title:title titleImage:titleImage index:(int)row];
        selectionController.delegate = self;
        [self presentViewController:selectionController animated:YES completion:nil];
    }
}

- (void)selectedIndex:(int)index andSubIndex:(int)subIndex{
    switch (index) {
            
        case 2:{
                //Food
            EFGFoodVC *food = [[EFGFoodVC alloc] init];
            switch (subIndex) {
                case 0:{
                    [food initWithFoodType:MealTypeBreakFast];
                }
                    
                    break;
                case 1:{
                    [food initWithFoodType:MealTypeLunch];
                }
                    
                    break;
                    
                case 2:{
                    [food initWithFoodType:MealTypeSnacks];
                }
                    
                    break;
                    
                case 3:{
                    [food initWithFoodType:MealTypeDinner];
                }
                    break;
                    
                default:
                    [food initWithFoodType:MealTypeBreakFast];
                    break;
            }
            [self presentViewController:food animated:YES completion:nil];
                    }
            break;
            
            
        case 3:{
            EFGExerciseController *exerciseController = [[EFGExerciseController alloc] init];
            switch (subIndex) {
                case 0:
                {
                    [exerciseController loadViewControllerFor:ExerciseTypeCardiac];
                }
                    break;
                    
                case 1:
                {
                    [exerciseController loadViewControllerFor:ExerciseTypeStrength];
                }
                    break;
                    
                default:
                    break;
            }
            [self.navigationController pushViewController:exerciseController animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Search Control Protocols

-(void)selectedUPC:(NSString *)upc{
    [self productData:upc];
}


@end
