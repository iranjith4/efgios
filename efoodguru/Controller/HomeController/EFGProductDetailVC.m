//
//  EFGProductDetailVC.m
//  efoodguru
//
//  Created by Ranjithkumar on 16/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGProductDetailVC.h"
#import "EFGUtilities.h"

@interface EFGProductDetailVC (){
    EFGNavigationView *navView;
}

@end

@implementation EFGProductDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadInitialSetup{
    float yPos = STATUSBAR_HEIGHT;
    self.view.backgroundColor = [UIColor whiteColor];
    
    yPos += navView.frame.size.height + 5;
    self.scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:self.scroll];
    
    navView = [[EFGNavigationView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 65)];
    [navView initViewsWithColor:[EFGUtilities themeColor] title:@"Product Report" andBackText:@"Back"];
    navView.delegate = self;
    [self.scroll addSubview:navView];
    
    
}

- (void)loadPageWithData: (NSDictionary *)dict{
    
    [self loadInitialSetup];
    
    float x = 5;
    float y = STATUSBAR_HEIGHT + navView.frame.size.height + 5;
    
    //ProductName
    self.productName = [[UILabel alloc] initWithFrame:CGRectMake(x, y, self.scroll.frame.size.width - 2 * x, 0)];
    self.productName.text = dict[@"data"][@"ProductName"];
    self.productName.font = [UIFont systemFontOfSize:28];
    self.productName.textColor = [UIColor grayColor];
    self.productName.numberOfLines = 0;
    [self.productName sizeToFit];
    [self.scroll addSubview:self.productName];
    y += self.productName.frame.size.height + ELEMENT_SPACE;
    
//    //Brand
//    self.brandName = [[UILabel alloc] initWithFrame:CGRectMake(x, y, self.scroll.frame.size.width - 2 * x, 0)];
//    self.brandName.text = dict[@"score"][@"product"][@"brand"];
//    self.brandName.font = [UIFont systemFontOfSize:18];
//    self.brandName.textColor = [EFGUtilities themeColor];
//    self.brandName.numberOfLines = 0;
//    [self.brandName sizeToFit];
//    [self.scroll addSubview:self.brandName];
//    y += self.brandName.frame.size.height + ELEMENT_SPACE;
    
    //Manufatured by --- Using Organic Data
    self.manaufacturer = [[UILabel alloc] initWithFrame:CGRectMake(x, y, self.scroll.frame.size.width - 2 * x, 0)];
    self.manaufacturer.text = dict[@"data"][@"Organic"];
    self.manaufacturer.font = [UIFont italicSystemFontOfSize:13];
    self.manaufacturer.textColor = [UIColor grayColor];
    self.manaufacturer.numberOfLines = 0;
    [self.manaufacturer sizeToFit];
    [self.scroll addSubview:self.manaufacturer];
    y += self.manaufacturer.frame.size.height + ELEMENT_SPACE;
    
    
//    UILabel *allergensTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, y, self.scroll.frame.size.width, 40)];
//    allergensTitle.text = @"FOOD AVOIDANCE";
//    allergensTitle.font = [UIFont boldSystemFontOfSize:20];
//    allergensTitle.backgroundColor = [UIColor grayColor];
//    allergensTitle.textColor = [UIColor whiteColor];
//    [self.scroll addSubview:allergensTitle];
//    y += allergensTitle.frame.size.height + ELEMENT_SPACE;
//    
//    //Allergens
//    NSArray *allergens = dict[@"label"][@"allergens"];
//    for (NSDictionary *allergen in allergens) {
//        float allergenY = 3;
//        UIView *allergenView = [[UIView alloc] initWithFrame:CGRectMake(x, y, self.view.frame.size.width - 2 * x, 0)];
//        
//        UILabel *allergenName = [[UILabel alloc] initWithFrame:CGRectMake(0, allergenY, self.view.frame.size.width, 0)];
//        allergenName.text = allergen[@"allergen_name"];
//        allergenName.font = [UIFont boldSystemFontOfSize:18];
//        allergenName.textColor = [EFGUtilities themeColor];
//        allergenName.numberOfLines = 0;
//        [allergenName sizeToFit];
//        [allergenView addSubview:allergenName];
//        allergenY += allergenName.frame.size.height + ALLERGENT_SPACE;
//        
////        UILabel *allergenRedTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, allergenY, self.view.frame.size.width, 0)];
////        allergenRedTitle.text = @"ALLERGENT RED INGREDIENTS";
////        allergenRedTitle.font = [UIFont boldSystemFontOfSize:18];
////        allergenRedTitle.textColor = [UIColor redColor];
////        allergenRedTitle.numberOfLines = 0;
////        [allergenRedTitle sizeToFit];
////        [allergenView addSubview:allergenRedTitle];
////        allergenY += allergenRedTitle.frame.size.height + ALLERGENT_SPACE;
//
//        if (![allergen[@"allergen_red_ingredients"] isEqualToString:@""] && allergen[@"allergen_red_ingredients"] != nil) {
//            UILabel *allergenRed = [[UILabel alloc] initWithFrame:CGRectMake(0, allergenY, self.view.frame.size.width, 0)];
//            allergenRed.text = [NSString stringWithFormat:@"Red Ingredients : %@",allergen[@"allergen_red_ingredients"]];
//            allergenRed.font = [UIFont boldSystemFontOfSize:14];
//            allergenRed.textColor = [UIColor grayColor];
//            allergenRed.numberOfLines = 0;
//            [allergenRed sizeToFit];
//            [allergenView addSubview:allergenRed];
//            allergenY += allergenRed.frame.size.height + ALLERGENT_SPACE;
//        }
//        
////        UILabel *allergenYellowTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, allergenY, self.view.frame.size.width, 0)];
////        allergenYellowTitle.text = @"ALLERGENT YELLOW INGREDIENTS";
////        allergenYellowTitle.font = [UIFont boldSystemFontOfSize:18];
////        allergenYellowTitle.textColor = [UIColor yellowColor];
////        allergenYellowTitle.numberOfLines = 0;
////        [allergenYellowTitle sizeToFit];
////        [allergenView addSubview:allergenYellowTitle];
////        allergenY += allergenYellowTitle.frame.size.height + ALLERGENT_SPACE;
//        
//        if (![allergen[@"allergen_yellow_ingredients"] isEqualToString:@""] && allergen[@"allergen_yellow_ingredients"] != nil) {
//            UILabel *allergenYellow = [[UILabel alloc] initWithFrame:CGRectMake(0, allergenY, self.view.frame.size.width, 0)];
//            allergenYellow.text = [NSString stringWithFormat:@"Yellow Ingredients : %@",allergen[@"allergen_yellow_ingredients"]];
//            allergenYellow.font = [UIFont boldSystemFontOfSize:14];
//            allergenYellow.textColor = [UIColor grayColor];
//            allergenYellow.numberOfLines = 0;
//            [allergenYellow sizeToFit];
//            [allergenView addSubview:allergenYellow];
//            allergenY += allergenYellow.frame.size.height + ALLERGENT_SPACE;
//        }
//        
//        CGRect frame = allergenView.frame;
//        frame.size.height = allergenY;
//        allergenView.frame = frame;
//        
//        [self.scroll addSubview:allergenView];
//        
//        y += allergenView.frame.size.height + 5;
//    }
//    
//    
//    
    UILabel *nutrientsTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, y, self.scroll.frame.size.width, 40)];
    nutrientsTitle.text = @"NUTRIENTS";
    nutrientsTitle.font = [UIFont boldSystemFontOfSize:20];
    nutrientsTitle.backgroundColor = [UIColor grayColor];
    nutrientsTitle.textColor = [UIColor whiteColor];
    [self.scroll addSubview:nutrientsTitle];
    y += nutrientsTitle.frame.size.height + ELEMENT_SPACE;
//
//    //nutrients
    NSArray *nutrients = dict[@"data"][@"Nutrients"];
    for (NSDictionary *nutrient in nutrients) {
        float nutrientY = 3;
        UIView *nutrientView = [[UIView alloc] initWithFrame:CGRectMake(x, y, self.view.frame.size.width - 2 * x, 0)];
        
        UILabel *nutrientName = [[UILabel alloc] initWithFrame:CGRectMake(0, nutrientY, self.view.frame.size.width, 0)];
        nutrientName.text = nutrient[@"nutrient_name"];
        nutrientName.font = [UIFont boldSystemFontOfSize:18];
        nutrientName.textColor = [EFGUtilities themeColor];
        nutrientName.numberOfLines = 0;
        [nutrientName sizeToFit];
        [nutrientView addSubview:nutrientName];
        nutrientY += nutrientName.frame.size.height + NUTRIENT_SPACE;
    
        
        if ( nutrient[@"nutrient_value"] != nil) {
            
            NSString *text = [NSString stringWithFormat:@"Value : %@",nutrient[@"nutrient_value"]];
            if (![nutrient[@"nutrient_uom"] isEqualToString:@""] && nutrient[@"nutrient_uom"] != nil && ![nutrient[@"nutrient_uom"] isEqualToString:@"none"]) {
                text = [text stringByAppendingString:nutrient[@"nutrient_uom"]];
            }
            
            UILabel *nutrientVal = [[UILabel alloc] initWithFrame:CGRectMake(0, nutrientY, self.view.frame.size.width, 0)];
            nutrientVal.text = text;
            nutrientVal.font = [UIFont boldSystemFontOfSize:14];
            nutrientVal.textColor = [UIColor grayColor];
            nutrientVal.numberOfLines = 0;
            [nutrientVal sizeToFit];
            [nutrientView addSubview:nutrientVal];
            nutrientY += nutrientVal.frame.size.height + NUTRIENT_SPACE;
        }
        
        
        CGRect frame = nutrientView.frame;
        frame.size.height = nutrientY;
        nutrientView.frame = frame;
        
        [self.scroll addSubview:nutrientView];
        
        y += nutrientView.frame.size.height + 5;
    }
//
//    
//    //Ingredients
//    UILabel *ingredientsTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, y, self.scroll.frame.size.width, 40)];
//    ingredientsTitle.text = @"INGREDIENTS";
//    ingredientsTitle.font = [UIFont boldSystemFontOfSize:20];
//    ingredientsTitle.backgroundColor = [UIColor grayColor];
//    ingredientsTitle.textColor = [UIColor whiteColor];
//    [self.scroll addSubview:ingredientsTitle];
//    y += ingredientsTitle.frame.size.height + ELEMENT_SPACE;
//    
//    UILabel *ingredients = [[UILabel alloc] initWithFrame:CGRectMake(x, y, self.scroll.frame.size.width - 2 * x, 0)];
//    ingredients.text = dict[@"data"][@"ingredients"];
//    ingredients.font = [UIFont systemFontOfSize:17];
//    ingredients.textColor = [UIColor grayColor];
//    ingredients.numberOfLines = 0;
//    [ingredients sizeToFit];
//    [self.scroll addSubview:ingredients];
//    y += ingredients.frame.size.height + ELEMENT_SPACE;
    
    
    UILabel *doesContain = [[UILabel alloc] initWithFrame:CGRectMake(0, y, self.scroll.frame.size.width, 40)];
    doesContain.text = @"DOES CONTAIN";
    doesContain.font = [UIFont boldSystemFontOfSize:20];
    doesContain.backgroundColor = [UIColor grayColor];
    doesContain.textColor = [UIColor whiteColor];
    [self.scroll addSubview:doesContain];
    y += doesContain.frame.size.height + ELEMENT_SPACE;
    
    
    NSArray *doesContainArray = dict[@"data"][@"DoesContain"];
    for (NSDictionary *d in doesContainArray) {
        NSArray *keys = [d allKeys];
        for (NSString *key in keys) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, y, self.view.frame.size.width - 20, 0)];
            label.text = [NSString stringWithFormat:@"%@ : %@",d[key],key];
            label.font = [UIFont systemFontOfSize:15];
            label.textColor = [UIColor grayColor];
            label.numberOfLines = 0;
            label.lineBreakMode = NSLineBreakByWordWrapping;
            [label sizeToFit];
            [self.scroll addSubview:label];
            y += label.frame.size.height + 5;
        }
    }
    
    
    UILabel *mayContain = [[UILabel alloc] initWithFrame:CGRectMake(0, y, self.scroll.frame.size.width, 40)];
    mayContain.text = @"MAY CONTAIN";
    mayContain.font = [UIFont boldSystemFontOfSize:20];
    mayContain.backgroundColor = [UIColor grayColor];
    mayContain.textColor = [UIColor whiteColor];
    [self.scroll addSubview:mayContain];
    y += mayContain.frame.size.height + ELEMENT_SPACE;
    
    
    NSArray *mayContainArray = dict[@"data"][@"MayContain"];
    for (NSDictionary *d in mayContainArray) {
        NSArray *keys = [d allKeys];
        for (NSString *key in keys) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, y, self.view.frame.size.width - 20, 0)];
            label.text = [NSString stringWithFormat:@"%@ : %@",d[key],key];
            label.font = [UIFont systemFontOfSize:15];
            label.textColor = [UIColor grayColor];
            label.numberOfLines = 0;
            label.lineBreakMode = NSLineBreakByWordWrapping;
            [label sizeToFit];
            [self.scroll addSubview:label];
            y += label.frame.size.height + 5;
        }
    }
    
    
    //Serving Size
    
    if ([[dict objectForKey:@"data"] objectForKey:@"Serving_Size"] != [NSNull null] && [[dict objectForKey:@"data"] objectForKey:@"Serving_Size"]) {
        UILabel *servingSizeTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, y, self.scroll.frame.size.width, 40)];
        servingSizeTitle.text = @"Serving Size";
        servingSizeTitle.font = [UIFont boldSystemFontOfSize:20];
        servingSizeTitle.backgroundColor = [UIColor grayColor];
        servingSizeTitle.textColor = [UIColor whiteColor];
        [self.scroll addSubview:servingSizeTitle];
        y += servingSizeTitle.frame.size.height + ELEMENT_SPACE;
        
        UILabel *servingSize = [[UILabel alloc] initWithFrame:CGRectMake(x, y, self.scroll.frame.size.width - 2 * x, 0)];
        servingSize.text = [NSString stringWithFormat:@"%@ %@",dict[@"data"][@"Serving_Size"][@"serving_size"],dict[@"data"][@"Serving_Size"][@"serving_size_uom"]];
        servingSize.font = [UIFont systemFontOfSize:17];
        servingSize.textColor = [UIColor grayColor];
        servingSize.numberOfLines = 0;
        [servingSize sizeToFit];
        [self.scroll addSubview:servingSize];
        y += servingSize.frame.size.height + ELEMENT_SPACE;
    }
    
    UIButton *addFood = [[UIButton alloc] initWithFrame:CGRectMake( 15, y, self.view.frame.size.width - 30, 40)];
    [addFood setTitle:@"Add Food" forState:UIControlStateNormal];
    [addFood setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addFood.backgroundColor = [EFGUtilities themeOrangeCreateNew];
    addFood.layer.cornerRadius = 6.0;
    [self.scroll addSubview:addFood];
    y += addFood.frame.size.height + ELEMENT_SPACE;
    
    self.scroll.contentSize = CGSizeMake(self.scroll.frame.size.width, y);
}
-(void)backNavigationTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
