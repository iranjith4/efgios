//
//  EFGSelectionController.m
//  efoodguru
//
//  Created by Ranjithkumar on 25/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGSelectionController.h"
#import "EFGUtilities.h"

@interface EFGSelectionController (){
    float xPos;
    float yPos;
    int index;
    NSArray *namesArray;
    UIButton *closeButton;
    UIImageView *titleImage;
    UILabel *titleLabel;
    UITableView *table;
}

@end

@implementation EFGSelectionController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initSetupWithNamesArray: (NSArray *)names title: (NSString *)title titleImage:(UIImage *)titleImg index:(int)ind{
    self.view.backgroundColor = [UIColor whiteColor];
    xPos = 0;
    yPos = self.view.frame.size.height * 0.10;
    namesArray = names;
    index = ind;
    
    //Close Button
    
    closeButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 30, self.view.frame.size.width -15, 40)];
    [closeButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [closeButton setTitleColor:[EFGUtilities themeButtonBlueColor] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeController) forControlEvents:UIControlEventTouchUpInside];
    [closeButton sizeToFit];
    [self.view addSubview:closeButton];
    
    
    //Title Image View
    titleImage = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width * 0.15, self.view.frame.size.width * 0.15)];
    titleImage.backgroundColor = [UIColor grayColor];
    titleImage.image = titleImg;
    titleImage.center = CGPointMake(self.view.center.x, titleImage.center.y);
    [self.view addSubview:titleImage];
    
    yPos += titleImage.frame.size.height + 10;
    
    //Title Label
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 40)];
    titleLabel.text = title;
    titleLabel.font = [UIFont boldSystemFontOfSize:22];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor blackColor];
    [self.view addSubview:titleLabel];
    
    yPos += titleLabel.frame.size.height + 2;
    
    //Table View
    table = [[UITableView alloc] initWithFrame:CGRectMake(10, yPos, self.view.frame.size.width - 20, self.view.frame.size.height - yPos + 40)];
    table.delegate = self;
    table.dataSource = self;
    [table sizeToFit];
    table.scrollEnabled = NO;
    [self.view addSubview:table];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return namesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    cell.textLabel.text = [namesArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self closeControllerWithIndex:index subIndex:(int)indexPath.row];
}

- (void)closeControllerWithIndex:(int)indx subIndex:(int)subIndex{
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate selectedIndex:indx andSubIndex:subIndex];
    }];
}

- (void)closeController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
