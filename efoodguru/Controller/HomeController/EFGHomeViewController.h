//
//  EFGHomeViewController.h
//  efoodguru
//
//  Created by Ranjithkumar on 16/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import "VCFloatingActionButton.h"
#import "EFGSelectionController.h"
#import "EFGHomeSearchController.h"

@interface EFGHomeViewController : UIViewController<UIScrollViewDelegate,floatMenuDelegate, SelectionControllerProtocol, UISearchBarDelegate,EFGSearchControlProtocol>

@end
