//
//  EFGProfileSetupModel.h
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface EFGProfileSetupModel : JSONModel

@property (nonatomic, strong) NSArray *avoidance_list;
@property (nonatomic, strong) NSDictionary *nutrition_names;
@property (nonatomic, strong) NSNumber *organic_food_preference;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSNumber *default_serving_size;

@end
