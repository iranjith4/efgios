//
//  EFGProfileSetupModel.m
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGProfileSetupModel.h"

@implementation EFGProfileSetupModel

-(instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err{
    if (self) {
        self.password = [dict objectForKey:@"password"];
        self.username = [dict objectForKey:@"username"];
        self.organic_food_preference = [NSNumber numberWithInt:[[dict objectForKey:@"organic_food_preference"] intValue]];
        self.nutrition_names = [dict objectForKey:@"nutrition_names"];
        self.avoidance_list = [dict objectForKey:@"avoidance_list"];
        self.default_serving_size = [dict objectForKey:@"default_serving_size"];
    }
    return self;
}


@end
