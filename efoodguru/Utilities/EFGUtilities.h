//
//  Utilities.h
//  efoodguru
//
//  Created by Ranjithkumar on 06/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//Macros

#define STORYBOARD [UIStoryboard storyboardWithName:@"Main" bundle:nil]

#define VC_VIEWCONTROLLER @"EFGViewController"
#define VC_STARTINGVC @"EFGStartingViewController"
#define VC_RETRIVEPASSWORD @"EFGRetrievePasswordVC"
#define VC_SIGNINCONTROLLER @"EFGSignInViewController"
#define VC_SIGNUPCONTROLLER @"EFGSignUpViewController"

#define DEVICE_HEIGHT_RATIO [[UIScreen mainScreen] bounds].size.height / 480
#define DEVICE_WIDTH_RATIO [[UIScreen mainScreen] bounds].size.width / 320
#define NAVIGATIONBAR_HEIGHT self.navigationController.navigationBar.frame.size.height
#define STATUSBAR_HEIGHT [UIApplication sharedApplication].statusBarFrame.size.height
#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

//User Defaults Keys
#define EFG_UD_USER_USERNAME @"EFG_UD_USER_USERNAME"
#define EFG_UD_USER_PASSWORD @"EFG_UD_USER_PASSWORD"
#define EFG_UD_USER_EMAIL @"EFG_UD_USER_EMAIL"
#define EFG_UD_USER_ID @"EFG_UD_USER_ID"
#define EFG_UD_USER_COUNTRY @"EFG_UD_USER_COUNTRY"
#define EFG_UD_USER_NAME @"EFG_UD_USER_NAME"
#define EFG_UD_USER_ZIPCODE @"EFG_UD_USER_ZIPCODE"
#define EFG_UD_USER_PROFILE_IMAGE @"EFG_UD_USER_PROFILE_IMAGE"
#define EFG_UD_USER_FBLOGIN_STATUS @"EFG_UD_USER_FBLOGIN_STATUS"
#define EFG_UD_USER_PASSWORD_RESET_CODE @"EFG_UD_USER_PASSWORD_RESET_CODE"
#define EFG_UD_USER_ISLOGGED @"EFG_UD_USER_ISLOGGED"
#define EFG_UD_USER_GOALS_ENTERED @"EFG_UD_USER_GOALS_ENTERED"

#define EFG_UD_ISLOGGED_LOCAL @"EFG_UD_ISLOGGED_LOCAL"

//Network Data
static float const SERVER_TIMEOUT_INTERVAL = 20.0f;

//URL List

//#define EFG_URL_BASE @"http://119.9.76.86/efood-guru-api-final/public"
#define EFG_URL_BASE @"http://104.130.27.217/efood-guru-api-final/public"
#define EFG_URL_USER_GET_AVOIDANCE @"/getUserAvoidanceAnswers"
#define EFG_URL_USER_GET_PRODUCT_REPORT @"/getProductReport"
#define EFG_URL_USER_LOGIN @"/userLogin"
#define EFG_URL_USER_REGISTER @"/userRegister"
#define EFG_URL_USER_EDIT_PROFILE @"/userEditProfile"
#define EFG_URL_USER_DETAILS @"/getUserDetails"
#define EFG_URL_GET_SEARCH_PRODUCT @"/getSearchProduct"
#define EFG_URL_GET_ADMIN_EXERCSE @"/getAdminExercise"
#define EFG_URL_USER_ADD_CUSTOM_EXERCISE @"/userAddCustomExercise"
#define EFG_URL_USER_SAVE_AVOIDANCE @"/saveUserAvoidanceAnswers"
#define EFG_URL_USER_FORGOT_PASSWORD @"/forgotPassword"
#define EFG_URL_GET_RECENT_FOOD @"/getRecentFoods"
#define EFG_URL_SAVE_CUSTOM_FOOD @"/saveUserCustomFood"
#define EFG_URL_QUICK_ADD_CALORIES @"/quickAddCalories"
#define EFG_URL_WATER_INTAKE @"/saveWaterIntake"
#define EFG_URL_WEIGHT @"/updateUserGoals"
#define EFG_URL_USER_GOALS_SAVE @"/saveUserGoals"
#define EFG_URL_USER_GOALS_UPDATE @"/updateUserGoals"
#define EFG_URL_USER_GOALS_GET @"/getUserGoals"
#define EFG_URL_GOALS_SAVE_CALORIEMACRO @"/saveCalorieMacroNutrientGoals"
#define EFG_URL_GOALS_GET_CALORIEMACRO @"/getCalorieMacroNutrientGoals"
#define EFG_URL_GOALS_SAVE_ADDITIONALGOALS @"/saveAdditionalNutrientGoals"
#define EFG_URL_GOALS_GET_ADDITIONALGOALS @"/getAdditionalNutrientGoals"
#define EFG_URL_CALORIES_REMAINING @"/getCaloriesRemaining"
//Nutrition APIs
#define EFG_URL_NUTRITION_CALORIE_DAY @"/getCaloriesNutritionDayView"
#define EFG_URL_NUTRITION_CALORIE_WEEK @"/getCaloriesNutritionWeekView"
#define EFG_URL_NUTRITION_NUTRIRION_DAY @"/getNutrientsDayView"
#define EFG_URL_NUTRITION_NUTRITION_WEEK @"/getNutrientsWeekView"
#define EFG_URL_NUTRITION_MACRO_DAY @"/getMacrosDayView"
#define EFG_URL_NUTRITION_MACRO_WEEK @"/getMacrosWeekView"

//Progress API
#define EFG_URL_PROGRESS_MONTH_VIEW @"/getProgressMonthView"

//Diary API
#define EFG_URL_DIARY_DETAILS @"/getDiaryDetails"



@interface EFGUtilities : NSObject

// Theme Colors
+ (UIColor *) themeColor;
+ (UIColor *) themeLabelBlackShade;
+ (UIColor *) themeButtonRedColor;
+ (UIColor *) themeButtonBlueColor;
+ (UIColor *)themeBackgroundGray;
+ (UIColor*) themeColorSegmentRed;
+ (UIColor*) themeOrangeCreateNew;

//UserDefaultMethods
+ (NSString *)getUserPassword;
+ (NSString *)getUserUsername;
+ (NSString *)getUserEmail;
+ (NSString *)getUserName;
+ (NSString *)getUserId;
+ (NSString *)getUserCountry;
+ (NSString *)getUserZipCode;
+ (NSString *)getUserProfileImageUrl;
+ (NSString *)getUserPasswordResetCode;
+ (BOOL) getUserLoginStatus;
+ (BOOL) getUserFBLoginStatus;
+ (BOOL) isGoalsEntered;

+ (void) setUserUsername: (NSString *)username;
+ (void) setUserPassword: (NSString *)password;
+ (void) setUserEmail: (NSString *)email;
+ (void) setUserId: (NSString *) userId;
+ (void) setUserName: (NSString *)name;
+ (void) setUserCountry : (NSString *)country;
+ (void) setUserZipCode : (NSString *)zip;
+ (void) setuserProfileImageUrl : (NSString *)imageUrl;
+ (void) setUserPasswordResetCode: (NSString *)code;
+ (void) setUserLoginStatus : (BOOL)status;
+ (void) setUserFBLoginStatus: (BOOL)status;
+ (BOOL) isLoggedIn;
+ (void) setLoggedIn:(BOOL)status;
+ (void)setIsGoalsEntered:(BOOL)status;


//Font Methods
+ (UIFont *)fontWithRegular:(CGFloat)size;
+ (UIFont *)fontWithBold:(CGFloat)size;
+ (UIFont *)fontWithRegular;
+ (UIFont *)fontWithBold;

//Validations
+ (BOOL)validateEmail:(NSString *)candidate;

//UIAlert
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message actionTitle:(NSString *)action inViewController:(UIViewController *)viewController;
+ (void)showAlertWithTitleVC:(NSString *)title message:(NSString *)message actionTitle:(NSString *)action inViewController:(UIViewController *)viewController;
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message actionTitle:(NSString *)action inViewController:(UIViewController *)viewController type:(int)type;
+ (void)serverError;
+ (void)showProgress;
+ (void)hideProgress;

//API Helpers
+(NSString*)addQueryStringToUrlString:(NSString *)urlString withDictionary:(NSDictionary *)dictionary;
+ (void)saveUserDetails:(NSDictionary *)dict;
+ (NSString *)URLEncodeStringFromString:(NSString *)string;

//Date Time formatters.
+ (NSString *)getCurrentFormattedDate;
+ (NSDate *)getDateWithMonth:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second;
+ (NSString *)getCurrentFormattedDate:(NSDate *)date inFormat: (NSString *)format;
+ (NSString *)getLocalMonth : (NSDate *)date;
+ (NSString *)getLocalMonthName : (NSDate *)date;
+ (NSString *)getLocalYear : (NSDate *)date;

//Temp Methods
//TODO: Delete in Production Build
+(void)setTestAccount;

@end
