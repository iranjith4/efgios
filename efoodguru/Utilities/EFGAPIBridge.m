//
//  EFGAPIBridge.m
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGAPIBridge.h"
#import "EFGUtilities.h"
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "TSMessage.h"

@implementation EFGAPIBridge

+ (BOOL)AFGetRequestWithURL:(NSString *)urlString
                    success:(void(^)(NSDictionary *, NSURLResponse *))successBlock_
                    failure:(void(^)(NSHTTPURLResponse *, NSError *))failureBlock_ {
    if ([self isNetworkdDown]) {
        return FALSE;
    }
    NSURL* url = [NSURL URLWithString:urlString];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:SERVER_TIMEOUT_INTERVAL];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setHTTPMethod:@"GET"];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    operation.responseSerializer.acceptableContentTypes = [operation.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock_(responseObject, operation.response);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [EFGAPIBridge handleResponseCode:operation.response.statusCode];
        failureBlock_((NSHTTPURLResponse *)operation.response, error);
        [SVProgressHUD dismiss];
        
    }];
    [operation start];
    return TRUE;
}

+ (BOOL)AFPostRequest:(NSString *)jsonObject withURL:(NSString *)urlString
              success:(void(^)(NSData *, NSURLResponse *))successBlock_
              failure:(void(^)(NSHTTPURLResponse *, NSError *))failureBlock_ {
    
    if ([self isNetworkdDown]) {
        [SVProgressHUD dismiss];
        return FALSE;
    }
    
    NSURL *url=[NSURL URLWithString:urlString];
    NSData *postData = [jsonObject dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    [request setTimeoutInterval: SERVER_TIMEOUT_INTERVAL];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setHTTPBody:postData];
    
    AFHTTPRequestOperation* operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock_(operation.responseData, operation.response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [EFGAPIBridge handleResponseCode:operation.response.statusCode];
        failureBlock_((NSHTTPURLResponse *)operation.response, error);
    }];
    [operation start];
    return TRUE;
}

+ (BOOL)AFPutRequest:(NSString *)jsonObject withURL:(NSString *)urlString
              success:(void(^)(NSData *, NSURLResponse *))successBlock_
              failure:(void(^)(NSHTTPURLResponse *, NSError *))failureBlock_ {
    
    if ([self isNetworkdDown]) {
        [SVProgressHUD dismiss];
        return FALSE;
    }
    
    NSURL *url=[NSURL URLWithString:urlString];
    NSData *postData = [jsonObject dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    [request setTimeoutInterval: SERVER_TIMEOUT_INTERVAL];
    [request setURL:url];
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setHTTPBody:postData];
    
    AFHTTPRequestOperation* operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock_(operation.responseData, operation.response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [EFGAPIBridge handleResponseCode:operation.response.statusCode];
        failureBlock_((NSHTTPURLResponse *)operation.response, error);
    }];
    [operation start];
    return TRUE;
}

+ (BOOL)AFPostRequestWithURLEncoded:(NSString *)jsonObject withURL:(NSString *)urlString
              success:(void(^)(NSData *, NSURLResponse *))successBlock_
              failure:(void(^)(NSHTTPURLResponse *, NSError *))failureBlock_ {
    
    if ([self isNetworkdDown]) {
        [SVProgressHUD dismiss];
        return FALSE;
    }
    
    NSURL *url=[NSURL URLWithString:urlString];
    NSData *postData = [jsonObject dataUsingEncoding:NSUTF8StringEncoding];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    [request setTimeoutInterval: SERVER_TIMEOUT_INTERVAL];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setHTTPBody:postData];
    
    AFHTTPRequestOperation* operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock_(operation.responseData, operation.response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [EFGAPIBridge handleResponseCode:operation.response.statusCode];
        failureBlock_((NSHTTPURLResponse *)operation.response, error);
    }];
    [operation start];
    return TRUE;}

+ (BOOL) handleResponseCode:(NSInteger)status{
    if (status == 401) {
        } else if (status >= 500){
        return FALSE;
    }
    return TRUE;
}


+ (BOOL)isNetworkdDown {
    Reachability *networkReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReach currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [TSMessage showNotificationWithTitle:@"No Network"
                                    subtitle:@"Please connect to the internet and try again."
                                        type:TSMessageNotificationTypeError];
        [SVProgressHUD dismiss];
        return true;
    }
    return false;
}

@end
