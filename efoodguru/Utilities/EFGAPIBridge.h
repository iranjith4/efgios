//
//  EFGAPIBridge.h
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface EFGAPIBridge : NSObject

+ (BOOL)AFPostRequest:(NSString *)jsonObject withURL:(NSString *)urlString
              success:(void(^)(NSData *, NSURLResponse *))successBlock_
              failure:(void(^)(NSHTTPURLResponse *, NSError *))failureBlock_;

+ (BOOL)AFGetRequestWithURL:(NSString *)urlString
                    success:(void(^)(NSDictionary *, NSURLResponse *))successBlock_
                    failure:(void(^)(NSHTTPURLResponse *, NSError *))failureBlock_;

+ (BOOL)AFPostRequestWithURLEncoded:(NSString *)jsonObject withURL:(NSString *)urlString
                            success:(void(^)(NSData *, NSURLResponse *))successBlock_
                            failure:(void(^)(NSHTTPURLResponse *, NSError *))failureBlock_;

+ (BOOL)AFPutRequest:(NSString *)jsonObject withURL:(NSString *)urlString
             success:(void(^)(NSData *, NSURLResponse *))successBlock_
             failure:(void(^)(NSHTTPURLResponse *, NSError *))failureBlock_;

@end
