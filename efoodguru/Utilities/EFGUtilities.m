//
//  Utilities.m
//  efoodguru
//
//  Created by Ranjithkumar on 06/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGUtilities.h"
#import "TSMessage.h"
#import "SVProgressHUD.h"

@implementation EFGUtilities

+ (UIColor*) themeColor{
    return [UIColor colorWithRed:(float)59/255 green:(float)153/255 blue:(float)216/255 alpha:1.0];
}

+ (UIColor*) themeButtonRedColor{
    return [UIColor colorWithRed:(float)229/255 green:(float)77/255 blue:(float)66/255 alpha:1.0];
}

+ (UIColor *)themeButtonBlueColor{
    return [UIColor colorWithRed:(float)67/255 green:(float)95/255 blue:(float)172/255 alpha:1.0];
}

+ (UIColor *)themeLabelBlackShade{
    return [UIColor blackColor];
}

+ (UIColor *)themeBackgroundGray{
    return [UIColor colorWithRed:(float)238/255 green:(float)238/255 blue:(float)238/255 alpha:1.0];
}

+ (UIColor*) themeColorSegmentRed{
    return [UIColor colorWithRed:(float)229/255 green:(float)77/255 blue:(float)66/255 alpha:1.0];
}

+ (UIColor*) themeOrangeCreateNew{
    return [UIColor colorWithRed:(float)243/255 green:(float)150/255 blue:(float)37/255 alpha:1.0];
}


+ (UIFont *)fontWithRegular:(CGFloat)size{
    return [UIFont systemFontOfSize:size];
}

+ (UIFont *)fontWithBold:(CGFloat)size{
    return [UIFont boldSystemFontOfSize:size];
}

+ (UIFont *)fontWithRegular{
    return [UIFont systemFontOfSize:[UIFont systemFontSize]];
}

+ (UIFont *)fontWithBold{
    return [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
}


#pragma mark - User Default Methods

+ (NSString *)getUserUsername{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_USERNAME] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_USERNAME];
    }else{
        return nil;
    }
}

+ (NSString *)getUserPassword{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_PASSWORD] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_PASSWORD];
    }else{
        return nil;
    }
}

+ (NSString *)getUserEmail{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_EMAIL] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_EMAIL];
    }else{
        return nil;
    }
}

+ (NSString *)getUserId{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_ID] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_ID];
    }else{
        return nil;
    }
}

+ (NSString *)getUserName{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_NAME] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_NAME];
    }else{
        return nil;
    }
}

+ (NSString *)getUserCountry{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_COUNTRY] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_COUNTRY];
    }else{
        return nil;
    }
}

+ (NSString *)getUserZipCode{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_ZIPCODE] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_ZIPCODE];
    }else{
        return nil;
    }
}

+ (NSString *)getUserProfileImageUrl{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_PROFILE_IMAGE] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_PROFILE_IMAGE];
    }else{
        return nil;
    }
}

+ (NSString *)getUserPasswordResetCode{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_PASSWORD_RESET_CODE] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_PASSWORD_RESET_CODE];
    }else{
        return nil;
    }
}

+ (BOOL)getUserLoginStatus{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_ISLOGGED] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_ISLOGGED];
    }else{
        return nil;
    }
}

+ (BOOL)getUserFBLoginStatus{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_FBLOGIN_STATUS] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_FBLOGIN_STATUS];
    }else{
        return nil;
    }
}

+(BOOL)isGoalsEntered{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_USER_GOALS_ENTERED] != [NSNull null]) {
        return [user objectForKey:EFG_UD_USER_GOALS_ENTERED];
    }else{
        [user setObject:[NSNumber numberWithBool:NO] forKey:EFG_UD_USER_GOALS_ENTERED];
        return NO;
    }
}


+ (void) setUserUsername:(NSString *)username{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:username forKey:EFG_UD_USER_USERNAME];
}

+ (void)setUserPassword:(NSString *)password{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:password forKey:EFG_UD_USER_PASSWORD];
}

+ (void)setUserEmail:(NSString *)email{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:email forKey:EFG_UD_USER_EMAIL];
}

+ (void)setUserId:(NSString *)userId{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:userId forKey:EFG_UD_USER_ID];
}

+ (void)setUserName:(NSString *)name{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:name forKey:EFG_UD_USER_NAME];
}

+ (void)setUserCountry:(NSString *)country{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:country forKey:EFG_UD_USER_COUNTRY];
}

+ (void)setUserZipCode:(NSString *)zip{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:zip forKey:EFG_UD_USER_ZIPCODE];
}

+ (void)setuserProfileImageUrl:(NSString *)imageUrl{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:imageUrl forKey:EFG_UD_USER_PROFILE_IMAGE];
}

+ (void)setUserPasswordResetCode:(NSString *)code{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:code forKey:EFG_UD_USER_PASSWORD_RESET_CODE];
}

+ (void)setUserLoginStatus:(BOOL)status{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:[NSNumber numberWithBool:status] forKey:EFG_UD_USER_ISLOGGED];
}

+ (void)setUserFBLoginStatus:(BOOL)status{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:[NSNumber numberWithBool:status] forKey:EFG_UD_USER_FBLOGIN_STATUS];
}

+ (void)setIsGoalsEntered:(BOOL)status{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:[NSNumber numberWithBool:status] forKey:EFG_UD_USER_GOALS_ENTERED];
}




//Local Login Status

+ (BOOL)isLoggedIn{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:EFG_UD_ISLOGGED_LOCAL] == [NSNull null]) {
        [user setObject:[NSNumber numberWithBool:NO] forKey:EFG_UD_ISLOGGED_LOCAL];
    }
    return [[user objectForKey:EFG_UD_ISLOGGED_LOCAL] boolValue];
    //return YES; //TODO: Remove this once the login is fixed.
}

+ (void)setLoggedIn:(BOOL)status{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:[NSNumber numberWithBool:status] forKey:EFG_UD_ISLOGGED_LOCAL];
}


#pragma mark - validations

+ (BOOL)validateEmail:(NSString *)candidate{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

#pragma mark - API Helpers
+(void)saveUserDetails:(NSDictionary *)dict{
    if (dict[@"data"][@"id"] != [NSNull null]) {
        [EFGUtilities setUserId:dict[@"data"][@"id"]];
    }
    if (dict[@"data"][@"name"] != [NSNull null]) {
        [EFGUtilities setUserName:dict[@"data"][@"name"]];
    }
    if (dict[@"data"][@"email"] != [NSNull null]) {
        [EFGUtilities setUserEmail:dict[@"data"][@"email"]];
    }
    if (dict[@"data"][@"username"] != [NSNull null]) {
        [EFGUtilities setUserUsername:dict[@"data"][@"username"]];
    }
    if (dict[@"data"][@"password"] != [NSNull null]) {
        [EFGUtilities setUserPassword:dict[@"data"][@"password"]];
    }
    if (dict[@"data"][@"country"] != [NSNull null]) {
        [EFGUtilities setUserCountry:dict[@"data"][@"country"]];
    }
    if (dict[@"data"][@"zipcode"] != [NSNull null]) {
        [EFGUtilities setUserZipCode:dict[@"data"][@"zipcode"]];
    }
    if (dict[@"data"][@"image_url"] != [NSNull null]) {
        [EFGUtilities setuserProfileImageUrl:dict[@"data"][@"image_url"]];
    }
    if (dict[@"data"][@"fb_login"] != [NSNull null]) {
        [EFGUtilities setUserFBLoginStatus:[dict[@"data"][@"fb_login"] boolValue]];
    }
    if (dict[@"data"][@"password_reset_code"] != [NSNull null]) {
        [EFGUtilities setUserPasswordResetCode:dict[@"data"][@"password_reset_code"]];
    }
    if (dict[@"data"][@"logged_in"] != [NSNull null]) {
        [EFGUtilities setUserLoginStatus:[dict[@"data"][@"logged_in"] boolValue]];
    }

}



//TODO: Delete these test Methods in Production
+ (void)setTestAccount{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:@"test@gmail.com" forKey:EFG_UD_USER_USERNAME];
    [user setObject:@"test" forKey:EFG_UD_USER_PASSWORD];
}

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message actionTitle:(NSString *)action inViewController:(UIViewController *)viewController{
    [TSMessage showNotificationWithTitle:title subtitle:message type:TSMessageNotificationTypeWarning];
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
//    [alert addAction:[UIAlertAction actionWithTitle:action style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//    }]];
//    [viewController presentViewController:alert animated:YES completion:nil];

}


+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message actionTitle:(NSString *)action inViewController:(UIViewController *)viewController type:(int)type{
    if (type == 1) {
        [TSMessage showNotificationWithTitle:title subtitle:message type:TSMessageNotificationTypeSuccess];
    }else if (type == 2){
        [TSMessage showNotificationWithTitle:title subtitle:message type:TSMessageNotificationTypeWarning];
    }else if (type == 3){
        [TSMessage showNotificationWithTitle:title subtitle:message type:TSMessageNotificationTypeMessage];
    }else if (type == 0){
        [TSMessage showNotificationWithTitle:title subtitle:message type:TSMessageNotificationTypeError];
    }

    //    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    //    [alert addAction:[UIAlertAction actionWithTitle:action style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    //    }]];
    //    [viewController presentViewController:alert animated:YES completion:nil];
    
}

+ (void)showAlertWithTitleVC:(NSString *)title message:(NSString *)message actionTitle:(NSString *)action inViewController:(UIViewController *)viewController{
    [TSMessage showNotificationInViewController:viewController title:title subtitle:message type:TSMessageNotificationTypeWarning duration:3 canBeDismissedByUser:YES];
    //    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    //    [alert addAction:[UIAlertAction actionWithTitle:action style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    //    }]];
    //    [viewController presentViewController:alert animated:YES completion:nil];
}

+ (NSString *)getCurrentFormattedDate{
    NSDate *date = [NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSLog(@"%@",[dateFormatter stringFromDate:date]);
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)getCurrentFormattedDate:(NSDate *)date inFormat: (NSString *)format{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    NSLog(@"%@",[dateFormatter stringFromDate:date]);
    return [dateFormatter stringFromDate:date];
}

+ (void)serverError{
    [TSMessage showNotificationWithTitle:@"Server Error" subtitle:@"Something Went Wrong. Try again. If problem persists, contact help." type:TSMessageNotificationTypeError];
}


+ (void)showProgress{
    [SVProgressHUD show];
}

+ (void)hideProgress{
    [SVProgressHUD dismiss];
}

//Date Formatters

+ (NSDate *)getDateWithMonth:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second{
    
    NSDate *currentDate = [NSDate date];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:month];
    [dateComponents setDay:day];
    [dateComponents setHour:hour];
    [dateComponents setMinute:minute];
    [dateComponents setSecond:second];
    
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
}


+ (NSString *)getLocalMonth : (NSDate *)date{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:timeZone];
    [df setDateFormat:@"MM"];
    return [df stringFromDate:date];
}

+ (NSString *)getLocalMonthName : (NSDate *)date{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:timeZone];
    [df setDateFormat:@"MMM"];
    return [df stringFromDate:date];
}

+ (NSString *)getLocalYear : (NSDate *)date{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:timeZone];
    [df setDateFormat:@"yyyy"];
    return [df stringFromDate:date];
}



@end
