//
//  NSString+EFGString.h
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EFGString)

- (NSString *) URLEncodedString;

@end
