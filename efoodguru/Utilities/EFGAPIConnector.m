//
//  EFGAPIConnector.m
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGAPIConnector.h"
#import "EFGAPIBridge.h"
#import "EFGUtilities.h"
#import "NSDictionary+EFGQueryString.h"
#import "NSString+EFGString.h"
#import "JSONHTTPClient.h"

@implementation EFGAPIConnector

static EFGAPIConnector *sharedDelegate = nil;

+ (id)sharedManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (sharedDelegate == nil) {
            sharedDelegate = [[self alloc] init];
        }
    });
    return sharedDelegate;
}

- (id) init {
    if (self = [super init]) {
    }
    return self;
}

//Food Avoidance API
- (void)getAvoidance:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
               failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_GET_AVOIDANCE];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)saveAvoidance:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
              failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_SAVE_AVOIDANCE];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    
    NSString *userAvoidanceList = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:params[@"avoidance_list"] options:0 error:nil] encoding:NSUTF8StringEncoding];
    
    NSString *nutritienNames = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:params[@"nutrition_names"] options:0 error:nil] encoding:NSUTF8StringEncoding];

    userAvoidanceList = [userAvoidanceList URLEncodedString];
    nutritienNames = [nutritienNames URLEncodedString];
    
    NSString *myString = [NSString stringWithFormat:@"username=%@&password=%@&avoidance_list=%@&organic_food_preference=%d&nutrition_names=%@&default_serving_size=%d",[EFGUtilities getUserUsername],[EFGUtilities getUserPassword],userAvoidanceList,[params[@"organic_food_preference"] intValue],nutritienNames,[params[@"default_serving_size"] intValue]];
    [EFGAPIBridge AFPostRequestWithURLEncoded:myString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
  }

- (void)productSearchResult:(NSString *)queryString success:(void (^)(NSDictionary *, NSURLResponse *))successBlock_
             failure:(void (^)(NSHTTPURLResponse *, NSError *))failureBlock_ {
    NSString *url = [NSString stringWithFormat:@"%@%@?query=%@", EFG_URL_BASE, EFG_URL_GET_SEARCH_PRODUCT,queryString];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url =  [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFGetRequestWithURL:url success:^(NSDictionary *dict, NSURLResponse *resp) {
        successBlock_(dict, resp);
    } failure:failureBlock_];
}

- (void)productDataForUPCWithParam:(NSDictionary *)params success:(void (^)(NSDictionary *, NSURLResponse *))successBlock_
                           failure:(void (^)(NSHTTPURLResponse *, NSError *))failureBlock_ {
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_GET_PRODUCT_REPORT];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);

    } failure:failureBlock_];
}

//Log In API

- (void)loginWithParams:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_LOGIN];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)registerWithParams:(NSDictionary *)params andImage:(UIImage *)img success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSData *imgData = UIImageJPEGRepresentation(img, 1);
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_REGISTER];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
    
}

- (void)registerWithParams:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                   failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_REGISTER];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
        [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
            NSError* error;
            NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                         error:&error];
            successBlock_(resultData,resp);
        } failure:failureBlock_];
    
    }

- (void)editProfileWith:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                   failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_EDIT_PROFILE];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
    
}


- (void)forgotPasswordWithParam:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_FORGOT_PASSWORD];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getUserDetails:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                   failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_DETAILS];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getAdminExercise:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
               failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSString *myString = [NSString stringWithFormat:@"username=%@&password=%@&category=%@",[EFGUtilities getUserUsername],[EFGUtilities getUserPassword],params[@"category"]];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_GET_ADMIN_EXERCSE];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    postString = [postString stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequestWithURLEncoded:myString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)saveUserCustomExercise:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
               failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_ADD_CUSTOM_EXERCISE];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getRecentFood:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                       failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_GET_RECENT_FOOD];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)saveUserFood:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
              failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_SAVE_CUSTOM_FOOD];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)quickAddCalories:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
             failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_QUICK_ADD_CALORIES];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}


- (void)saveWaterIntake:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                 failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_WATER_INTAKE];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)saveWeight:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_WEIGHT];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPutRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

// Goal data

- (void)saveGoalData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
           failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_GOALS_SAVE];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getGoalData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
             failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_USER_GOALS_GET];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)saveGoalCalorieMacroData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
             failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_GOALS_SAVE_CALORIEMACRO];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)saveGoalAdditionalData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
             failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_GOALS_SAVE_ADDITIONALGOALS];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getGoalCalorieMacroData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                         failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_GOALS_GET_CALORIEMACRO];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getGoalAdditionalData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                       failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_GOALS_GET_ADDITIONALGOALS];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

//Home Page Calories Remaining

- (void)getCaloriesRemaining:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                      failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_CALORIES_REMAINING];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

//Nutrition View

- (void)getNutritionCalorieWeek:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_NUTRITION_CALORIE_WEEK];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getNutritionCalorieDay:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_NUTRITION_CALORIE_DAY];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getNutritionNutritionWeek:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_NUTRITION_NUTRITION_WEEK];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getNutritionNutritionDay:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_NUTRITION_NUTRIRION_DAY];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getNutritionMacroWeek:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_NUTRITION_MACRO_WEEK];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getNutritionMacroDay:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_NUTRITION_MACRO_DAY];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getProgressMonthView:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_PROGRESS_MONTH_VIEW];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}

- (void)getDiaryDetails:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString * postString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@",EFG_URL_BASE, EFG_URL_DIARY_DETAILS];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    [EFGAPIBridge AFPostRequest:postString withURL:url success:^(NSData *data, NSURLResponse *resp) {
        NSError* error;
        NSDictionary* resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions
                                                                     error:&error];
        successBlock_(resultData,resp);
    } failure:failureBlock_];
}






@end
