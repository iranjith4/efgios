//
//  NSDictionary+EFGQueryString.h
//  efoodguru
//
//  Created by Ranjithkumar on 06/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (EFGQueryString)

-(NSString*) urlEncodedString;

@end
