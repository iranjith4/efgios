//
//  EFGAPIConnector.h
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface EFGAPIConnector : NSObject

+ (id)sharedManager;

- (void)getAvoidance:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
             failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)saveAvoidance:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
              failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)productDataForUPCWithParam:(NSDictionary *)params success:(void (^)(NSDictionary *, NSURLResponse *))successBlock_
                           failure:(void (^)(NSHTTPURLResponse *, NSError *))failureBlock_;

- (void)loginWithParams:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)registerWithParams:(NSDictionary *)params andImage:(UIImage *)img success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_ failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)registerWithParams:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                   failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getUserDetails:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                   failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)productSearchResult:(NSString *)queryString success:(void (^)(NSDictionary *, NSURLResponse *))successBlock_
                    failure:(void (^)(NSHTTPURLResponse *, NSError *))failureBlock_;

- (void)getAdminExercise:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                 failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)forgotPasswordWithParam:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_ failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)editProfileWith:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)saveUserCustomExercise:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                       failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getRecentFood:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
              failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)saveUserFood:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
             failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)quickAddCalories:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                 failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)saveWaterIntake:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)saveWeight:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)saveGoalAdditionalData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                       failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)saveGoalCalorieMacroData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                         failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)saveGoalData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
             failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getGoalData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
            failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getGoalCalorieMacroData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                        failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getGoalAdditionalData:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                      failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getCaloriesRemaining:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getNutritionCalorieWeek:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                        failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getNutritionCalorieDay:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                       failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getNutritionNutritionWeek:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                          failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getNutritionNutritionDay:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                         failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getNutritionMacroWeek:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                      failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getNutritionMacroDay:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getProgressMonthView:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                     failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

- (void)getDiaryDetails:(NSDictionary *)params success:(void (^)(NSDictionary *,NSURLResponse *))successBlock_
                failure:(void (^)(NSHTTPURLResponse *, NSError *)) failureBlock_;

@end
