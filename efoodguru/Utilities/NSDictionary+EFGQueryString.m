//
//  NSDictionary+EFGQueryString.m
//  efoodguru
//
//  Created by Ranjithkumar on 06/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "NSDictionary+EFGQueryString.h"

@implementation NSDictionary (EFGQueryString)

-(NSString*) urlEncodedString {
    NSMutableArray *parts = [NSMutableArray array];
    for (id key in self) {
        id value = [self objectForKey: key];
        NSString *part = [NSString stringWithFormat: @"%@=%@", urlEncode(key), urlEncode(value)];
        [parts addObject: part];
    }
    return [parts componentsJoinedByString: @"&"];
}

static NSString *urlEncode(id object) {
    NSString *string = toString(object);
    return [string stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
}

static NSString *toString(id object) {
    return [NSString stringWithFormat: @"%@", object];
}

@end
