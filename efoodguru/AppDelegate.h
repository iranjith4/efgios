//
//  AppDelegate.h
//  efoodguru
//
//  Created by Ranjithkumar on 05/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFGViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) EFGViewController *viewController;


@end

