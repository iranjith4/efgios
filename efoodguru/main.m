//
//  main.m
//  efoodguru
//
//  Created by Ranjithkumar on 05/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//
    
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
