//
//  EFGNutritionNutritionView.h
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGNutritionNutritionView : UIView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *table;

- (void)updateData:(NSArray *)array;

@end
