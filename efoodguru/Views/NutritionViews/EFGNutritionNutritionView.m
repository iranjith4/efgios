//
//  EFGNutritionNutritionView.m
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGNutritionNutritionView.h"
#import "EFGNutritionCell.h"

static NSString *identifier = @"nutritionCell";

@implementation EFGNutritionNutritionView{
    NSArray *nutrientArray;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
        [self initView];
    }
    return self;
}

- (void)initData{
    NSDictionary *val1 = @{
                           @"name" : @"Protein",
                           @"avg" : @"0.00",
                           @"goal" : @"900",
                           @"left" : @"0g"
                           };
    NSDictionary *val2 = @{
                           @"name" : @"Carbohydrates",
                           @"avg" : @"0.00",
                           @"goal" : @"900",
                           @"left" : @"0g"
                           };
    NSDictionary *val3 = @{
                           @"name" : @"Fiber",
                           @"avg" : @"0.00",
                           @"goal" : @"900",
                           @"left" : @"0g"
                           };
    NSDictionary *val4 = @{
                           @"name" : @"Sugars",
                           @"avg" : @"0.00",
                           @"goal" : @"900",
                           @"left" : @"0g"
                           };
    NSDictionary *val5 = @{
                           @"name" : @"Fat",
                           @"avg" : @"0.00",
                           @"goal" : @"900",
                           @"left" : @"0g"
                           };
    NSDictionary *val6 = @{
                           @"name" : @"Saturated",
                           @"avg" : @"0.00",
                           @"goal" : @"900",
                           @"left" : @"0g"
                           };
    NSDictionary *val7 = @{
                           @"name" : @"PolySaturated",
                           @"avg" : @"0.00",
                           @"goal" : @"900",
                           @"left" : @"0g"
                           };
    NSDictionary *val8 = @{
                           @"name" : @"Trans",
                           @"avg" : @"0.00",
                           @"goal" : @"900",
                           @"left" : @"0g"
                           };
    NSDictionary *val9 = @{
                           @"name" : @"Cholestrol",
                           @"avg" : @"0.00",
                           @"goal" : @"900",
                           @"left" : @"0g"
                           };
    NSDictionary *val10 = @{
                           @"name" : @"Sodium",
                           @"avg" : @"0.00",
                           @"goal" : @"900",
                           @"left" : @"0g"
                           };
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:val1,val2,val3,val4,val5,val6,val7,val8,val9,val10,nil];
    
    nutrientArray = array;
}

- (void)initView{
    self.backgroundColor = [UIColor whiteColor];
    
    self.table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) style:UITableViewStylePlain];
    self.table.delegate = self;
    self.table.dataSource = self;
    [self.table registerClass:[EFGNutritionCell class] forCellReuseIdentifier:identifier];
    [self addSubview:self.table];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return nutrientArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGNutritionCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[EFGNutritionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    NSDictionary *data = [nutrientArray objectAtIndex:indexPath.row];
    [cell setUpCellWithTitle:data[@"name"] average:data[@"avg"] goal:data[@"goal"] left:data[@"left"]];
    return cell;
}

- (void)updateData:(NSArray *)array{
    nutrientArray = array;
    [self.table reloadData];
}

@end
