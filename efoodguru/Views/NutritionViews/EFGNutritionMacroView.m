//
//  EFGNutritionMacroView.m
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGNutritionMacroView.h"
#import "EFGDataPresentLabel.h"
#import "EFGUtilities.h"
#import "PNChart.h"

@implementation EFGNutritionMacroView{
    float xPos;
    float yPos;
    PNPieChart *pieChart;
    PNBarChart *barChart;
    EFGDataPresentLabel *total;
    EFGDataPresentLabel *net;
    EFGDataPresentLabel *goal;
    UIView *legend;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView{
    xPos = 20;
    yPos = 15;
    self.backgroundColor = [UIColor whiteColor];
    
    NSArray *items = @[[PNPieChartDataItem dataItemWithValue:15 color:PNRed description:@"Carbohydrates"],
                       [PNPieChartDataItem dataItemWithValue:27 color:PNBlue description:@"Protein"],
                       [PNPieChartDataItem dataItemWithValue:33 color:PNGreen description:@"Fat"],
                       ];
    
    pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(xPos, yPos, self.frame.size.width - 2 * xPos, self.frame.size.width - 2 * xPos) items:items];
    pieChart.descriptionTextColor = [UIColor whiteColor];
    pieChart.descriptionTextFont  = [UIFont fontWithName:@"Avenir-Medium" size:14.0];
    pieChart.backgroundColor = [UIColor whiteColor];
    [pieChart strokeChart];
    
    pieChart.legendStyle = PNLegendItemStyleSerial;
    pieChart.legendFont = [UIFont fontWithName:@"Avenir-Medium" size:14.0];
    legend = [pieChart getLegendWithMaxWidth:200];
    
    yPos += pieChart.frame.size.height + 15;
    
    
    //BarChart
    barChart = [[PNBarChart alloc] initWithFrame:pieChart.frame];
    [barChart setXLabels:@[@"SEP 1",@"SEP 2",@"SEP 3",@"SEP 4",@"SEP 5"]];
    [barChart setYValues:@[@1,  @10, @2, @6, @3]];
    barChart.backgroundColor = [UIColor whiteColor];
    [barChart strokeChart];
    [self addSubview:barChart];
    [self bringSubviewToFront:pieChart];
    
    //Move legend to the desired position and add to view
    [legend setFrame:CGRectMake(xPos, yPos, self.frame.size.width, 60)];
    [self addSubview:legend];
    
    [self addSubview:pieChart];
    yPos += legend.frame.size.height;
    
    UIView *dataContainer = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.frame.size.width, 3 * 40 + (3 * 5) + 10)];
    dataContainer.backgroundColor = [EFGUtilities themeBackgroundGray];
    [self addSubview:dataContainer];
    
    float dataYPos = 5;
    
    total = [[EFGDataPresentLabel alloc] initWithFrame:CGRectMake(0, dataYPos, self.frame.size.width, 40) leftData:@"Carbohydrates" rightData:@"450 g"];
    [dataContainer addSubview:total];
    
    dataYPos += total.frame.size.height + 5;
    
    net = [[EFGDataPresentLabel alloc] initWithFrame:CGRectMake(0, dataYPos, self.frame.size.width, 40) leftData:@"Fat" rightData:@"124 g"];
    [dataContainer addSubview:net];
    
    dataYPos += net.frame.size.height + 5;
    
    goal = [[EFGDataPresentLabel alloc] initWithFrame:CGRectMake(0, dataYPos, self.frame.size.width, 40) leftData:@"Protein" rightData:@"8 mg"];
    [dataContainer addSubview:goal];
    
    yPos += dataContainer.frame.size.height;
    
    [self setSelfFrame];
    
}

- (void)updateView:(NSDictionary *)values{
    barChart.hidden = YES;
    pieChart.hidden = NO;
    legend.hidden = NO;
    [self bringSubviewToFront:pieChart];
    float totalValue = [[values objectForKey:@"carbohydrates"] intValue] + [[values objectForKey:@"proteins"] intValue] + [[values objectForKey:@"fat"] intValue];
    NSArray *items;
    if (totalValue != 0.0) {
        items = @[[PNPieChartDataItem dataItemWithValue:[[values objectForKey:@"carbohydrates"] intValue]  color:PNRed description:@"Carbohydrate"],
                  [PNPieChartDataItem dataItemWithValue:[[values objectForKey:@"proteins"] intValue] color:PNBlue description:@"Protein"],
                  [PNPieChartDataItem dataItemWithValue:[[values objectForKey:@"fat"] intValue] color:PNGreen description:@"Fat"],
                  ];
    }else{
        items = @[[PNPieChartDataItem dataItemWithValue:0  color:PNGrey description:@"No Data / Empty"],
                  ];
    }
    [pieChart updateChartData:items];
    [pieChart strokeChart];
    
    //Updating the Calorie Values
    total.rightData.text = values[@"carbohydrates_goal"];
    net.rightData.text = values[@"fat_goal"];
    goal.rightData.text = values[@"protein_goal"];
}

- (void)updateBarChart:(NSDictionary *)d andAdditionalData:(NSDictionary *)additionalData{
    barChart.hidden = NO;
    pieChart.hidden = YES;
    NSArray *keys = d.allKeys;
    NSArray *values = d.allValues;
    float max = 0;
    for (NSNumber *value in values) {
        if (value.floatValue > max) {
            max = value.floatValue;
        }
    }
    
    if (keys.count <=0 || values.count <= 0) {
        keys = [NSArray arrayWithObjects:@"No Value", nil];
        values = [NSArray arrayWithObjects:@0, nil];
    }
    
    [barChart setXLabels:keys];
    [barChart setYLabels:values];
    [barChart strokeChart];
    legend.hidden = YES;
    [self bringSubviewToFront:barChart];
    
    
    total.rightData.text = [NSString stringWithFormat:@"%@",additionalData[@"carbohydrates"]];
    net.rightData.text = [NSString stringWithFormat:@"%@",additionalData[@"proteins"]];
    goal.rightData.text = [NSString stringWithFormat:@"%@",additionalData[@"fat"]];
}


- (void)setSelfFrame{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, yPos);
}


@end
