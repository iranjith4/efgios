//
//  EFGNutritionCell.m
//  efoodguru
//
//  Created by Ranjithkumar on 11/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGNutritionCell.h"

@implementation EFGNutritionCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void) setUpCellWithTitle: (NSString *)title average : (NSString *)averageString goal: (NSString *)goalString left : (NSString *)leftString{
    
    NSTextAlignment labelAlignment = NSTextAlignmentCenter;
    UIColor *titileColor = [UIColor darkGrayColor];
    UIColor *dataColor = [UIColor grayColor];
    UIFont *labelsFont = [UIFont systemFontOfSize:15];
    
    if (!self.titleLabel) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.frame.size.width * 0.49 - 5, self.frame.size.height)];
        self.titleLabel.textColor = titileColor;
        self.titleLabel.font = labelsFont;
        [self addSubview:self.titleLabel];
    }
    self.titleLabel.text = title;
    
    CGRect dataFrame;
    if (!self.averageLabel) {
        dataFrame = CGRectMake(self.frame.size.width * 0.49, 0, self.frame.size.width * 0.17 -2, self.frame.size.height);
        self.averageLabel = [[UILabel alloc] initWithFrame:dataFrame];
        self.averageLabel.textAlignment = labelAlignment;
        self.averageLabel.font = labelsFont;
        self.averageLabel.textColor = dataColor;
        [self addSubview:self.averageLabel];
    }
    self.averageLabel.text = averageString;
    
    if (!self.goalLabel) {
        dataFrame.origin.x += self.averageLabel.frame.size.width;
        self.goalLabel = [[UILabel alloc] initWithFrame:dataFrame];
        self.goalLabel.textAlignment = labelAlignment;
        self.goalLabel.font = labelsFont;
        self.goalLabel.textColor = dataColor;
        [self addSubview:self.goalLabel];
    }
    self.goalLabel.text = goalString;
    
    if (!self.leftLabel) {
        dataFrame.origin.x += self.goalLabel.frame.size.width;
        self.leftLabel = [[UILabel alloc] initWithFrame:dataFrame];
        self.leftLabel.textColor = dataColor;
        self.leftLabel.font = labelsFont;
        self.leftLabel.textAlignment = labelAlignment;
        [self addSubview:self.leftLabel];
    }
    self.leftLabel.text = leftString;
}
@end
