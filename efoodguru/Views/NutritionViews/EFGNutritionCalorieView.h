//
//  EFGNutritionCalorieView.h
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGNutritionCalorieView : UIView

- (void)updateView:(NSDictionary *) values;
- (void)updateBarChart:(NSDictionary *)d andAdditionalData:(NSDictionary *)additionalData;

@end
