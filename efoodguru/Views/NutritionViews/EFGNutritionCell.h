//
//  EFGNutritionCell.h
//  efoodguru
//
//  Created by Ranjithkumar on 11/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGNutritionCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *averageLabel;
@property (nonatomic, strong) UILabel *goalLabel;
@property (nonatomic, strong) UILabel *leftLabel;

- (void) setUpCellWithTitle: (NSString *)title average : (NSString *)averageString goal: (NSString *)goalString left : (NSString *)leftString;

@end
