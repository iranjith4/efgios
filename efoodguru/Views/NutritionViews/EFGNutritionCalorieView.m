//
//  EFGNutritionCalorieView.m
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGNutritionCalorieView.h"
#import "EFGDataPresentLabel.h"
#import "EFGUtilities.h"
#import "PNChart.h"

@implementation EFGNutritionCalorieView{
    float xPos;
    float yPos;
    UIView *legend;
    PNPieChart *pieChart;
    PNBarChart * barChart;
    EFGDataPresentLabel *total;
    EFGDataPresentLabel *net;
    EFGDataPresentLabel *goal;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView{
    xPos = 20;
    yPos = 15;
    self.backgroundColor = [UIColor whiteColor];
    
    NSArray *items = @[[PNPieChartDataItem dataItemWithValue:10 color:PNRed description:@"Breakfast"],
                       [PNPieChartDataItem dataItemWithValue:20 color:PNBlue description:@"Lunch"],
                       [PNPieChartDataItem dataItemWithValue:40 color:PNGreen description:@"Snacks"],
                       [PNPieChartDataItem dataItemWithValue:30 color:PNYellow description:@"Dinner"]
                       ];
    
    pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(xPos, yPos, self.frame.size.width - 2 * xPos, self.frame.size.width - 2 * xPos) items:items];
    pieChart.descriptionTextColor = [UIColor whiteColor];
    pieChart.descriptionTextFont  = [UIFont fontWithName:@"Avenir-Medium" size:14.0];
    [pieChart strokeChart];
    pieChart.backgroundColor = [UIColor whiteColor];
    
    pieChart.legendStyle = PNLegendItemStyleSerial;
    pieChart.legendFont = [UIFont fontWithName:@"Avenir-Medium" size:14.0];
    legend = [pieChart getLegendWithMaxWidth:200];
    
    yPos += pieChart.frame.size.height + 15;
    
    //Move legend to the desired position and add to view
    [legend setFrame:CGRectMake(xPos, yPos, self.frame.size.width, 60)];
    [self addSubview:legend];
    [self addSubview:pieChart];
    
    yPos += legend.frame.size.height;
    
    
    //BarChart
    barChart = [[PNBarChart alloc] initWithFrame:pieChart.frame];
    [barChart setXLabels:@[@"SEP 1",@"SEP 2",@"SEP 3",@"SEP 4",@"SEP 5"]];
    [barChart setYValues:@[@1,  @10, @2, @6, @3]];
    barChart.backgroundColor = [UIColor whiteColor];
    [barChart strokeChart];
    [self addSubview:barChart];
    [self bringSubviewToFront:pieChart];
    
    
    
    UIView *dataContainer = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, self.frame.size.width, 3 * 40 + (3 * 5) + 10)];
    dataContainer.backgroundColor = [EFGUtilities themeBackgroundGray];
    [self addSubview:dataContainer];
    
    float dataYPos = 5;
    
    total = [[EFGDataPresentLabel alloc] initWithFrame:CGRectMake(0, dataYPos, self.frame.size.width, 40) leftData:@"Total Calories" rightData:@"0"];
    [dataContainer addSubview:total];
    
    dataYPos += total.frame.size.height + 5;
    
    net = [[EFGDataPresentLabel alloc] initWithFrame:CGRectMake(0, dataYPos, self.frame.size.width, 40) leftData:@"Net Calories" rightData:@"0"];
    [dataContainer addSubview:net];
    
    dataYPos += net.frame.size.height + 5;
    
    goal = [[EFGDataPresentLabel alloc] initWithFrame:CGRectMake(0, dataYPos, self.frame.size.width, 40) leftData:@"Goal" rightData:@"1240"];
    [dataContainer addSubview:goal];
    
    yPos += dataContainer.frame.size.height;
    [self setSelfFrame];
}

- (void)updateView:(NSDictionary *)values{
    barChart.hidden = YES;
    pieChart.hidden = NO;
    legend.hidden = NO;
    [self bringSubviewToFront:pieChart];
    float totalValue = [[values objectForKey:@"breakfast"] intValue] + [[values objectForKey:@"lunch"] intValue] + [[values objectForKey:@"snacks"] intValue] + [[values objectForKey:@"dinner"] intValue];
    NSArray *items;
    if (totalValue != 0.0) {
        items = @[[PNPieChartDataItem dataItemWithValue:[[values objectForKey:@"breakfast"] intValue]  color:PNRed description:@"Breakfast"],
                           [PNPieChartDataItem dataItemWithValue:[[values objectForKey:@"luch"] intValue] color:PNBlue description:@"Lunch"],
                           [PNPieChartDataItem dataItemWithValue:[[values objectForKey:@"snacks"] intValue] color:PNGreen description:@"Snacks"],
                           [PNPieChartDataItem dataItemWithValue:[[values objectForKey:@"dinner"] intValue] color:PNYellow description:@"Dinner"]
                           ];
    }else{
        items = @[[PNPieChartDataItem dataItemWithValue:0  color:PNGrey description:@"No Data / Empty"],
                           ];
    }
    [pieChart updateChartData:items];
    [pieChart strokeChart];
    
    //Updating the Calorie Values
    
    //Updating Other Data
    total.leftData.text = @"Total Calories";
    net.leftData.text = @"Net Calories";
    
    total.rightData.text = [NSString stringWithFormat:@"%@",values[@"total_calories"]];
    net.rightData.text = [NSString stringWithFormat:@"%@",values[@"net_calories"]];
    goal.rightData.text = [NSString stringWithFormat:@"%@",values[@"goal"]];
}

- (void)updateBarChart:(NSDictionary *)d andAdditionalData:(NSDictionary *)additionalData{
    barChart.hidden = NO;
    pieChart.hidden = YES;
    NSArray *keys = d.allKeys;
    NSArray *values = d.allValues;
    float max = 0;
    for (NSNumber *value in values) {
        if (value.floatValue > max) {
            max = value.floatValue;
        }
    }
    
    if (keys.count <=0 || values.count <= 0) {
        keys = [NSArray arrayWithObjects:@"No Value", nil];
        values = [NSArray arrayWithObjects:@0, nil];
    }
    
    [barChart setXLabels:keys];
    [barChart setYLabels:values];
    [barChart strokeChart];
    legend.hidden = YES;
    [self bringSubviewToFront:barChart];
    
    //Updating Other Data
    total.leftData.text = @"Weekly Calorie Goal";
    net.leftData.text = @"Daily Average";

    total.rightData.text = [NSString stringWithFormat:@"%@",additionalData[@"total"]];
    net.rightData.text = [NSString stringWithFormat:@"%@",additionalData[@"avg"]];
    goal.rightData.text = [NSString stringWithFormat:@"%@",additionalData[@"goal"]];
}

- (void)setSelfFrame{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, yPos);
}


@end
