//
//  EFGQuestionSingleView.m
//  efoodguru
//
//  Created by Ranjithkumar on 11/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGQuestionSingleView.h"

@implementation EFGQuestionSingleView{
    float xPos;
    float yPos;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        xPos = 0;
        yPos = 0;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)setUpViewWithQuestion : (NSString *)questionString labelPlaceHolder: (NSString *)placeHolderString{
    xPos = self.frame.size.width * 0.15 / 2;
    self.questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, self.frame.size.width * 0.85, 0)];
    self.questionLabel.text = questionString;
    self.questionLabel.textColor = [UIColor blackColor];
    self.questionLabel.font = [UIFont systemFontOfSize:17];
    self.questionLabel.textAlignment = NSTextAlignmentCenter;
    self.questionLabel.numberOfLines = 0;
    self.questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.questionLabel sizeToFit];
    self.questionLabel.center = CGPointMake(self.center.x, self.questionLabel.center.y);
    [self addSubview:self.questionLabel];
    
    yPos += self.questionLabel.frame.size.height + 15;
    xPos = self.frame.size.width * 0.15;
    self.answerField = [[UITextField alloc] initWithFrame:CGRectMake(xPos, yPos, self.frame.size.width * 0.70, 45)];
    self.answerField.placeholder = placeHolderString;
    self.answerField.delegate = self;
    self.answerField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    self.answerField.textColor = [UIColor grayColor];
    self.answerField.textAlignment = NSTextAlignmentCenter;
    self.answerField.font = [UIFont systemFontOfSize:16];
    [self addSubview:self.answerField];
    yPos += self.answerField.frame.size.height;
    
    //Adding a line
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(self.answerField.frame.origin.x, yPos , self.answerField.frame.size.width, 1)];
    line.backgroundColor = [UIColor grayColor];
    [self addSubview:line];
    [self resizeView];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)resizeView{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.height, yPos + 15);
}

@end
