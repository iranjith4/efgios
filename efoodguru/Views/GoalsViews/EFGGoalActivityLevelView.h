//
//  EFGGoalActivityLevelView.h
//  efoodguru
//
//  Created by Ranjithkumar on 11/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGGoalActivityLevelView : UIView <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UILabel *questionLabel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSString *answerData;

@end
