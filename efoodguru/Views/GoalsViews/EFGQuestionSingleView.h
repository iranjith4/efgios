//
//  EFGQuestionSingleView.h
//  efoodguru
//
//  Created by Ranjithkumar on 11/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGQuestionSingleView : UIView <UITextFieldDelegate>

@property (nonatomic, strong) UILabel *questionLabel;
@property (nonatomic, strong) UITextField *answerField;
- (void)setUpViewWithQuestion : (NSString *)questionString labelPlaceHolder: (NSString *)placeHolderString;

@end
