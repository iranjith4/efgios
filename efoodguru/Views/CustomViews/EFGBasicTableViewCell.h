//
//  EFGBasicTableViewCell.h
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#define CELL_HEIGHT 44.0

@interface EFGBasicTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *checkImage;
@property (nonatomic, strong) UILabel *nameLabel;

- (void)setupCell:(NSDictionary *)data isSub:(BOOL)isSub;

@end
