//
//  EFGRadioButtonCell.h
//  efoodguru
//
//  Created by Ranjithkumar on 16/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#define CELL_HEIGHT 44.0

@interface EFGRadioButtonCell : UITableViewCell

@property (nonatomic, strong) UIImageView *checkImage;
@property (nonatomic, strong) UILabel *nameLabel;

- (void)setupCell:(NSDictionary *)data;


@end
