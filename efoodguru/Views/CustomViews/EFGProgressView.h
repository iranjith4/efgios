//
//  EFGProgressView.h
//  efoodguru
//
//  Created by Ranjithkumar on 11/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

#define PROGRESS_SPACE 15.0
#define INTENT_SPACE 10.0
#define PROGRESS_HEIGHT 3.0

@interface EFGProgressView : UIView

@property (nonatomic) int numberOfPages;
@property (nonatomic) int currentPage;
@property (nonatomic, strong) UIColor *unProgressedColor;
@property (nonatomic, strong) UIColor *progressedColor;

- (void)makeProgress:(NSInteger)page;
- (void) initProgressBar;
@end
