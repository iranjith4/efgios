//
//  EFGTimeIntervalSelector.h
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EFGTimeIntervalProtocol <NSObject>
@optional
- (void)timeTitleTapped;
- (void)loadDayView : (NSString *)date;
- (void)loadWeekViewFrom : (NSString *)fromDate toDate : (NSString *)toDate;

@end

typedef enum : NSUInteger {
    TimeTypeDay,
    TimeTypeWeek,
    TimeTypeDate
} TimeType;

@interface EFGTimeIntervalSelector : UIView

@property (nonatomic, strong) UIButton *back;
@property (nonatomic, strong) UIButton *forward;
@property (nonatomic, strong) UIButton *timeInterval;
@property (nonatomic, strong) id <EFGTimeIntervalProtocol> delegate;
@property (nonatomic) TimeType timeType;

- (void)setType : (TimeType)t;
- (void)changeToDate:(NSDate *)date;

@end
