//
//  EFGDataPresentLabel.m
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGDataPresentLabel.h"

@implementation EFGDataPresentLabel{
    NSString *lStr;
    NSString *rStr;
}

- (instancetype)initWithFrame:(CGRect)frame leftData:(NSString *)leftString rightData:(NSString *)rightString
{
    self = [super initWithFrame:frame];
    if (self) {
        lStr = leftString;
        rStr = rightString;
        [self initView];
    }
    return self;
}

- (void) initView{
    
    self.backgroundColor = [UIColor whiteColor];
    
    //left one
    self.leftData = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.frame.size.width * 0.60 - 5, self.frame.size.height)];
    self.leftData.text = lStr;
    self.leftData.textColor = [UIColor grayColor];
    self.leftData.font = [UIFont systemFontOfSize:17];
    [self addSubview:self.leftData];
    
    self.rightData = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width * 0.60, 0, self.frame.size.width * 0.40 - 5, self.frame.size.height)];
    self.rightData.text = rStr;
    self.rightData.textAlignment = NSTextAlignmentRight;
    self.rightData.textColor = [UIColor darkGrayColor];
    self.rightData.font = [UIFont systemFontOfSize:17];
    [self addSubview:self.rightData];
}

@end
