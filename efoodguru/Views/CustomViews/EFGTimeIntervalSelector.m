//
//  EFGTimeIntervalSelector.m
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGTimeIntervalSelector.h"

@implementation EFGTimeIntervalSelector{
    NSDate *selectedDate;
    int dateWeekCount;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initViews];
    }
    return self;
}

- (void)initViews{
    //Initialization
    dateWeekCount = 0;
    self.timeType = TimeTypeDay;
    selectedDate = [NSDate date];
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.back = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width * 0.20, self.frame.size.height)];
    [self.back setTitle:@"<" forState:UIControlStateNormal];
    [self.back addTarget:self action:@selector(tappedBack) forControlEvents:UIControlEventTouchUpInside];
    [self.back setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self addSubview:self.back];
    
    self.timeInterval = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width * 0.20,0,self.frame.size.width * 0.60, self.frame.size.height)];
    [self.timeInterval setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.timeInterval setTitle:@"Today" forState:UIControlStateNormal];
    [self.timeInterval addTarget:self action:@selector(tappedTimeInterval) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.timeInterval];
    
    self.forward = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width * 0.80,0,self.frame.size.width * 0.20, self.frame.size.height)];
    [self.forward setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.forward addTarget:self action:@selector(tappedForward) forControlEvents:UIControlEventTouchUpInside];
    [self.forward setTitle:@">" forState:UIControlStateNormal];
    [self addSubview:self.forward];
}

- (void)tappedBack{
    if (self.timeType != TimeTypeDate) {
        dateWeekCount--;
    }
    switch (self.timeType) {
        case TimeTypeDay:
        {
            [self.delegate loadDayView:[self getDateWithMonth:0 day:dateWeekCount hour:0 minute:0 second:0]];
            [self changeTimeTitle:[self getFormattedFullDateWithMonth:0 day:dateWeekCount hour:0 minute:0 second:0]];
        }
            break;
            
        case TimeTypeWeek:
        {
            int start = (dateWeekCount - 1) * 7;
            int end = dateWeekCount * 7;
            [self.delegate loadWeekViewFrom:[self getDateWithMonth:0 day:start hour:0 minute:0 second:0] toDate:[self getDateWithMonth:0 day:end hour:0 minute:0 second:0]];
            [self changeTimeTitle:[NSString stringWithFormat:@"%@ to %@",[self getFormattedDateWithMonth:0 day:start hour:0 minute:0 second:0],[self getFormattedDateWithMonth:0 day:end hour:0 minute:0 second:0]]];
        }
            break;
            
        case TimeTypeDate:
        {
            NSDate *newDate = [self getDatedDateWithMonth:0 day:-1 hour:0 minute:0 second:0 fromDate:selectedDate];
            selectedDate = newDate;
            if (![self isSelectedDateInFuture]) {
                [self.delegate loadDayView:[self getStringDateForDate:selectedDate inFormat:@"yyyy-MM-dd"]];
                [self changeTimeTitle:[self getStringDateForDate:selectedDate inFormat:@"MMM dd, yyyy"]];
            }else{
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)tappedForward{
    if (self.timeType != TimeTypeDate) {
        dateWeekCount++;
    }
    if (dateWeekCount<=0) {
        switch (self.timeType) {
            case TimeTypeDay:
            {
                [self.delegate loadDayView:[self getDateWithMonth:0 day:dateWeekCount hour:0 minute:0 second:0]];
                [self changeTimeTitle:[self getFormattedFullDateWithMonth:0 day:dateWeekCount hour:0 minute:0 second:0]];
            }
                break;
                
            case TimeTypeWeek:
            {
                int start = (dateWeekCount - 1) * 7;
                int end = dateWeekCount * 7;
                [self.delegate loadWeekViewFrom:[self getDateWithMonth:0 day:start hour:0 minute:0 second:0] toDate:[self getDateWithMonth:0 day:end hour:0 minute:0 second:0]];
                [self changeTimeTitle:[NSString stringWithFormat:@"%@ to %@",[self getFormattedDateWithMonth:0 day:start hour:0 minute:0 second:0],[self getFormattedDateWithMonth:0 day:end hour:0 minute:0 second:0]]];
            }
                break;
                
            case TimeTypeDate:
            {
                NSDate *newDate = [self getDatedDateWithMonth:0 day:+1 hour:0 minute:0 second:0 fromDate:selectedDate];
                selectedDate = newDate;
                if (![self isSelectedDateInFuture]) {
                    [self.delegate loadDayView:[self getStringDateForDate:selectedDate inFormat:@"yyyy-MM-dd"]];
                    [self changeTimeTitle:[self getStringDateForDate:selectedDate inFormat:@"MMM dd, yyyy"]];
                }else{
                    selectedDate = [self getDatedDateWithMonth:0 day:-1 hour:0 minute:0 second:0 fromDate:newDate];
                }
            }
                break;
                
            default:
                break;
        }
    }else{
        dateWeekCount--;
        //TODO: Show alert for future dates.
    }
}

- (BOOL)isSelectedDateInFuture{
    if ([[NSDate date] compare:selectedDate] == NSOrderedSame) {
        return NO;
    }else if([[NSDate date] compare:selectedDate] == NSOrderedAscending){
        return YES;
    }else if([[NSDate date] compare:selectedDate] == NSOrderedDescending){
        return NO;
    }else{
        return YES;
    }
}

- (void)tappedTimeInterval{
    [self.delegate timeTitleTapped];
}

- (void)changeTimeTitle:(NSString *)string{
    [self.timeInterval setTitle:string forState:UIControlStateNormal];
}

- (void)setType :(TimeType)t{
    dateWeekCount = 0;
    self.timeType = t;
    switch (t) {
        case TimeTypeDate:
        {
            [self changeTimeTitle:@"Today"];
        }
            break;
            
        case TimeTypeDay:
            [self changeTimeTitle:@"Today"];
            [self.delegate loadDayView:[self getDateWithMonth:0 day:0 hour:0 minute:0 second:0]];
            break;
            
        case TimeTypeWeek:
            [self changeTimeTitle:@"This Week"];
            [self.delegate loadWeekViewFrom:[self getDateWithMonth:0 day:-7 hour:0 minute:0 second:0] toDate:[self getDateWithMonth:0 day:0 hour:0 minute:0 second:0]];
            break;
            
        default:
            break;
    }
    
}

- (void)changeToDate:(NSDate *)date{
    selectedDate = date;
    [self changeTimeTitle:[self getStringDateForDate:date inFormat:@"MMM dd, yyyy"]];
    [self.delegate loadDayView:[self getStringDateForDate:date inFormat:@"yyyy-MM-dd"]];
}

//Helper Methods

- (NSString *)getDateWithMonth:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second{
    NSDate *currentDate = [NSDate date];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:month];
    [dateComponents setDay:day];
    [dateComponents setHour:hour];
    [dateComponents setMinute:minute];
    [dateComponents setSecond:second];
    NSDate *date =  [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    return [dateFormatter stringFromDate:date];
}

- (NSString *)getDateWithMonth:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second fromDate:(NSDate *)dt inFormat:(NSString *)format{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:month];
    [dateComponents setDay:day];
    [dateComponents setHour:hour];
    [dateComponents setMinute:minute];
    [dateComponents setSecond:second];
    NSDate *date =  [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:dt options:0];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    return [dateFormatter stringFromDate:date];
}

- (NSDate *)getDatedDateWithMonth:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second fromDate:(NSDate *)dt{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:month];
    [dateComponents setDay:day];
    [dateComponents setHour:hour];
    [dateComponents setMinute:minute];
    [dateComponents setSecond:second];
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:dt options:0];
}

- (NSString *)getFormattedDateWithMonth:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second{
    NSDate *currentDate = [NSDate date];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:month];
    [dateComponents setDay:day];
    [dateComponents setHour:hour];
    [dateComponents setMinute:minute];
    [dateComponents setSecond:second];
    NSDate *date =  [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MMM dd";
    return [dateFormatter stringFromDate:date];
}

- (NSString *)getFormattedFullDateWithMonth:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second{
    NSDate *currentDate = [NSDate date];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:month];
    [dateComponents setDay:day];
    [dateComponents setHour:hour];
    [dateComponents setMinute:minute];
    [dateComponents setSecond:second];
    NSDate *date =  [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MMM dd,yyyy";
    return [dateFormatter stringFromDate:date];
}

- (NSString *)getStringDateForDate:(NSDate *)date inFormat:(NSString *)format{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    return [dateFormatter stringFromDate:date];
}

@end
