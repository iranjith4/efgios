//
//  EFGTitleLabel.h
//  efoodguru
//
//  Created by Ranjithkumar on 04/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGTitleLabel : UIView

@property (nonatomic, strong) UILabel *label;
- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title;

@end
