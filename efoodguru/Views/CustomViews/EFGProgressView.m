//
//  EFGProgressView.m
//  efoodguru
//
//  Created by Ranjithkumar on 11/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGProgressView.h"

@implementation EFGProgressView{
    CGRect rect;
    float progressWidth;
    float progressHeight;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        rect = frame;
    }
    return self;
}

- (void)initSetUp{
    if (!self.numberOfPages) {
        self.numberOfPages = 4;
    }
    if (!self.unProgressedColor) {
        self.unProgressedColor = [UIColor lightGrayColor];
    }
    if (!self.progressedColor) {
        self.progressedColor = [UIColor darkGrayColor];
    }
    progressWidth = (rect.size.width - 2 * INTENT_SPACE - ((self.numberOfPages - 1) * PROGRESS_SPACE)) / self.numberOfPages;
    progressHeight = PROGRESS_HEIGHT;
}

- (void) initProgressBar{
    [self initSetUp];
    float xPos = INTENT_SPACE;
    for (int i = 0; i < self.numberOfPages; i++) {
        UIView *progress = [[UIView alloc] initWithFrame:CGRectMake(xPos, 0, progressWidth, progressHeight)];
        progress.backgroundColor = self.unProgressedColor;
        progress.tag = i;
        progress.center = CGPointMake(progress.center.x, rect.size.height / 2);
        [self addSubview:progress];
        xPos += progress.frame.size.width + PROGRESS_SPACE ;
    }
}

- (void)makeProgress:(NSInteger)page{
    for (UIView *progress in self.subviews) {
        UIView *newView = progress;
        if (progress.tag < page) {
            newView.backgroundColor = self.progressedColor;
        }else{
            newView.backgroundColor = self.unProgressedColor;
        }
    }
}


@end
