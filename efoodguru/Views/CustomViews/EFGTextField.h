//
//  EFGTextField.h
//  efoodguru
//
//  Created by Ranjithkumar on 04/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGTextField : UIView <UITextFieldDelegate>

- (instancetype)initWithFrame:(CGRect)frame labelText: (NSString *)labelText placeHolder:(NSString *)placeHolder;
- (void)setTextFieldBorderStyle :(UITextBorderStyle)borderStyle;

@property (nonatomic,strong) UILabel *label;
@property (nonatomic, strong) UITextField *text;


@end
