


//
//  EFGNavigationView.m
//  efoodguru
//
//  Created by Ranjithkumar on 01/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGNavigationView.h"
#import "EFGUtilities.h"

@implementation EFGNavigationView{
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];
    }
    return self;
}

- (void)initViewsWithColor : (UIColor *)color title: (NSString *)title andBackText : (NSString *)backString{
    if (color) {
        self.backgroundColor = color;
    }
    
    if (title && ![title isEqualToString:@""]) {
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.text = title;
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        [self.titleLabel sizeToFit];
        self.titleLabel.center = CGPointMake(self.frame.size.width / 2 , self.frame.size.height / 2 - self.titleLabel.frame.size.height / 2 + STATUSBAR_HEIGHT );
        [self addSubview:self.titleLabel];
    }
    
    self.backButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 0, 0)];
    [self.backButton setTitle:backString forState:UIControlStateNormal];
    [self.backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backButton.titleLabel.font = [UIFont systemFontOfSize:18];
    [self.backButton addTarget:self action:@selector(backTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton sizeToFit];
    self.backButton.center = CGPointMake(self.backButton.center.x, self.frame.size.height / 2 - self.titleLabel.frame.size.height / 2 + STATUSBAR_HEIGHT / 2);
    [self addSubview:self.backButton];
}

- (void)backTapped{
    [self.delegate backNavigationTapped];
}

@end
