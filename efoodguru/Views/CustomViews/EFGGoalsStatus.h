//
//  EFGGoalsStatus.h
//  efoodguru
//
//  Created by Ranjithkumar on 06/01/16.
//  Copyright © 2016 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGGoalsStatus : UIView

- (void)setGoal : (NSString *)goalValue;
- (void)setFood : (NSString *)foodValue;
- (void) setExercise : (NSString *)exerciseValue;
- (void)setRemaining : (NSString *)remainingValue;

@end
