//
//  EFGGoalsStatus.m
//  efoodguru
//
//  Created by Ranjithkumar on 06/01/16.
//  Copyright © 2016 EFG. All rights reserved.
//

#import "EFGGoalsStatus.h"

@implementation EFGGoalsStatus{
    //Labels
    UILabel *goal;
    UILabel *food;
    UILabel *exercise;
    UILabel *remaining;
    UILabel *minus;
    UILabel *plus;
    UILabel *equal;
    
    //SubLabels
    UILabel *goalLabel;
    UILabel *foodLabel;
    UILabel *exerciseLabel;
    UILabel *remainingLablel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void) initView{
    
    //Label Properties
    UIFont *labelFont = [UIFont boldSystemFontOfSize:16];
    UIFont *subLabelFont = [UIFont systemFontOfSize:9];
    NSTextAlignment sublabelTextAlignment = NSTextAlignmentCenter;
    float symbolsWidth = 20;
    float widthOfLabels = (self.frame.size.width - symbolsWidth * 4 - 15) / 4;
    CGRect frame;
    
    
    //Title Label
    UILabel *calorieRemaining = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.frame.size.width - 10, 35)];
    calorieRemaining.font = [UIFont systemFontOfSize:17];
    calorieRemaining.text = @"Calorie Remaining";
    [self addSubview:calorieRemaining];
    
    //Goal
    goal = [[UILabel alloc] init];
    goal.font = labelFont;
    goal.text = @"2340";
    [goal sizeToFit];
    frame = goal.frame;
    frame.origin.x = 5;
    frame.origin.y = self.frame.size.height / 2 - frame.size.height / 2;
    frame.size.width = widthOfLabels;
    goal.frame = frame;
    goal.textAlignment = sublabelTextAlignment;
    [self addSubview:goal];
    
    //Goal SubLabel
    goalLabel = [[UILabel alloc] init];
    goalLabel.font = subLabelFont;
    goalLabel.text = @"Goal";
    [goalLabel sizeToFit];
    frame = goalLabel.frame;
    frame.origin.x = goal.frame.origin.x;
    frame.origin.y = goal.frame.origin.y + goal.frame.size.height + 2;
    frame.size.width = widthOfLabels;
    goalLabel.textAlignment = sublabelTextAlignment;
    goalLabel.center = CGPointMake(goal.center.x, goalLabel.center.y);
    goalLabel.frame = frame;
    [self addSubview:goalLabel];
    
    //Minus
    minus = [[UILabel alloc] init];
    minus.font = labelFont;
    minus.text = @"-";
    [minus sizeToFit];
    frame = minus.frame;
    frame.origin.x = goal.frame.origin.x + goal.frame.size.width + 5;
    frame.origin.y = self.frame.size.height / 2 - frame.size.height / 2;
    frame.size.width = symbolsWidth;
    minus.frame = frame;
    minus.textAlignment = sublabelTextAlignment;
    [self addSubview:minus];
    
    //Food
    food = [[UILabel alloc] init];
    food.font = labelFont;
    food.text = @"0";
    [food sizeToFit];
    frame = food.frame;
    frame.origin.x = minus.frame.origin.x + minus.frame.size.width + 5;
    frame.origin.y = self.frame.size.height / 2 - frame.size.height / 2;
    frame.size.width = widthOfLabels;
    food.frame = frame;
    food.textAlignment = sublabelTextAlignment;
    [self addSubview:food];
    
    //Food Title Label
    foodLabel = [[UILabel alloc] init];
    foodLabel.font = subLabelFont;
    foodLabel.text = @"Food";
    [foodLabel sizeToFit];
    frame = foodLabel.frame;
    frame.origin.x = food.frame.origin.x;
    frame.origin.y = food.frame.origin.y + food.frame.size.height + 2;
    frame.size.width = widthOfLabels;
    foodLabel.textAlignment = sublabelTextAlignment;
    foodLabel.frame = frame;
    foodLabel.center = CGPointMake(food.center.x, foodLabel.center.y);
    [self addSubview:foodLabel];
    
    plus = [[UILabel alloc] init];
    plus.font = labelFont;
    plus.text = @"+";
    [plus sizeToFit];
    frame = plus.frame;
    frame.origin.x = food.frame.origin.x + food.frame.size.width + 5;
    frame.origin.y = self.frame.size.height / 2 - frame.size.height / 2;
    frame.size.width = symbolsWidth;
    plus.frame = frame;
    plus.textAlignment = sublabelTextAlignment;
    [self addSubview:plus];
    
    exercise = [[UILabel alloc] init];
    exercise.font = labelFont;
    exercise.text = @"0";
    [exercise sizeToFit];
    frame = exercise.frame;
    frame.origin.x = plus.frame.origin.x + plus.frame.size.width + 5;
    frame.origin.y = self.frame.size.height / 2 - frame.size.height / 2;
    frame.size.width = widthOfLabels;
    exercise.frame = frame;
    exercise.textAlignment = sublabelTextAlignment;
    [self addSubview:exercise];
    
    //Exercise Title Label
    exerciseLabel = [[UILabel alloc] init];
    exerciseLabel.font = subLabelFont;
    exerciseLabel.text = @"Exercise";
    [exerciseLabel sizeToFit];
    frame = exerciseLabel.frame;
    frame.origin.x = exercise.frame.origin.x;
    frame.origin.y = exercise.frame.origin.y + exercise.frame.size.height + 2;
    frame.size.width = widthOfLabels;
    exerciseLabel.textAlignment = sublabelTextAlignment;
    exerciseLabel.frame = frame;
    exerciseLabel.center = CGPointMake(exercise.center.x, exerciseLabel.center.y);
    [self addSubview:exerciseLabel];
    
    equal = [[UILabel alloc] init];
    equal.font = labelFont;
    equal.text = @"=";
    [equal sizeToFit];
    frame = equal.frame;
    frame.origin.x = exercise.frame.origin.x + exercise.frame.size.width + 5;
    frame.origin.y = self.frame.size.height / 2 - frame.size.height / 2;
    frame.size.width = symbolsWidth;
    equal.frame = frame;
    equal.textAlignment = sublabelTextAlignment;
    [self addSubview:equal];
    
    remaining = [[UILabel alloc] init];
    remaining.font = labelFont;
    remaining.text = @"2340";
    [remaining sizeToFit];
    frame = remaining.frame;
    frame.origin.x = equal.frame.origin.x + equal.frame.size.width + 5;
    frame.origin.y = self.frame.size.height / 2 - frame.size.height / 2;
    frame.size.width = widthOfLabels;
    remaining.frame = frame;
    remaining.textAlignment = sublabelTextAlignment;
    [self addSubview:remaining];
    
    //Remaining Label
    remainingLablel = [[UILabel alloc] init];
    remainingLablel.font = subLabelFont;
    remainingLablel.text = @"Remaining";
    [remainingLablel sizeToFit];
    frame = remainingLablel.frame;
    frame.origin.x = remaining.frame.origin.x;
    frame.origin.y = remaining.frame.origin.y + remaining.frame.size.height + 2;
    frame.size.width = widthOfLabels;
    remainingLablel.textAlignment = sublabelTextAlignment;
    remainingLablel.frame = frame;
    remainingLablel.center = CGPointMake(remaining.center.x, remainingLablel.center.y);
    [self addSubview:remainingLablel];
}

- (void)setGoal : (NSString *)goalValue{
    goal.text = goalValue;
}

- (void)setFood : (NSString *)foodValue{
    food.text = foodValue;
}

- (void) setExercise : (NSString *)exerciseValue {
    exercise.text = exerciseValue;
}

- (void)setRemaining : (NSString *)remainingValue{
    remaining.text = remainingValue;
}

@end
