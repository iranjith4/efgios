//
//  EFGCheckStatement.h
//  efoodguru
//
//  Created by Ranjithkumar on 01/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 * Protocol Methods
 */

@protocol EFGCheckStatementProtocol <NSObject>

- (void)checkedTapped:(BOOL)isChecked;

@end

@interface EFGCheckStatement : UIView

/*
 *Properties and Delegates
 */

@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIImageView *checkImage;
@property (nonatomic) id <EFGCheckStatementProtocol>delegate;

/*
 * For initializing the View
 * 'text' cannot be empty
 */

- (void)initWithText:(NSString *)text;

@end
