//
//  EFGTitleLabel.m
//  efoodguru
//
//  Created by Ranjithkumar on 04/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGTitleLabel.h"
#import "EFGUtilities.h"

@implementation EFGTitleLabel{
    NSString *titleText;
}

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        titleText = title;
        [self initView];
    }
    return self;
}

- (void)initView{
    self.backgroundColor = [EFGUtilities themeColor];
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width - 20, self.frame.size.height)];
    self.label.text = titleText;
    self.label.backgroundColor = [EFGUtilities themeColor];
    self.label.textColor = [UIColor whiteColor];
    self.label.font = [UIFont boldSystemFontOfSize:15];
    self.label.numberOfLines = 1;
    self.center = CGPointMake(self.label.center.x, self.center.y);
    [self addSubview:self.label];
}

@end
