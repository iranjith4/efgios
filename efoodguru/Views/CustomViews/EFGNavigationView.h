//
//  EFGNavigationView.h
//  efoodguru
//
//  Created by Ranjithkumar on 01/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

/**********************************************************************
 Protocol Methods to actually dismiss the controller
 **********************************************************************/

@protocol EFGNavigationProtocol <NSObject>

- (void)backNavigationTapped;

@end


@interface EFGNavigationView : UIView

/**********************************************************************
 Properties for the View
 **********************************************************************/

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic) id<EFGNavigationProtocol> delegate;

/**********************************************************************
 * color is Optional
 * title is optional
 * backString is mandatory
***********************************************************************/
- (void)initViewsWithColor : (UIColor *)color title: (NSString *)title andBackText : (NSString *)backString;

@end
