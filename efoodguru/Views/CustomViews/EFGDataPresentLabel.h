//
//  EFGDataPresentLabel.h
//  efoodguru
//
//  Created by Ranjithkumar on 07/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGDataPresentLabel : UIView

@property (nonatomic,strong) UILabel *leftData;
@property (nonatomic,strong) UILabel *rightData;

- (instancetype)initWithFrame:(CGRect)frame leftData:(NSString *)leftString rightData:(NSString *)rightString;

@end
