//
//  EFGRadioButtonCell.m
//  efoodguru
//
//  Created by Ranjithkumar on 16/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGRadioButtonCell.h"

@implementation EFGRadioButtonCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // configure control(s)
        [self loadCells];
    }
    return self;
}

- (void)loadCells{
    if (self.checkImage == nil) {
        self.checkImage = [[UIImageView alloc] initWithFrame:CGRectMake(CELL_HEIGHT * 0.20, CELL_HEIGHT * 0.20, CELL_HEIGHT * 0.60, CELL_HEIGHT * 0.60)];
        [self addSubview:self.checkImage];
        self.checkImage.image = [UIImage imageNamed:@"check_off"];
    }
    
    if (self.nameLabel == nil) {
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.checkImage.frame.origin.x + self.checkImage.frame.size.width + 10, 0 ,self.frame.size.width - 2 * self.checkImage.frame.origin.x - self.checkImage.frame.size.width - 20, 50)];
        self.nameLabel.textColor = [UIColor blackColor];
        [self addSubview:self.nameLabel];
        self.nameLabel.center = CGPointMake(self.nameLabel.center.x, self.center.y);
    }
    
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}


- (void)setupCell:(NSString *)data{
    self.nameLabel.text = data;
    if (self.isSelected) {
        self.checkImage.image = [UIImage imageNamed:@"check_on"];
        NSLog(@"on");
    }else{
        NSLog(@"off");
        self.checkImage.image = [UIImage imageNamed:@"check_off"];
    }
}

@end
