//
//  EFGTextField.m
//  efoodguru
//
//  Created by Ranjithkumar on 04/12/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGTextField.h"
#import "EFGUtilities.h"

@implementation EFGTextField{
    NSString *labelTexts;
    NSString *placeHolderText;
}

- (instancetype)initWithFrame:(CGRect)frame labelText: (NSString *)labelText placeHolder:(NSString *)placeHolder
{
    self = [super initWithFrame:frame];
    if (self) {
        labelTexts = labelText;
        placeHolderText = placeHolder;
        [self initUI];
    }
    return self;
}

- (void)initUI{
    
    //Label
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.frame.size.width * 0.40 - 15, 40)];
    self.label.text = labelTexts;
    self.label.font = [UIFont systemFontOfSize:15];
    self.label.textColor = [UIColor grayColor];
    self.label.minimumScaleFactor = 0.5;
    self.label.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.label];
    
    //Text Field
    self.text = [[UITextField alloc] initWithFrame:CGRectMake(self.label.frame.origin.x + self.label.frame.size.width + 5, 10, self.frame.size.width * 0.60 - 15, 40)];
    self.text.borderStyle = UITextBorderStyleRoundedRect;
    self.text.delegate = self;
    if (placeHolderText) {
        self.text.placeholder = placeHolderText;
    }
    [self addSubview:self.text];
}

-(void)setTextFieldBorderStyle:(UITextBorderStyle)borderStyle{
    if (borderStyle == UITextBorderStyleNone) {
        self.text.borderStyle = borderStyle;
        CGRect frame = self.text.frame;
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y + frame.size.height, frame.size.width, 1)];
        line.backgroundColor = [UIColor grayColor];
        [self addSubview:line];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
