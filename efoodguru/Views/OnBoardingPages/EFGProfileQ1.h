//
//  EFGProfileQ1.h
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFGBasicTableViewCell.h"

@protocol EFGProfileQ1Protocol <NSObject>

- (void)openSubSelectionWithData : (NSArray *)subAvoidance;

@end

@interface EFGProfileQ1 : UIView <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UILabel *questionLabel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic) id <EFGProfileQ1Protocol> delegate;
@property (nonatomic, strong) NSArray *answerData;

- (instancetype)initWithFrame:(CGRect)frame andData:(NSArray *)array;

- (void)saveSubAvoidanceValue :(NSArray *)a forAvoidance: (NSString *)avd;

@end
