//
//  EFGProfileQ2.h
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFGBasicTableViewCell.h"

@interface EFGProfileQ2 : UIView<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UILabel *questionLabel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSDictionary *answerData;

- (instancetype)initWithFrame:(CGRect)frame andData:(NSArray *)array;

@end
