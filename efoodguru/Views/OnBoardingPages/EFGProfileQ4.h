//
//  EFGProfileQ4.h
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ELEMENT_HEIGHT 35

@interface EFGProfileQ4 : UIView <UITextFieldDelegate>

@property (nonatomic, strong) UILabel *questionLabel;
@property (nonatomic, strong) NSString *answerData;

- (NSNumber *)getAnswers;

@end
