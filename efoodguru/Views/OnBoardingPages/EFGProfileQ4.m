//
//  EFGProfileQ4.m
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGProfileQ4.h"
#import "EFGUtilities.h"
#import "ActionSheetStringPicker.h"

@implementation EFGProfileQ4{
    UITextField *servingsField;
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadView];
    }
    return self;
}

- (void)loadView{
    self.questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.frame.size.width - 20, 0)];
    self.questionLabel.text = @"What would you like to make the default serving size for this profile ?";
    self.questionLabel.numberOfLines = 0;
    self.questionLabel.textAlignment = NSTextAlignmentCenter;
    self.questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.questionLabel sizeToFit];
    [self addSubview:self.questionLabel];
    
    
    servingsField = [[UITextField alloc] initWithFrame:CGRectMake(20, self.questionLabel.frame.size.height + self.questionLabel.frame.origin.y + 30, self.frame.size.width - 40, ELEMENT_HEIGHT)];
    servingsField.center = CGPointMake(self.center.x, servingsField.center.y);
    servingsField.borderStyle = UITextBorderStyleRoundedRect;
    servingsField.text = @"1";
    servingsField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    servingsField.delegate = self;
    [self addSubview:servingsField];
    
    self.answerData = @"1";
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == servingsField) {
        //[self loadServings]; //Not showing Picker
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(NSNumber *)getAnswers{
    return [NSNumber numberWithInt:servingsField.text.intValue];
}



- (void) loadServings{
    NSArray *colors = [NSArray arrayWithObjects:@"1", @"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Servings"
                                            rows:colors
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           servingsField.text = selectedValue;
                                           self.answerData = selectedValue;
                                           [servingsField resignFirstResponder];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         [servingsField resignFirstResponder];
                                     }
                                          origin:self];
}

@end
