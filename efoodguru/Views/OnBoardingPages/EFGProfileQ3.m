//
//  EFGProfileQ3.m
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGProfileQ3.h"
#import "EFGRadioButtonCell.h"

@implementation EFGProfileQ3{
    CGRect rect;
    NSArray *optionsArray;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        rect = frame;
        [self loadView];
    }
    return self;
}

- (void)loadView{
    
    optionsArray = [NSArray arrayWithObjects:@"Yes",@"No",nil];
    
    self.questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, rect.size.width - 20, 0)];
    self.questionLabel.text = @"Do you prefer Organic Food ?";
    self.questionLabel.numberOfLines = 0;
    self.questionLabel.textAlignment = NSTextAlignmentCenter;
    self.questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.questionLabel sizeToFit];
    [self addSubview:self.questionLabel];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, self.questionLabel.frame.origin.y + self.questionLabel.frame.size.height + 10, rect.size.width - 20, rect.size.height - self.questionLabel.frame.origin.y + self.questionLabel.frame.size.height - 150) style:UITableViewStylePlain];
    [self.tableView registerClass:[EFGRadioButtonCell class] forCellReuseIdentifier:@"cell_radio"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self addSubview:self.tableView];
    self.tableView.layer.cornerRadius = 6.0;
    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    [self.tableView sizeToFit];
    
    
    self.answerData = [NSNumber numberWithInt:1];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return optionsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGRadioButtonCell *cell = (EFGRadioButtonCell *)[tableView dequeueReusableCellWithIdentifier:@"cell_radio" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[EFGRadioButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell_radio"];
    }
    [cell setupCell:[optionsArray objectAtIndex:indexPath.row]];
    if (indexPath.row == 0) {
        cell.selected = YES;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGRadioButtonCell *cell = (EFGRadioButtonCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.checkImage.image = [UIImage imageNamed:@"check_on"];
    if (indexPath.row == 1) {
        self.answerData = [NSNumber numberWithInt:1];
    }else if (indexPath.row == 2){
        self.answerData = [NSNumber numberWithInt:0];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGRadioButtonCell *cell = (EFGRadioButtonCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.checkImage.image = [UIImage imageNamed:@"check_off"];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}



@end
