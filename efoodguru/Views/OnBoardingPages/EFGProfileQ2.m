//
//  EFGProfileQ2.m
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGProfileQ2.h"
#import "EFGUtilities.h"

@implementation EFGProfileQ2{
    NSArray *avoidanceList;
    CGRect rect;
    NSMutableArray *answersList;
    NSMutableDictionary *di;
}

- (instancetype)initWithFrame:(CGRect)frame andData:(NSArray *)array
{
    self = [super initWithFrame:frame];
    if (self) {
        avoidanceList = array;
        rect = frame;
        answersList = [[NSMutableArray alloc] init];
        [self formatAnswerData];
        [self initSetup];
    }
    return self;
}

- (void)formatAnswerData{
    di = [[NSMutableDictionary alloc] init];
    for (NSDictionary *d in avoidanceList) {
        [di setObject:@"false" forKey:d[@"avoidance_label"]];
    }
}

- (void)initSetup{
    self.questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, rect.size.width - 20, 0)];
    self.questionLabel.text = @"What nutrition facts would you like displayed to you when you scan an item ?";
    self.questionLabel.numberOfLines = 0;
    self.questionLabel.textAlignment = NSTextAlignmentCenter;
    self.questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.questionLabel sizeToFit];
    [self addSubview:self.questionLabel];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, self.questionLabel.frame.origin.y + self.questionLabel.frame.size.height + 10, rect.size.width - 20, rect.size.height - self.questionLabel.frame.origin.y + self.questionLabel.frame.size.height - 150) style:UITableViewStylePlain];
    [self.tableView registerClass:[EFGBasicTableViewCell class] forCellReuseIdentifier:@"cell_basic"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self addSubview:self.tableView];
    self.tableView.layer.cornerRadius = 6.0;
    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.tableView.allowsMultipleSelection = YES;
    self.tableView.separatorInset = UIEdgeInsetsZero;
}

- (void)loadAvoidance:(NSArray *)avoidance{
    avoidanceList = avoidance;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return avoidanceList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGBasicTableViewCell *cell = (EFGBasicTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell_basic" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[EFGBasicTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell_basic"];
    }
    [cell setupCell:[avoidanceList objectAtIndex:indexPath.row] isSub:NO];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGBasicTableViewCell *cell = (EFGBasicTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.checkImage.image = [UIImage imageNamed:@"check_on"];
    [di setObject:@"true" forKey:[avoidanceList objectAtIndex:indexPath.row][@"avoidance_label"]];
    //[answersList addObject:[avoidanceList objectAtIndex:indexPath.row][@"avoidance_label"]];
    self.answerData = [di mutableCopy];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGBasicTableViewCell *cell = (EFGBasicTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.checkImage.image = [UIImage imageNamed:@"check_off"];
    [di setObject:@"false" forKey:[avoidanceList objectAtIndex:indexPath.row][@"avoidance_label"]];
    //[answersList removeObject:[avoidanceList objectAtIndex:indexPath.row][@"avoidance_label"]];
    self.answerData = [di mutableCopy];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}


@end
