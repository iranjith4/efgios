//
//  EFGProfileQ3.h
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFGProfileQ3 : UIView <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UILabel *questionLabel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSNumber *answerData;

@end
