//
//  EFGProfileQ1.m
//  efoodguru
//
//  Created by Ranjithkumar on 15/11/15.
//  Copyright © 2015 EFG. All rights reserved.
//

#import "EFGProfileQ1.h"

@implementation EFGProfileQ1{
    NSArray *avoidanceList;
    NSMutableArray *answersList;
    CGRect rect;
}

- (instancetype)initWithFrame:(CGRect)frame andData:(NSArray *)array
{
    self = [super initWithFrame:frame];
    if (self) {
        avoidanceList = array;
        rect = frame;
        answersList = [[NSMutableArray alloc] init];
        [self formAnswersArray];
        [self initSetup];
    }
    return self;
}

- (void)formAnswersArray {
    for (NSDictionary *avoidance in avoidanceList) {
        NSString *selectionValue;
        if ([avoidance[@"avoidance_selection_value"] intValue] == 1) {
            selectionValue = @"true";
        }else{
            selectionValue = @"false";
        }
        //
        NSArray *subAvoidanceArray = [[NSArray alloc] init];
        NSMutableArray *subData = [[NSMutableArray alloc] init];
        NSArray *temp = avoidance[@"user_sub_avoidance_ans"];
        
        for (NSDictionary *d in temp) {
            NSString *sVal = @"false";
            if (d[@"subavoidance_selection_value"] != [NSNull null] && d[@"subavoidance_selection_value"] != nil ){
                if ([d[@"subavoidance_selection_value"] intValue] == 1) {
                    selectionValue = @"true";
                }else{
                    selectionValue = @"false";
                }
            }
                NSDictionary *dict = @{
                                       @"subavoidance_label" :d[@"subavoidance_label"],
                                       @"subavoidance_id" :d[@"subavoidancelist_id"],
                                       @"subavoidance_selection" : sVal
                                       };
                [subData addObject:dict];
        }
        subAvoidanceArray = subData;
        
        //
        NSDictionary *dict = @{
                               @"avoidance_id" : avoidance[@"avoidancelist_id"],
                               @"avoidance_selection" : selectionValue,
                               @"avoidance_label" : avoidance[@"avoidance_label"],
                               @"sub_avoidance" : subAvoidanceArray
                               };
        [answersList addObject:dict];
    }
    
}

- (void)initSetup{
    self.questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, rect.size.width - 20, 0)];
    self.questionLabel.text = @"What food or food ingredients do you want to avoid ?";
    self.questionLabel.numberOfLines = 0;
    self.questionLabel.textAlignment = NSTextAlignmentCenter;
    self.questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.questionLabel sizeToFit];
    [self addSubview:self.questionLabel];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, self.questionLabel.frame.origin.y + self.questionLabel.frame.size.height + 10, rect.size.width - 20, rect.size.height - self.questionLabel.frame.origin.y + self.questionLabel.frame.size.height - 150) style:UITableViewStylePlain];
    [self.tableView registerClass:[EFGBasicTableViewCell class] forCellReuseIdentifier:@"cell_basic"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self addSubview:self.tableView];
    self.tableView.layer.cornerRadius = 6.0;
    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.tableView.allowsMultipleSelection = YES;
    self.tableView.separatorInset = UIEdgeInsetsZero;
}

- (void)loadAvoidance:(NSArray *)avoidance{
    avoidanceList = avoidance;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return avoidanceList.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EFGBasicTableViewCell *cell = (EFGBasicTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell_basic" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[EFGBasicTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell_basic"];
    }
    if ([[avoidanceList objectAtIndex:indexPath.row][@"avoidance_selection_value"] boolValue]) {
            [tableView
             selectRowAtIndexPath:indexPath
             animated:TRUE
             scrollPosition:UITableViewScrollPositionNone
             ];
            
            [[tableView delegate]
             tableView:tableView
             didSelectRowAtIndexPath:indexPath
             ];
    }

    [cell setupCell:[avoidanceList objectAtIndex:indexPath.row] isSub:NO];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"select");
    
    EFGBasicTableViewCell *cell = (EFGBasicTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    NSArray *subArray = [avoidanceList objectAtIndex:indexPath.row][@"user_sub_avoidance_ans"];
    if (subArray.count > 0) {
        cell.checkImage.image = [UIImage imageNamed:@"check_on"];
        [self.delegate openSubSelectionWithData:subArray];
    }else{
        cell.checkImage.image = [UIImage imageNamed:@"check_on"];
        [self setSelectforRow:(int)indexPath.row select:YES];
        self.answerData = answersList;
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"deselect");
    EFGBasicTableViewCell *cell = (EFGBasicTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    NSArray *subArray = [avoidanceList objectAtIndex:indexPath.row][@"user_sub_avoidance_ans"];
    if (subArray.count > 0) {
        cell.checkImage.image = [UIImage imageNamed:@"check_off"];
        [self.delegate openSubSelectionWithData:subArray];
    }else{
        cell.checkImage.image = [UIImage imageNamed:@"check_off"];
        [self setSelectforRow:(int)indexPath.row select:NO];
        self.answerData = answersList;
    }
}

- (void)setSelectforRow:(int)row select:(BOOL)isSlected{
    NSDictionary *dict = [avoidanceList objectAtIndex:row];
     NSArray *subAvoidanceArray = [[NSArray alloc] init];
    if (isSlected) {
        NSDictionary *d = @{
                                @"avoidance_id" : dict[@"avoidancelist_id"],
                               @"avoidance_selection" : @"true",
                               @"avoidance_label" : dict[@"avoidance_label"],
                               @"sub_avoidance" : subAvoidanceArray
                               };
        [answersList setObject:d atIndexedSubscript:row];

    }else{
        NSDictionary *d = @{
                            @"avoidance_id" : dict[@"avoidancelist_id"],
                            @"avoidance_selection" : @"false",
                            @"avoidance_label" : dict[@"avoidance_label"],
                            @"sub_avoidance" : subAvoidanceArray
                            };
        [answersList setObject:d atIndexedSubscript:row];
    }
}

-(void)saveSubAvoidanceValue:(NSArray *)a forAvoidance:(NSString *)avd{
    for (int i = 0; i < answersList.count; i++) {
        NSDictionary *d = [answersList objectAtIndex:i];
        NSMutableDictionary *mD = [[NSMutableDictionary alloc] initWithDictionary:d];
        if ([[NSString stringWithFormat:@"%@",d[@"avoidance_id"]] isEqualToString:avd]) {
            [mD setObject:a forKey:@"sub_avoidance"];
            [answersList setObject:mD atIndexedSubscript:i];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

@end
